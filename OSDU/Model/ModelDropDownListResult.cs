﻿using OSDU_API;
using OSDU_API.Objects;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;

namespace OSDU.Model
{
    public class ModelDropDownListResult
    {
        public HttpStatusCode ResponseStatusCode { get; private set; }
        public const String DateFormat = "DATE";
        public String ResponseContent { get; private set; }

        public Dictionary<String, String> LookUpOrganisation = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpOperatingEnvironment = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpFacilityEventType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpWellInterestType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpUnitOfMeasure = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpVerticalMeasurementPath = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpVerticalMeasurementType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpVerticalMeasuremenSource = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpGeoPoliticalEntity = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpWellboreTrajectoryType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpMaterialType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpWell = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpFacilityStateType = new Dictionary<String, String>();
        public Dictionary<String, String> LookUpCoordinateReferenceSystem = new Dictionary<String, String>();

        private readonly Authenticator _auth;
        private readonly DataSourceParameters _parameters;
        private readonly RestClient _client;

        public ModelDropDownListResult(Authenticator auth, DataSourceParameters parameters)
        {
            _auth = auth;
            _parameters = parameters;
            _client = new RestClient(_parameters.SearchURL + Common.URLs.QueryURL);
            _client.Authenticator = _auth;
            _client.Timeout = -1;
        }

        public void SetAllLookUpValues(ref ModelLog log)
        {
            GetOrganisation();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupOrganization, ModelLog.Status.Success, LookUpOrganisation.Count + " record/s.");
            GetOperatingEnvironment();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupOperatingEnvironment, ModelLog.Status.Success, LookUpOperatingEnvironment.Count + " record/s.");
            GetFacilityEventType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupFacilityEventType, ModelLog.Status.Success, LookUpFacilityEventType.Count + " record/s.");
            GetWellInterestType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupWellInterestType, ModelLog.Status.Success, LookUpWellInterestType.Count + " record/s.");
            GetUnitOfMeasure();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupUnitOfMeasure, ModelLog.Status.Success, LookUpUnitOfMeasure.Count + " record/s.");
            GetVerticalMeasurementPath();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupVerticalMeasurementPath, ModelLog.Status.Success, LookUpVerticalMeasurementPath.Count + " record/s.");
            GetVerticalMeasurementType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupVerticalMeasurementType, ModelLog.Status.Success, LookUpVerticalMeasurementType.Count + " record/s.");
            GetVerticalMeasuremenSource();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupVerticalMeasuremenSource, ModelLog.Status.Success, LookUpVerticalMeasuremenSource.Count + " record/s.");
            GetGeoPoliticalEntity();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupGeoPoliticalEntity, ModelLog.Status.Success, LookUpGeoPoliticalEntity.Count + " record/s.");
            GetWellboreTrajectoryType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupWellboreTrajectoryType, ModelLog.Status.Success,LookUpWellboreTrajectoryType.Count + " record/s.");
            GetMaterialType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupMaterialType, ModelLog.Status.Success, LookUpMaterialType.Count + " record/s.");
            GetWellKind();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupWell, ModelLog.Status.Success, LookUpWell.Count + " record/s.");
            GetFacilityStateType();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupFacilityStateType, ModelLog.Status.Success, LookUpFacilityStateType.Count + " record/s.");
            GetCoordinateReferenceSystem();
            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupCoordinateReferenceSystem, ModelLog.Status.Success, LookUpCoordinateReferenceSystem.Count + " record/s.");
        }

        public Dictionary<String, String> GetLookUpByPropertyName_Well(String propertyName, ref String LookUpName)
        {
            switch(propertyName)
            {
                case ModelWell.InitialOperatorID:
                case ModelWell.CurrentOperatorID:
                case ModelWell.DataSourceOrganisationID:
                    {
                        LookUpName = nameof(LookUpOrganisation);
                        return LookUpOrganisation;
                    }
                    
                case ModelWell.OperatingEnvironmentID:
                    {
                        LookUpName = nameof(LookUpOperatingEnvironment);
                        return LookUpOperatingEnvironment;
                    }
                    
                case ModelWell.FacilityEventTypeID1:
                case ModelWell.FacilityEventTypeID2:
                    {
                        LookUpName = nameof(LookUpFacilityEventType);
                        return LookUpFacilityEventType;
                    }
                    
                case ModelWell.InterestTypeID:
                    {
                        LookUpName = nameof(LookUpWellInterestType);
                        return LookUpWellInterestType;
                    }
                    
                case ModelWell.VerticalMeasurementUnitOfMeasureID1:
                case ModelWell.VerticalMeasurementUnitOfMeasureID2:
                    {
                        LookUpName = nameof(LookUpUnitOfMeasure);
                        return LookUpUnitOfMeasure;
                    }
                    
                case ModelWell.VerticalMeasurementPathID1:
                case ModelWell.VerticalMeasurementPathID2:
                    {
                        LookUpName = nameof(LookUpVerticalMeasurementPath);
                        return LookUpVerticalMeasurementPath;
                    }
                    
                case ModelWell.VerticalMeasurementTypeID1:
                case ModelWell.VerticalMeasurementTypeID2:
                    {
                        LookUpName = nameof(LookUpVerticalMeasurementType);
                        return LookUpVerticalMeasurementType;
                    }
                   
                case ModelWell.VerticalMeasurementSourceID1:
                case ModelWell.VerticalMeasurementSourceID2:
                    {
                        LookUpName = nameof(LookUpVerticalMeasuremenSource);
                        return LookUpVerticalMeasuremenSource;
                    }
                case ModelWell.EffectiveDateTime1:
                case ModelWell.EffectiveDateTime2:
                    {
                        LookUpName = DateFormat;
                        return new Dictionary<String, String>();
                    }
                default:
                    {
                        LookUpName = String.Empty;
                        return new Dictionary<String, String>();
                    }
                   
            }
        }

        public Dictionary<String, String> GetLookUpByPropertyName_Wellbore(String propertyName, ref String LookUpName)
        {
            switch (propertyName)
            {
                case ModelWellbore.InitialOperatorID:
                case ModelWellbore.CurrentOperatorID:
                    {
                        LookUpName = nameof(LookUpOrganisation);
                        return LookUpOrganisation;
                    }
                case ModelWellbore.TrajectoryTypeID:
                    {
                        LookUpName = nameof(LookUpWellboreTrajectoryType);
                        return LookUpWellboreTrajectoryType;
                    }
                case ModelWellbore.Country:
                    {
                        LookUpName = nameof(LookUpGeoPoliticalEntity);
                        return LookUpGeoPoliticalEntity;
                    }

                case ModelWellbore.PrimaryMaterialID:
                    {
                        LookUpName = nameof(LookUpMaterialType);
                        return LookUpMaterialType;
                    }
                case ModelWellbore.Status:
                    {
                        LookUpName = nameof(LookUpFacilityStateType);
                        return LookUpFacilityStateType;
                    }
                case ModelWellbore.WellID:
                    {
                        LookUpName = nameof(LookUpWell);
                        return LookUpWell;
                    }
                case ModelWellbore.RT_VerticalMeasurementUnitOfMeasureID:
                case ModelWellbore.TD_VerticalMeasurementUnitOfMeasureID:
                    {
                        LookUpName = nameof(LookUpUnitOfMeasure);
                        return LookUpUnitOfMeasure;
                    }
                case ModelWellbore.StartDrillingDate:
                case ModelWellbore.FinishDrillingDate:
                    {
                        LookUpName = DateFormat;
                        return new Dictionary<String, String>();
                    }
                default:
                    {
                        LookUpName = String.Empty;
                        return new Dictionary<String, String>();
                    }
            }
        }

        public void GetOrganisation()
        {
            GetLookUp_Data_Organization(Common.JsonTags.OrganisationKind, ref LookUpOrganisation);   
        }
        public void GetOperatingEnvironment()
        {
            GetLookUp_Data(Common.JsonTags.OperatingEnvironmentKind, ref LookUpOperatingEnvironment);
        }
        public void GetFacilityEventType()
        {
            GetLookUp_Data(Common.JsonTags.FacilityEventTypeKind, ref LookUpFacilityEventType);
        }
        public void GetWellInterestType()
        {
            GetLookUp_Data(Common.JsonTags.WellInterestTypeKind, ref LookUpWellInterestType);     
        }
        public void GetUnitOfMeasure()
        {
            GetLookUp_Data(Common.JsonTags.UnitOfMeasureKind, ref LookUpUnitOfMeasure, "data.UnitDimensionName: length");
        }
        public void GetVerticalMeasurementPath()
        {
            GetLookUp_Data(Common.JsonTags.VerticalMeasurementPathKind, ref LookUpVerticalMeasurementPath);
        }
        public void GetVerticalMeasurementType()
        {
            GetLookUp_Data(Common.JsonTags.VerticalMeasurementTypeKind, ref LookUpVerticalMeasurementType);
        }
        public void GetVerticalMeasuremenSource()
        {
            GetLookUp_Data(Common.JsonTags.VerticalMeasuremenSourceKind, ref LookUpVerticalMeasuremenSource);
        }
        public void GetGeoPoliticalEntity()
        {
            GetLookUp_Data_GeoPoliticalEntity(Common.JsonTags.GeoPoliticalEntityKind, ref LookUpGeoPoliticalEntity);
        }
        public void GetWellboreTrajectoryType()
        {
            GetLookUp_Data(Common.JsonTags.WellboreTrajectoryTypeKind, ref LookUpWellboreTrajectoryType);
        }
        public void GetMaterialType()
        {
            GetLookUp_Data(Common.JsonTags.MaterialTypeKind, ref LookUpMaterialType);
        }
        public void GetWellKind()
        {
            GetLookUp_Well(Common.JsonTags.WellKind, ref LookUpWell);
        }
        public void GetFacilityStateType()
        {
            GetLookUp_Data(Common.JsonTags.FacilityStateTypeKind, ref LookUpFacilityStateType);
        }
        public void GetCoordinateReferenceSystem()
        {
            GetLookUp_Data(Common.JsonTags.CoordinateReferenceSystemKind, ref LookUpCoordinateReferenceSystem);
        }

        public void GetLookUp_Data_Organization(String kind, ref Dictionary<String, String> lookUpBind)
        {
            RestRequest request;
            request = new RestRequest(Method.POST);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            SearchQuery query = new SearchQuery();
            query.kind = kind;
            query.returnedFields.Add("id");
            query.returnedFields.Add("data.OrganisationName");
            //query.returnedFields.Add("data.Name");
            query.limit = 10000;
            //TODO: cannot seem to get pagination to work properly in R2 as offsets returning random results, sort doesn't work, see "totalCount" below.
            //For now will assume we always get all results in one go using a high limit.
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(query);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                _auth.ClearBearerToken();
                GetLookUp_Data_Organization(kind, ref lookUpBind);
            }
            //otherwise, good response then parse out the values
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                ModelDropDownList.Root resultLookUp = JsonSerializer.Deserialize<ModelDropDownList.Root>(response.Content);

                Dictionary<String, String> keyValuePairs = new Dictionary<String, String>();
                foreach (ModelDropDownList.Result result in resultLookUp.results)
                    keyValuePairs.Add(result.id.Split(':').Last().Trim(), result.data.OrganisationName);

                foreach (KeyValuePair<String, String> keyValue in keyValuePairs.OrderBy(key => key.Value))
                {
                    lookUpBind.Add(keyValue.Key, keyValue.Value);
                }
            }
        }
        public void GetLookUp_Data_GeoPoliticalEntity(String kind, ref Dictionary<String, String> lookUpBind)
        {
            RestRequest request;
            request = new RestRequest(Method.POST);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            SearchQuery query = new SearchQuery();
            query.kind = kind;
            query.query = "data.GeoPoliticalEntityTypeID: \""+ _parameters.DataPartitionID +":reference-data--GeoPoliticalEntityType:Country:\"";
            query.returnedFields.Add("id");
            query.returnedFields.Add("data.GeoPoliticalEntityName");
            //query.returnedFields.Add("data.Name");
            query.limit = 10000;
            //TODO: cannot seem to get pagination to work properly in R2 as offsets returning random results, sort doesn't work, see "totalCount" below.
            //For now will assume we always get all results in one go using a high limit.
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(query);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                _auth.ClearBearerToken();
                GetLookUp_Data_GeoPoliticalEntity(kind, ref lookUpBind);
            }
            //otherwise, good response then parse out the values
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                ModelDropDownList.Root resultLookUp = JsonSerializer.Deserialize<ModelDropDownList.Root>(response.Content);

                Dictionary<String, String> keyValuePairs = new Dictionary<String, String>();
                foreach (ModelDropDownList.Result result in resultLookUp.results)
                    keyValuePairs.Add(result.id.Split(':').Last().Trim(), result.data.GeoPoliticalEntityName);

                foreach (KeyValuePair<String, String> keyValue in keyValuePairs.OrderBy(key => key.Value))
                {
                    lookUpBind.Add(keyValue.Key, keyValue.Value);
                }
            }
        }
        public void GetLookUp_Well(String kind, ref Dictionary<String, String> lookUpBind)
        {
            RestRequest request;
            request = new RestRequest(Method.POST);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            SearchQuery query = new SearchQuery();
            query.kind = kind;
            query.returnedFields.Add("id");
            query.returnedFields.Add("data.FacilityID");
            //query.returnedFields.Add("data.Name");
            query.limit = 10000;
            //TODO: cannot seem to get pagination to work properly in R2 as offsets returning random results, sort doesn't work, see "totalCount" below.
            //For now will assume we always get all results in one go using a high limit.
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(query);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                _auth.ClearBearerToken();
                GetLookUp_Well(kind, ref lookUpBind);
            }
            //otherwise, good response then parse out the values
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                ModelDropDownList.Root resultLookUp= JsonSerializer.Deserialize<ModelDropDownList.Root>(response.Content);

                Dictionary<String, String> keyValuePairs = new Dictionary<String, String>();
                foreach (ModelDropDownList.Result result in resultLookUp.results)
                    keyValuePairs.Add(result.id.Split(':').Last().Trim(), result.data.FacilityID);

                foreach (KeyValuePair<String, String> keyValue in keyValuePairs.OrderBy(key => key.Value))
                {
                    lookUpBind.Add(keyValue.Key,keyValue.Value);
                }
            }
        }
        public void GetLookUp_Data(String kind, ref Dictionary<String, String> lookUpBind, String searchQuery = "")
        {
            RestRequest request;
            request = new RestRequest(Method.POST);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            SearchQuery query = new SearchQuery();
            query.kind = kind;
            if (!String.IsNullOrEmpty(searchQuery))
                query.query = searchQuery;
            query.returnedFields.Add("id");
            query.returnedFields.Add("data.Name");
            //query.returnedFields.Add("data.Name");
            query.limit = 10000;
            //TODO: cannot seem to get pagination to work properly in R2 as offsets returning random results, sort doesn't work, see "totalCount" below.
            //For now will assume we always get all results in one go using a high limit.
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(query);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                _auth.ClearBearerToken();
                GetLookUp_Data(kind, ref lookUpBind, searchQuery);
            }
            //otherwise, good response then parse out the values
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                ModelDropDownList.Root resultLookUp = JsonSerializer.Deserialize<ModelDropDownList.Root>(response.Content);

                Dictionary<String, String> keyValuePairs = new Dictionary<String, String>();
                foreach (ModelDropDownList.Result result in resultLookUp.results)
                    keyValuePairs.Add(result.id.Split(':').Last().Trim(), result.data.Name);

                foreach (KeyValuePair<String, String> keyValue in keyValuePairs.OrderBy(key => key.Value))
                {
                    lookUpBind.Add(keyValue.Key, keyValue.Value);
                }
            }
        }
    }
}
