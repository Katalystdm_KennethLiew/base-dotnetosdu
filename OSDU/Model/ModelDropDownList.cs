﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU.Model
{
    public class ModelDropDownList
    {
        public class Data
        {
            public String Code { get; set; }
            public String Name { get; set; }
            public String OrganisationName { get; set; }
            public String GeoPoliticalEntityName { get; set; }
            public String FacilityID { get; set; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
        public class Result
        {
            public Data data { get; set; }
            public String id { get; set; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
        public class Root
        {
            public List<Result> results { get; set; }
            public Int32 totalCount { get; set; }
        }
    }
}
