﻿using OSDU_API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU.Model
{
    public class ModelConnectionInfo
    {
        public Authenticator Auth { get; set; } = new Authenticator();
        public DataSourceParameters DSParams { get; set; } = new DataSourceParameters();
    }
}
