﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU.Model
{
    public static class ModelWellbore
    {
        public const String FacilityID = "FacilityID";
        public const String FacilityName = "FacilityName";
        public const String Country = "Country";//lookup GeoPoliticalEntityID
        public const String TrajectoryTypeID = "TrajectoryTypeID"; //Lookup WellboreTrajectoryType
        public const String PrimaryMaterialID = "PrimaryMaterialID";//Lookup  MaterialType
        public const String Status = "Status";//Lookup FacilityStateType ,special Handle
        public const String StartDrillingDate = "StartDrillingDate";// special Handle
        public const String FinishDrillingDate = "FinishDrillingDate";// special handle
        public const String InitialOperatorID = "InitialOperatorID";
        public const String CurrentOperatorID = "CurrentOperatorID";
        public const String WellID = "WellID";
        public const String SequenceNumber = "SequenceNumber";
        public const String TD_VerticalMeasurement = "TD_VerticalMeasurement";
        public const String TD_VerticalMeasurementUnitOfMeasureID = "TD_VerticalMeasurementUnitOfMeasureID";
        public const String RT_VerticalMeasurement = "RT_VerticalMeasurement";
        public const String RT_VerticalMeasurementUnitOfMeasureID = "RT_VerticalMeasurementUnitOfMeasureID";
    }

    public enum GetColNum_Wellbore
    {
        FacilityID = 0,
        FacilityName = 1,
        Country = 2,
        TrajectoryTypeID = 3,
        PrimaryMaterialID = 4,
        Status = 5,
        StartDrillingDate = 6,
        FinishDrillingDate = 7,
        InitialOperatorID = 8,
        CurrentOperatorID = 9,
        WellID = 10,
        SequenceNumber = 11,
        TD_VerticalMeasurement = 12,
        TD_VerticalMeasurementUnitOfMeasureID = 13,
        RT_VerticalMeasurement = 14,
        RT_VerticalMeasurementUnitOfMeasureID = 15
    }

    public static class MandataryField_Wellbore
    {
        public const String FacilityID = ModelWellbore.FacilityID;
        public const String FacilityName = ModelWellbore.FacilityName;
    }
}