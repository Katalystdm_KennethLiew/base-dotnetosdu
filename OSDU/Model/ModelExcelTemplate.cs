﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FrameworkExcel;

namespace OSDU.Model
{
    public class ModelExcelTemplate
    {
        public void WriteExcel(String filePath, Model.ModelDropDownListResult searchService)
        {

            //Create workbook
            IWorkbook workbook = new XSSFWorkbook();

            #region Well
            XSSFSheet sheetWell = (XSSFSheet)workbook.CreateSheet("Well");

            IRow HeaderRow = sheetWell.CreateRow(0);

            XSSFCellStyle borderedCellStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            borderedCellStyle.BorderLeft = BorderStyle.Medium;
            borderedCellStyle.BorderTop = BorderStyle.Medium;
            borderedCellStyle.BorderRight = BorderStyle.Medium;
            borderedCellStyle.BorderBottom = BorderStyle.Medium;
            borderedCellStyle.VerticalAlignment = VerticalAlignment.Center;

            SetSheetLookUp(ref workbook, searchService);

            int i = 0;
            foreach (FieldInfo wellField in typeof(ModelWell).GetFields().Where(x => x.IsStatic && x.IsLiteral))
            {
                ExcelWrite.CreateCell(HeaderRow, i, wellField.GetRawConstantValue().ToString(), borderedCellStyle);
                String LookUpName = String.Empty;
                Dictionary<String, String> keyValuePairs = searchService.GetLookUpByPropertyName_Well(wellField.GetRawConstantValue().ToString(), ref LookUpName);

                //if is date format, set it to date format cell
                if (LookUpName == ModelDropDownListResult.DateFormat)
                {
                    GetColNum_Well colNum = (GetColNum_Well)Enum.Parse(typeof(GetColNum_Well), wellField.GetRawConstantValue().ToString());
                    CreateExcelDateFormat(ref sheetWell, (Int32)colNum);
                }
                else
                {
                    if (!String.IsNullOrEmpty(LookUpName))
                    {
                        GetColNum_Well colNum = (GetColNum_Well)Enum.Parse(typeof(GetColNum_Well), wellField.GetRawConstantValue().ToString());
                        CreateExcelDropDownList(ref sheetWell, keyValuePairs, (Int32)colNum, LookUpName);
                    }
                }
               
                i++;
            }
            #endregion

            #region Wellbore
            XSSFSheet sheetWellbore = (XSSFSheet)workbook.CreateSheet("Wellbore");

            IRow HeaderRow_Wellbore = sheetWellbore.CreateRow(0);

            int index = 0;
            foreach (FieldInfo wellboreField in typeof(ModelWellbore).GetFields().Where(x => x.IsStatic && x.IsLiteral))
            {
                ExcelWrite.CreateCell(HeaderRow_Wellbore, index, wellboreField.GetRawConstantValue().ToString(), borderedCellStyle);
                String LookUpName = String.Empty;
                Dictionary<String, String> keyValuePairs = searchService.GetLookUpByPropertyName_Wellbore(wellboreField.GetRawConstantValue().ToString(), ref LookUpName);

                //if is date format, set it to date format cell
                if (LookUpName == ModelDropDownListResult.DateFormat)
                {
                    GetColNum_Wellbore colNum = (GetColNum_Wellbore)Enum.Parse(typeof(GetColNum_Wellbore), wellboreField.GetRawConstantValue().ToString());
                    CreateExcelDateFormat(ref sheetWellbore, (Int32)colNum);
                }
                else
                {
                    if (!String.IsNullOrEmpty(LookUpName))
                    {
                        GetColNum_Wellbore colNum = (GetColNum_Wellbore)Enum.Parse(typeof(GetColNum_Wellbore), wellboreField.GetRawConstantValue().ToString());
                        CreateExcelDropDownList(ref sheetWellbore, keyValuePairs, (Int32)colNum, LookUpName);
                    }
                }
                //sheetWell.SetDefaultColumnStyle(i, yourCellStyle);
                index++;
            }
            #endregion
            // Auto sized all the affected columns
            int lastColumNum = sheetWell.GetRow(0).LastCellNum;
            for (int x = 0; x <= lastColumNum; x++)
            {
                sheetWell.AutoSizeColumn(x);
                GC.Collect();
            }

            // Auto sized all the affected columns
            int lastColumNum1 = sheetWellbore.GetRow(0).LastCellNum;
            for (int x = 0; x <= lastColumNum1; x++)
            {
                sheetWellbore.AutoSizeColumn(x);
                GC.Collect();
            }

            ExcelWrite.WriteExecute(filePath, workbook);

        }

        public void SetSheetLookUp(ref IWorkbook workbook, Model.ModelDropDownListResult searchService )
        {
            AddDropDownSheet(ref workbook, searchService.LookUpOrganisation, nameof(searchService.LookUpOrganisation));
            AddDropDownSheet(ref workbook, searchService.LookUpOperatingEnvironment, nameof(searchService.LookUpOperatingEnvironment));
            AddDropDownSheet(ref workbook, searchService.LookUpFacilityEventType, nameof(searchService.LookUpFacilityEventType));
            AddDropDownSheet(ref workbook, searchService.LookUpWellInterestType, nameof(searchService.LookUpWellInterestType));
            AddDropDownSheet(ref workbook, searchService.LookUpUnitOfMeasure, nameof(searchService.LookUpUnitOfMeasure));
            AddDropDownSheet(ref workbook, searchService.LookUpVerticalMeasurementPath, nameof(searchService.LookUpVerticalMeasurementPath));
            AddDropDownSheet(ref workbook, searchService.LookUpVerticalMeasurementType, nameof(searchService.LookUpVerticalMeasurementType));
            AddDropDownSheet(ref workbook, searchService.LookUpVerticalMeasuremenSource, nameof(searchService.LookUpVerticalMeasuremenSource));
            AddDropDownSheet(ref workbook, searchService.LookUpGeoPoliticalEntity, nameof(searchService.LookUpGeoPoliticalEntity));
            AddDropDownSheet(ref workbook, searchService.LookUpWellboreTrajectoryType, nameof(searchService.LookUpWellboreTrajectoryType));
            AddDropDownSheet(ref workbook, searchService.LookUpMaterialType, nameof(searchService.LookUpMaterialType));
            AddDropDownSheet(ref workbook, searchService.LookUpWell, nameof(searchService.LookUpWell));
            AddDropDownSheet(ref workbook, searchService.LookUpFacilityStateType, nameof(searchService.LookUpFacilityStateType));
            AddDropDownSheet(ref workbook, searchService.LookUpCoordinateReferenceSystem, nameof(searchService.LookUpCoordinateReferenceSystem));
        }
    
        public void AddDropDownSheet(ref IWorkbook workbook, Dictionary<String, String> lookUpData, String lookupName)
        {
            XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet(lookupName);

            int count = 0;
            foreach (KeyValuePair<string, string> entry in lookUpData)
            {
                IRow Row = sheet.CreateRow(count);
                ICell Cell = Row.CreateCell(0);
                Cell.SetCellValue(entry.Value + " | " + entry.Key);
                count++;
            }

            workbook.SetSheetHidden(workbook.NumberOfSheets - 1, 1);
        }

        public void CreateExcelDropDownList(ref XSSFSheet sheet,Dictionary<String,String> lookUpValues, Int32 colIndex, String LookUpName )
        {
            if(lookUpValues.Count == 0)
            {
                IDataValidationHelper validationHelper = new XSSFDataValidationHelper(sheet);
                CellRangeAddressList addressList = new CellRangeAddressList(-1, -1, colIndex, colIndex);

                IDataValidationConstraint constraint = validationHelper.CreateExplicitListConstraint(new string[] {""});
                IDataValidation dataValidation = validationHelper.CreateValidation(constraint, addressList);
                dataValidation.ShowErrorBox = true;
                dataValidation.SuppressDropDownArrow = true;
                dataValidation.ErrorStyle = 0;
                dataValidation.CreateErrorBox("Drop Down List Value", "Please enter or select the value in the drop-down list.");
                dataValidation.ShowErrorBox = true;
                sheet.AddValidationData(dataValidation);
            }
            else
            {
                IDataValidationHelper validationHelper = new XSSFDataValidationHelper(sheet);
                CellRangeAddressList addressList = new CellRangeAddressList(-1, -1, colIndex, colIndex);

                IDataValidationConstraint constraint = validationHelper.CreateFormulaListConstraint("'" + LookUpName + "'!$A$1:$A$" + lookUpValues.Count + "");
                IDataValidation dataValidation = validationHelper.CreateValidation(constraint, addressList);
                //dataValidation.SuppressDropDownArrow = true;
                //dataValidation.CreateErrorBox("The input is invalid", "Please enter or select the value in the drop-down list.");
                //dataValidation.ShowPromptBox = true;
                dataValidation.ShowErrorBox = true;
                dataValidation.SuppressDropDownArrow = true;
                dataValidation.ErrorStyle = 0;
                dataValidation.CreateErrorBox("Drop Down List Value", "Please enter or select the value in the drop-down list.");
                dataValidation.ShowErrorBox = true;
                //dataValidation.CreatePromptBox("The input is invalid", "Please enter or select the value in the drop-down list.");
                //dataValidation.ShowPromptBox = true;
                sheet.AddValidationData(dataValidation);
            }
           
        }

        public void CreateExcelDateFormat(ref XSSFSheet sheetWell, Int32 colIndex)
        {
            IDataValidationHelper validationHelper = new XSSFDataValidationHelper(sheetWell);
            CellRangeAddressList addressList = new CellRangeAddressList(-1, -1, colIndex, colIndex);

            XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)validationHelper.CreateDateConstraint(OperatorType.BETWEEN, "=DATE(1900,1,1)", "=DATE(2119,12,31)", "dd-MMM-yyyy");
            XSSFDataValidation dataValidation = (XSSFDataValidation)validationHelper.CreateValidation(dvConstraint, addressList);
            dataValidation.ShowErrorBox = true;
            dataValidation.ErrorStyle = 0;
            dataValidation.CreateErrorBox("InvalidDate", "Allowed Format is dd-mon-yyyy");
            dataValidation.ShowErrorBox = true;
            dataValidation.CreatePromptBox("Date Data Validation", "Enter Date in Format dd-mon-yyyy.");
            dataValidation.ShowPromptBox = true;
            sheetWell.AddValidationData(dataValidation);
        }

       
    }
}
