﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU.Model
{
    public static class ModelWell
    {
        public const String FacilityID = "FacilityID";
        public const String FacilityName = "FacilityName";
        public const String InitialOperatorID = "InitialOperatorID";
        public const String CurrentOperatorID = "CurrentOperatorID";
        public const String DataSourceOrganisationID = "DataSourceOrganisationID";
        public const String OperatingEnvironmentID = "OperatingEnvironmentID";
        public const String FacilityEventTypeID1 = "FacilityEventTypeID1";
        public const String EffectiveDateTime1 = "EffectiveDateTime1";
        public const String FacilityEventTypeID2 = "FacilityEventTypeID2";
        public const String EffectiveDateTime2 = "EffectiveDateTime2";
        public const String InterestTypeID = "InterestTypeID";
        public const String VerticalMeasurementID1 = "VerticalMeasurementID1";
        public const String VerticalMeasurementDescription1 = "VerticalMeasurementDescription1";
        public const String VerticalMeasurement1 = "VerticalMeasurement1";
        public const String VerticalMeasurementUnitOfMeasureID1 = "VerticalMeasurementUnitOfMeasureID1";
        public const String VerticalMeasurementPathID1 = "VerticalMeasurementPathID1";
        public const String VerticalMeasurementTypeID1 = "VerticalMeasurementTypeID1";
        public const String VerticalMeasurementSourceID1 = "VerticalMeasurementSourceID1";
        public const String VerticalMeasurementID2 = "VerticalMeasurementID2";
        public const String VerticalMeasurementDescription2 = "VerticalMeasurementDescription2";
        public const String VerticalMeasurement2 = "VerticalMeasurement2";
        public const String VerticalMeasurementUnitOfMeasureID2 = "VerticalMeasurementUnitOfMeasureID2";
        public const String VerticalMeasurementPathID2 = "VerticalMeasurementPathID2";
        public const String VerticalMeasurementTypeID2 = "VerticalMeasurementTypeID2";
        public const String VerticalMeasurementSourceID2 = "VerticalMeasurementSourceID2";
    }

    public enum GetColNum_Well
    {
        FacilityID = 0,
        FacilityName = 1,
        InitialOperatorID = 2,
        CurrentOperatorID = 3,
        DataSourceOrganisationID = 4,
        OperatingEnvironmentID = 5,
        FacilityEventTypeID1 = 6,
        EffectiveDateTime1 = 7,
        FacilityEventTypeID2 = 8,
        EffectiveDateTime2 = 9,
        InterestTypeID = 10,
        VerticalMeasurementID1 = 11,
        VerticalMeasurementDescription1 = 12,
        VerticalMeasurement1 = 13,
        VerticalMeasurementUnitOfMeasureID1 = 14,
        VerticalMeasurementPathID1 = 15,
        VerticalMeasurementTypeID1 = 16,
        VerticalMeasurementSourceID1 = 17,
        VerticalMeasurementID2 = 18,
        VerticalMeasurementDescription2 = 19,
        VerticalMeasurement2 = 20,
        VerticalMeasurementUnitOfMeasureID2 = 21,
        VerticalMeasurementPathID2 = 22,
        VerticalMeasurementTypeID2 = 23,
        VerticalMeasurementSourceID2 = 24
    }

    public static class MandataryField_Well
    {
        public const String FacilityID = ModelWellbore.FacilityID;
        public const String FacilityName = ModelWellbore.FacilityName;
    }
}
