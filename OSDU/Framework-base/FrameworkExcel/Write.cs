﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using System.Text;

namespace FrameworkExcel
{
    public class ExcelWrite
    {

        public static String JsonFilePath = @"C:\Temp";

        public static void  WriteExecute(String filePath, IWorkbook workbook)
        {
            //Save the file
            using (FileStream fileStream = File.Create(filePath))
            {
                workbook.Write(fileStream);
            }
        }

        public static void WriteJsonExecute( String jsonString,String fileName)
        {
            if (!Directory.Exists(JsonFilePath))
            {
                // Try to create the directory.
                Directory.CreateDirectory(JsonFilePath);
            }

            //Save the file
            using (FileStream fileStream = File.Create(JsonFilePath+"/"+ fileName +".json"))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(jsonString);
                // Add some information to the file.
                fileStream.Write(info, 0, info.Length);
            }
        }

        public static void CreateCell(IRow currentRow, Int32 cellIndex, String value, XSSFCellStyle style)
        {
            ICell Cell = currentRow.CreateCell(cellIndex);
           
            Cell.SetCellValue(value);
            if(style!=null)
            Cell.CellStyle = style;
        }


    }
}
