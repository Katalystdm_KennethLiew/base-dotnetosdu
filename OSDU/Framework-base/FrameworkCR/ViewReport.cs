﻿using System.Drawing;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;

namespace FrameworkCR
{
    public partial class ViewReport : Form
    {
        public ViewReport(ReportClass rpt)
        {
            InitializeComponent();

            SetupReport();

            cryReportViewer.ReportSource = rpt;

            HideMainReportTab();

            cryReportViewer.Refresh();
        }

        private void SetupReport()
        {
            cryReportViewer.ShowCopyButton = false;
            cryReportViewer.ShowGroupTreeButton = false;
            cryReportViewer.ToolPanelView = ToolPanelViewType.None;
            cryReportViewer.ShowLogo = false;
            cryReportViewer.ShowParameterPanelButton = false;
            cryReportViewer.ShowRefreshButton = false;
            cryReportViewer.EnableDrillDown = false;
            cryReportViewer.AllowedExportFormats = (int)(CrystalDecisions.Shared.ViewerExportFormats.PdfFormat | CrystalDecisions.Shared.ViewerExportFormats.ExcelFormat | CrystalDecisions.Shared.ViewerExportFormats.RtfFormat | CrystalDecisions.Shared.ViewerExportFormats.WordFormat | CrystalDecisions.Shared.ViewerExportFormats.XLSXFormat);
        }

        //hides the "Main Report" tab
        //must be done AFTER setting ReportSource 
        private void HideMainReportTab()
        {
            System.Diagnostics.Debug.Assert(cryReportViewer.ReportSource != null, "You have to set the ReportSource first!");

            foreach (Control c1 in cryReportViewer.Controls)
            {
                if (c1 is PageView)
                {
                    foreach (Control c2 in ((PageView)c1).Controls)
                    {
                        if (c2 is TabControl)
                        {
                            ((TabControl)c2).ItemSize = new Size(0, 1);
                            ((TabControl)c2).SizeMode = TabSizeMode.Fixed;
                        }
                    }
                }
            }
        }
    }
}
