﻿using System;
using System.Collections.Generic;

namespace FrameworkKDM.Models
{
    //This is following SSI code logic public class IchgMediaMatchDmass in:
    //base-java\kdm-lib\persistence-lib\src\main\java\com\katalystdm\db\util\IchgMediaMatchDmass.java
    public static class ModelKdmMediaTypes
    {
        public static String _21_TRACK = "21-TRACK";
        public static String _3_5_FLOPPY = "3.5-FLOPPY";
        public static String _3480 = "3480";
        public static String _3490E = "3490E";
        public static String _3590 = "3590";
        public static String _3590E = "3590E";
        public static String _3592 = "3592";
        public static String _5_25_FLOPPY = "5.25FLOPPY";
        public static String _7_TRACK = "7-TRACK";
        public static String _8MM = "8MM";
        public static String _9_TRACK = "9-TRACK";
        public static String ANALOG = "ANALOG";
        public static String BLURAY = "BLU-RAY";
        public static String CDR = "CDR";
        public static String D2 = "D2";
        public static String DAT = "DAT";
        public static String DISK = "DISK";
        public static String DLT = "DLT";
        public static String DVD = "DVD";
        public static String FILM = "FILM";
        public static String LTO = "LTO";
        public static String LTO1 = "LTO1";
        public static String LTO2 = "LTO2";
        public static String LTO3 = "LTO3";
        public static String LTO4 = "LTO4";
        public static String LTO5 = "LTO5";
        public static String MICROFICHE = "MICROFICHE";
        public static String MICROFILM = "MICROFILM";
        public static String MYLAR = "MYLAR";
        public static String PAPER = "PAPER";
        public static String VHS = "VHS";
        public static String WORM = "WORM";
        public static String ZIP100 = "ZIP100";

        public static Dictionary<String, String> InterchangeDmassXref = new Dictionary<String, String>();

        static ModelKdmMediaTypes()
        {
            InterchangeDmassXref.Add("300", _9_TRACK);
            InterchangeDmassXref.Add("301", _9_TRACK);
            InterchangeDmassXref.Add("302", _9_TRACK);
            InterchangeDmassXref.Add("303", _9_TRACK);
            InterchangeDmassXref.Add("304", _3480);
            InterchangeDmassXref.Add("305", _3590);
            InterchangeDmassXref.Add("306", _8MM);
            InterchangeDmassXref.Add("307", DAT);
            InterchangeDmassXref.Add("308", DLT);
            InterchangeDmassXref.Add("309", _3_5_FLOPPY);
            InterchangeDmassXref.Add("310", _5_25_FLOPPY);
            InterchangeDmassXref.Add("311", CDR);
            InterchangeDmassXref.Add("312", WORM);
            InterchangeDmassXref.Add("313", MICROFICHE);
            InterchangeDmassXref.Add("314", MICROFICHE);
            InterchangeDmassXref.Add("315", MICROFILM);
            InterchangeDmassXref.Add("316", MICROFILM);
            InterchangeDmassXref.Add("317", FILM);
            InterchangeDmassXref.Add("318", MICROFILM);
            InterchangeDmassXref.Add("319", PAPER);
            InterchangeDmassXref.Add("320", PAPER);
            InterchangeDmassXref.Add("321", PAPER);
            InterchangeDmassXref.Add("322", _9_TRACK);
            InterchangeDmassXref.Add("323", _9_TRACK);
            InterchangeDmassXref.Add("324", _9_TRACK);
            InterchangeDmassXref.Add("325", _3490E);
            InterchangeDmassXref.Add("326", PAPER);
            InterchangeDmassXref.Add("327", PAPER);
            InterchangeDmassXref.Add("328", PAPER);
            InterchangeDmassXref.Add("329", ANALOG);
            InterchangeDmassXref.Add("338", _3592);
            InterchangeDmassXref.Add("339", _21_TRACK);
            InterchangeDmassXref.Add("399", _3590E);
            InterchangeDmassXref.Add("340", _21_TRACK);
            InterchangeDmassXref.Add("341", _21_TRACK);
            InterchangeDmassXref.Add("1080", ZIP100);
            InterchangeDmassXref.Add("1090", DISK);
            InterchangeDmassXref.Add("1100", DVD);
            InterchangeDmassXref.Add("1120", VHS);
            InterchangeDmassXref.Add("1121", D2);
            InterchangeDmassXref.Add("1122", FILM);
            InterchangeDmassXref.Add("1123", FILM);
            InterchangeDmassXref.Add("1124", _3590);
            InterchangeDmassXref.Add("1125", LTO);
            InterchangeDmassXref.Add("1126", PAPER);
            InterchangeDmassXref.Add("1127", PAPER);
            InterchangeDmassXref.Add("1137", _21_TRACK);
            InterchangeDmassXref.Add("1138", _21_TRACK);
            InterchangeDmassXref.Add("1140", BLURAY);
            InterchangeDmassXref.Add("1142", MYLAR);
        }

    }
}
