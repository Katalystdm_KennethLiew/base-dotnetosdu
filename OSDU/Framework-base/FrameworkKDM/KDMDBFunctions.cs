﻿using Framework;
using System;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Npgsql;

namespace FrameworkKDM
{
    public class KDMDBFunctions
    {
        //used for pref_type to separate global prefs between apps
        //pref_name is the 
        public enum APPS
        {
            OUTPUT_VOLUME_BUILDER,
            WELL_LOG_LOADER,
            NAV_LOADER
        }

        /// <summary>
        /// CLIENT options will store and retrieve different settings for each client.
        /// GLOBAL options will store and retrieve one single setting for all clients.
        /// </summary>
        public enum PreferenceLevel
        {
            CLIENT_VARCHAR,
            CLIENT_CLOB,
            GLOBAL_VARCHAR,
            GLOBAL_CLOB
        }

        private readonly DataSource _ds = null;

        private const String MULTIPLE = "MULTIPLE";
        private const String NONE = "N";

        private struct KDM_DB_PROC_NAMES
        {
            //owned by KELMAN
            public const String GET_NEXT_SEQUENCE = "ka_standard.get_next_sequence";

            //owned by KELMAN36
            public const String DOES_PREF_EXIST_CLIENT = "company_functions.does_preference_exist";
            public const String DOES_PREF_VAL_EXIST_CLIENT = "company_functions.does_preference_value_exist";
            public const String DOES_PREF_CLOB_EXIST_CLIENT = "company_functions.does_preference_value_exist_clob";
            public const String DOES_PREF_EXIST_GLOBAL = "k_user_functions.does_preference_exist_0";
            public const String DOES_PREF_VAL_EXIST_GLOBAL = "k_user_functions.does_preference_value_exist_0";
            public const String DOES_PREF_CLOB_EXIST_GLOBAL = "k_user_functions.does_preference_value_exist_clob_0";
            public const String SAVE_PREF_CLIENT = "company_functions.save_preference";
            public const String SAVE_PREF_GLOBAL = "k_user_functions.save_preference_0";
            public const String SAVE_PREF_VAL_CLIENT = "company_functions.save_preference_value";
            public const String SAVE_PREF_CLOB_CLIENT = "company_functions.save_preference_value_clob";
            public const String SAVE_PREF_VAL_GLOBAL = "k_user_functions.save_preference_value_0";
            public const String SAVE_PREF_CLOB_GLOBAL = "k_user_functions.save_preference_value_clob_0";
            public const String DEL_PREF_VAL_CLIENT = "company_functions.delete_preference_values";
            public const String DEL_PREF_CLIENT = "company_functions.delete_preference";
            public const String DEL_PREF_VAL_GLOBAL = "k_user_functions.delete_preference_values_0";
            public const String DEL_PREF_GLOBAL = "k_user_functions.delete_preference_0";
            public const String GET_LOGGED_ON_OWNER_BA_ID = "ba_functions.get_ba_id";

            //owned by DMASS38
            public const String ADD_JOB_NUM_INDEX_TO_INVENTORY = "inventory_util.add_index_to_item";
            public const String SET_ITEM_AS_NAV_SOURCE = "inventory_util.associate_nav_to_line";
        }

        public KDMDBFunctions(DataSource ds)
        {
            _ds = ds ?? throw new Exception("DataSource cannot be null.");
        }

        public String GetNextSequence(String sequenceName)
        {
            if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                return GetNextSequenceOracle(sequenceName);
            else
                return GetNextSequencePostgres(sequenceName);
        }

        private String GetNextSequenceOracle(String sequenceName)
        {
            String ret = String.Empty;

            using (OracleConnection conn = new OracleConnection(_ds.ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        OracleParameter p;

                        cmd.CommandText = KDM_DB_PROC_NAMES.GET_NEXT_SEQUENCE;

                        p = new OracleParameter();
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.ReturnValue;
                        p.Size = 2000;//IMPORTANT: you must set the size for return values or will get "ORA-06502: PL/SQL: numeric or value error: character string buffer too small\nORA-06512: at line 1"
                        cmd.Parameters.Add(p);//IMPORTANT: you must add any return value as the first parameter or you will always get a DBNull return value

                        p = new OracleParameter();
                        p.ParameterName = "p_seq_name";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = sequenceName;
                        cmd.Parameters.Add(p);

                        cmd.ExecuteNonQuery();

                        ret = cmd.Parameters[0].Value.ToString();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            return ret;
        }

        private String GetNextSequencePostgres(String sequenceName)
        {
            String ret = String.Empty;

            using (NpgsqlConnection conn = new NpgsqlConnection(_ds.ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        NpgsqlParameter p;

                        cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.GET_NEXT_SEQUENCE + "(:p_seq_name)";

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_seq_name";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Direction = ParameterDirection.Input;
                        p.Value = sequenceName;
                        cmd.Parameters.Add(p);

                        using (NpgsqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                                ret = dr.GetString(0);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            return ret;
        }

        private String DoesPreferenceExist(OracleConnection conn, PreferenceLevel prefLevel, String prefType, String prefName)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;
                String prefID = String.Empty;

                p = new OracleParameter();
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.ReturnValue;
                p.Size = 2000;//IMPORTANT: you must set the size for return values or will get "ORA-06502: PL/SQL: numeric or value error: character string buffer too small\nORA-06512: at line 1"
                cmd.Parameters.Add(p);//IMPORTANT: you must add any return value as the first parameter or you will always get a DBNull return value

                //if checking for either client pref
                //note here we don't care about varchar versus clob as the header record is the same
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_EXIST_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);

                }
                //global pref is a different function, no p_user_id param
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_EXIST_GLOBAL;
                }

                p = new OracleParameter();
                p.ParameterName = "p_pref_type";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefType;
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_name";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefName;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                prefID = cmd.Parameters[0].Value.ToString();

                if (String.IsNullOrEmpty(prefID) || prefID.ToUpper().Equals(NONE))
                    return String.Empty;
                else
                    return prefID;
            }
        }

        private String DoesPreferenceExist(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefType, String prefName)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;
                String prefID = String.Empty;

                //if checking for either client pref
                //note here we don't care about varchar versus clob as the header record is the same
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_EXIST_CLIENT + "(:p_owner_ba, :p_pref_type, :p_pref_name)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);

                }
                //global pref is a different function, no p_user_id param
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_EXIST_GLOBAL + "(:p_pref_type, :p_pref_name)";
                }

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_type";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefType;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_name";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefName;
                cmd.Parameters.Add(p);

                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        prefID = dr.GetString(0);
                }

                if (String.IsNullOrEmpty(prefID) || prefID.ToUpper().Equals(NONE))
                    return String.Empty;
                else
                    return prefID;
            }
        }

        private String InsertPreference(OracleConnection conn, PreferenceLevel prefLevel, String prefType, String prefName)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                OracleParameter p;
                cmd.CommandType = CommandType.StoredProcedure;

                p = new OracleParameter();
                p.ParameterName = "p_pref_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                //allow to pass null since inserting new
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_type";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefType;
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_name";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefName;
                cmd.Parameters.Add(p);

                //if inserting either client pref
                //note here we don't care about varchar versus clob as the header record is the same
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);

                }
                //global pref is a different function, no p_user_id param
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_GLOBAL;
                }

                p = new OracleParameter();
                p.ParameterName = "p_created_by";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = Environment.UserName;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                //retrieve the newly inserted prefID
                return DoesPreferenceExist(conn, prefLevel, prefType, prefName);
            }
        }

        private String InsertPreference(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefType, String prefName)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                NpgsqlParameter p;
                cmd.CommandType = CommandType.Text;

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = DBNull.Value;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_type";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefType;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_name";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefName;
                cmd.Parameters.Add(p);

                //if inserting either client pref
                //note here we don't care about varchar versus clob as the header record is the same
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = "EXEC " + KDM_DB_PROC_NAMES.SAVE_PREF_CLIENT + "(:p_pref_id, :p_pref_type, :p_pref_name, :p_owner_ba, :p_created_by)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);

                }
                //global pref is a different function, no p_user_id param
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = "EXEC " + KDM_DB_PROC_NAMES.SAVE_PREF_GLOBAL + "(:p_pref_id, :p_pref_type, :p_pref_name, :p_created_by)";
                }

                p = new NpgsqlParameter();
                p.ParameterName = "p_created_by";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = Environment.UserName;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                //retrieve the newly inserted prefID
                return DoesPreferenceExist(conn, prefLevel, prefType, prefName);
            }
        }

        private List<Object> DoesPreferenceValuesExist(OracleConnection conn, PreferenceLevel prefLevel, String prefID, String prefParam)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;
                List<Object> prefValueIDs = new List<Object>();

                p = new OracleParameter();
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.ReturnValue;
                p.Size = 2000;//IMPORTANT: you must set the size for return values or will get "ORA-06502: PL/SQL: numeric or value error: character string buffer too small\nORA-06512: at line 1"
                cmd.Parameters.Add(p);//IMPORTANT: you must add any return value as the first parameter or you will always get a DBNull return value

                //client varchar pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_VAL_EXIST_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);
                }
                //client clob pref
                else if (prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_CLOB_EXIST_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);
                }
                //global varchar pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_VAL_EXIST_GLOBAL;

                //global clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DOES_PREF_CLOB_EXIST_GLOBAL;

                p = new OracleParameter();
                p.ParameterName = "p_pref_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_parameter";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefParam;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                String ret = cmd.Parameters[0].Value.ToString();

                if (ret.ToUpper().Equals(MULTIPLE))
                {
                    using (DataSource ds = new DataSource(_ds.DatabaseLogon))
                    {
                        ds.AddSQL("SELECT PREF_VAL_ID FROM K_USER_PREFERENCE_VALUE WHERE PREF_ID = " + ds.AddParameter(prefID) + " AND PREF_PARAMETER = " + ds.AddParameter(prefParam));
                        using (DataTable dt = ds.GetDataTables()[0])
                        {
                            foreach (DataRow dr in dt.Rows)
                                prefValueIDs.Add(dr[0].ToString());
                        }
                    }
                }
                else if (!String.IsNullOrEmpty(ret) && !ret.ToUpper().Equals(NONE))
                    prefValueIDs.Add(ret);

                return prefValueIDs;
            }
        }

        private List<Object> DoesPreferenceValuesExist(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefID, String prefParam)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;
                List<Object> prefValueIDs = new List<Object>();

                //client varchar pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_VAL_EXIST_CLIENT + "(:p_owner_ba, :p_pref_id, :p_pref_parameter, :p_pref_value)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);
                }
                //client clob pref
                else if (prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_CLOB_EXIST_CLIENT + "(:p_owner_ba, :p_pref_id, :p_pref_parameter, :p_pref_value)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);
                }
                //global varchar pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_VAL_EXIST_GLOBAL + "(:p_pref_id, :p_pref_parameter, :p_pref_value)";

                //global clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DOES_PREF_CLOB_EXIST_GLOBAL + "(:p_pref_id, :p_pref_parameter, :p_pref_value)";

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_parameter";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefParam;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_value";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = DBNull.Value;
                cmd.Parameters.Add(p);

                String ret = String.Empty;
                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        ret = dr.GetString(0);
                }

                if (ret.ToUpper().Equals(MULTIPLE))
                {
                    using (DataSource ds = new DataSource(_ds.DatabaseLogon))
                    {
                        ds.AddSQL("SELECT PREF_VAL_ID FROM K_USER_PREFERENCE_VALUE WHERE PREF_ID = " + ds.AddParameter(prefID) + " AND PREF_PARAMETER = " + ds.AddParameter(prefParam));
                        using (DataTable dt = ds.GetDataTables()[0])
                        {
                            foreach (DataRow dr in dt.Rows)
                                prefValueIDs.Add(dr[0].ToString());
                        }
                    }
                }
                else if (!String.IsNullOrEmpty(ret) && !ret.ToUpper().Equals(NONE))
                    prefValueIDs.Add(ret);

                return prefValueIDs;
            }
        }

        private String InsertUpdatePreferenceValue(OracleConnection conn, PreferenceLevel prefLevel, String prefValueID, String prefID, String prefParam, String prefValue)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;

                p = new OracleParameter();
                p.ParameterName = "p_pref_val_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                if (!String.IsNullOrEmpty(prefValueID))
                    p.Value = prefValueID;
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                p = new OracleParameter();
                p.ParameterName = "p_pref_param";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefParam;
                cmd.Parameters.Add(p);

                //client varchar pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_VAL_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);
                }
                //client clob pref
                else if (prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_CLOB_CLIENT;

                    p = new OracleParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.OracleDbType = OracleDbType.Clob;
                    p.Size = prefValue.Length;
                    p.Direction = ParameterDirection.Input;
                    OracleClob clob = new OracleClob(cmd.Connection);
                    clob.Write(prefValue.ToCharArray(), 0, prefValue.Length);
                    p.Value = clob;
                    cmd.Parameters.Add(p);

                    p = new OracleParameter();
                    p.ParameterName = "p_owner_ba";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = GetLoggedOnOwnerBAIDOracle(conn);
                    cmd.Parameters.Add(p);
                }
                //global varchar pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_VAL_GLOBAL;

                    p = new OracleParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.OracleDbType = OracleDbType.Varchar2;
                    p.Direction = ParameterDirection.Input;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);
                }
                //global clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = KDM_DB_PROC_NAMES.SAVE_PREF_CLOB_GLOBAL;

                    p = new OracleParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.OracleDbType = OracleDbType.Clob;
                    p.Size = prefValue.Length;
                    p.Direction = ParameterDirection.Input;
                    OracleClob clob = new OracleClob(cmd.Connection);
                    clob.Write(prefValue.ToCharArray(), 0, prefValue.Length);
                    p.Value = clob;
                    cmd.Parameters.Add(p);
                }

                p = new OracleParameter();
                p.ParameterName = "p_created_by";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = Environment.UserName;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                //retrieve the newly inserted prefValueID
                List<Object> prefValueIDs = DoesPreferenceValuesExist(conn, prefLevel, prefID, prefParam);
                if (prefValueIDs.Count > 0)
                    return prefValueIDs[0].ToString();
                else
                    return String.Empty;
            }
        }

        private String InsertUpdatePreferenceValue(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefValueID, String prefID, String prefParam, String prefValue)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_val_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                if (!String.IsNullOrEmpty(prefValueID))
                    p.Value = prefValueID;
                else
                    p.Value = DBNull.Value;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_param";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefParam;
                cmd.Parameters.Add(p);

                //client varchar pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.SAVE_PREF_VAL_CLIENT + "(:p_pref_val_id, :p_pref_id, :p_pref_param, :p_pref_param_val, :p_owner_ba, :p_created_by)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);
                }
                //client clob pref
                else if (prefLevel == PreferenceLevel.CLIENT_CLOB)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.SAVE_PREF_CLOB_CLIENT + "(:p_pref_val_id, :p_pref_id, :p_pref_param, :p_pref_param_val, :p_owner_ba, :p_created_by)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Size = prefValue.Length;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_owner_ba";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = GetLoggedOnOwnerBAIDPostgres(conn);
                    cmd.Parameters.Add(p);
                }
                //global varchar pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.SAVE_PREF_VAL_GLOBAL + "(:p_pref_val_id, :p_pref_id, :p_pref_param, :p_pref_param_val, :p_created_by)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);
                }
                //global clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_CLOB)
                {
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.SAVE_PREF_CLOB_GLOBAL + "(:p_pref_val_id, :p_pref_id, :p_pref_param, :p_pref_param_val, :p_created_by)";

                    p = new NpgsqlParameter();
                    p.ParameterName = "p_pref_param_val";
                    p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                    p.Size = prefValue.Length;
                    p.Value = prefValue;
                    cmd.Parameters.Add(p);
                }

                p = new NpgsqlParameter();
                p.ParameterName = "p_created_by";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = Environment.UserName;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                //retrieve the newly inserted prefValueID
                List<Object> prefValueIDs = DoesPreferenceValuesExist(conn, prefLevel, prefID, prefParam);
                if (prefValueIDs.Count > 0)
                    return prefValueIDs[0].ToString();
                else
                    return String.Empty;
            }
        }

        public List<String> GetSavedPreference(PreferenceLevel prefLevel, String prefType, String prefName, String prefParam)
        {
            List<String> prefValues = new List<String>();

            using (ConnectionTypeHandler conn = new ConnectionTypeHandler(_ds))
            {
                try
                {
                    String prefID = String.Empty;
                    List<Object> prefValueIDs;

                    //first check if header preference record (K_USER_PREFERENCE) already exists or not
                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                        prefID = DoesPreferenceExist(conn.ConnectionOracle, prefLevel, prefType, prefName);
                    else
                        prefID = DoesPreferenceExist(conn.ConnectionPostgres, prefLevel, prefType, prefName);

                    //now we have a header preference record (K_USER_PREFERENCE) whether fetched or inserted
                    if (!String.IsNullOrEmpty(prefID))
                    {
                        if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                            prefValueIDs = DoesPreferenceValuesExist(conn.ConnectionOracle, prefLevel, prefID, prefParam);
                        else
                            prefValueIDs = DoesPreferenceValuesExist(conn.ConnectionPostgres, prefLevel, prefID, prefParam);

                        if (prefValueIDs.Count > 0)
                        {
                            using (DataSource ds = new DataSource(_ds.DatabaseLogon))
                            {
                                //client or global varchar pref
                                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_VARCHAR)
                                {
                                    ds.AddSQL("SELECT PREF_VALUE FROM K_USER_PREFERENCE_VALUE WHERE PREF_VAL_ID IN (" + ds.AddListOfINParameters(prefValueIDs) + ")");
                                    using (DataTable dt = ds.GetDataTables()[0])
                                    {
                                        foreach (DataRow dr in dt.Rows)
                                            prefValues.Add(dr[0].ToString());
                                    }
                                }
                                //client or global clob pref
                                else
                                {
                                    //build list of IN criteria
                                    List<Object> inCriteria = new List<Object>();
                                    foreach (Object obj in prefValueIDs)
                                        inCriteria.Add(obj);
                                    //add IN criteria as a single whereClause for input
                                    Dictionary<String, Object> whereClause = new Dictionary<String, Object> {{ "PREF_VAL_ID", inCriteria }};
                                    prefValues = ds.ReadCLOBs("PREF_VALUE_JSON", "K_USER_PREFERENCE_VALUE", whereClause);
                                }
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
                return prefValues;
            }
        }

        public String SavePreference(PreferenceLevel prefLevel, String prefType, String prefName, String prefParam, List<String> prefValues)
        {
            using (ConnectionTypeHandler conn = new ConnectionTypeHandler(_ds))
            {
                try
                {
                    String prefID = String.Empty;
                    List<Object> prefValueIDs;

                    //first check if header preference record (K_USER_PREFERENCE) already exists or not
                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                        prefID = DoesPreferenceExist(conn.ConnectionOracle, prefLevel, prefType, prefName);
                    else
                        prefID = DoesPreferenceExist(conn.ConnectionPostgres, prefLevel, prefType, prefName);

                    //if header preference record (K_USER_PREFERENCE) does not exist then must add one
                    if (String.IsNullOrEmpty(prefID))
                    {
                        if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                            prefID = InsertPreference(conn.ConnectionOracle, prefLevel, prefType, prefName);
                        else
                            prefID = InsertPreference(conn.ConnectionPostgres, prefLevel, prefType, prefName);
                    }

                    //now we have a header preference record (K_USER_PREFERENCE) whether fetched or inserted
                    if (String.IsNullOrEmpty(prefID))
                    {
                        return "Unable to find or insert the header preference record.";
                    }
                    else
                    {
                        //check if preference values already exist
                        if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                            prefValueIDs = DoesPreferenceValuesExist(conn.ConnectionOracle, prefLevel, prefID, prefParam);
                        else
                            prefValueIDs = DoesPreferenceValuesExist(conn.ConnectionPostgres, prefLevel, prefID, prefParam);

                        //if any existing
                        if (prefValueIDs.Count > 0)
                        {
                            //TODO: There is a bug here (looks like on DB-side) where we for now need to avoid having multi K_USER_PREFERENCE_VALUE records having different PREF_PARAMETER sets like below:
                            /*
                            PREF_VAL_ID, PREF_ID, PREF_PARAMETER, PREF_VALUE
                            554021	179483	TST_GLOBAL_VARCHAR_PARAM2	f
                            554019	179483	TST_GLOBAL_VARCHAR_PARAM2	d
                            554020	179483	TST_GLOBAL_VARCHAR_PARAM2	e
                            554016	179483	TST_GLOBAL_VARCHAR_PARAM1	1
                            554017	179483	TST_GLOBAL_VARCHAR_PARAM1	2
                            554018	179483	TST_GLOBAL_VARCHAR_PARAM1	3
                            */
                            //Working with DBA team on this... for now will avoid from calling codes.

                            //if new pref count same as existing then replace each one
                            if (prefValueIDs.Count == prefValues.Count)
                            {
                                for (Int32 i = 0; i < prefValueIDs.Count; i++)
                                {
                                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                                        InsertUpdatePreferenceValue(conn.ConnectionOracle, prefLevel, prefValueIDs[i].ToString(), prefID, prefParam, prefValues[i]);
                                    else
                                        InsertUpdatePreferenceValue(conn.ConnectionPostgres, prefLevel, prefValueIDs[i].ToString(), prefID, prefParam, prefValues[i]);
                                }
                            }
                            //otherwise, delete them all and insert new
                            else
                            {
                                if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                                    DeletePreferenceValues(conn.ConnectionOracle, prefLevel, prefID);
                                else
                                    DeletePreferenceValues(conn.ConnectionPostgres, prefLevel, prefID);

                                foreach (String prefVal in prefValues)
                                {
                                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                                        InsertUpdatePreferenceValue(conn.ConnectionOracle, prefLevel, String.Empty, prefID, prefParam, prefVal);
                                    else
                                        InsertUpdatePreferenceValue(conn.ConnectionPostgres, prefLevel, String.Empty, prefID, prefParam, prefVal);
                                }
                            }
                        }
                        //non existing, insert
                        else
                            foreach (String prefVal in prefValues)
                            {
                                if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                                    InsertUpdatePreferenceValue(conn.ConnectionOracle, prefLevel, String.Empty, prefID, prefParam, prefVal);
                                else
                                    InsertUpdatePreferenceValue(conn.ConnectionPostgres, prefLevel, String.Empty, prefID, prefParam, prefVal);
                            }

                        return String.Empty;
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void DeletePreferenceValues(OracleConnection conn, PreferenceLevel prefLevel, String prefID)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;

                //client varchar or clob pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DEL_PREF_VAL_CLIENT;
                //global varchar or clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DEL_PREF_VAL_GLOBAL;

                p = new OracleParameter();
                p.ParameterName = "p_pref_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }
        }

        private void DeletePreferenceValues(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefID)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;

                //client varchar or clob pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DEL_PREF_VAL_CLIENT + "(:p_pref_id)";

                //global varchar or clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DEL_PREF_VAL_GLOBAL + "(:p_pref_id)";

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }
        }

        public void DeletePreference(PreferenceLevel prefLevel, String prefType, String prefName)
        {
            using (ConnectionTypeHandler conn = new ConnectionTypeHandler(_ds))
            {
                try
                {
                    //first check if header preference record (K_USER_PREFERENCE) already exists or not
                    String prefID;
                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                        prefID = DoesPreferenceExist(conn.ConnectionOracle, prefLevel, prefType, prefName);
                    else
                        prefID = DoesPreferenceExist(conn.ConnectionPostgres, prefLevel, prefType, prefName);

                    if (!String.IsNullOrEmpty(prefID))
                    {
                        if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                        {
                            DeletePreferenceValues(conn.ConnectionOracle, prefLevel, prefID);
                            DeletePreference(conn.ConnectionOracle, prefLevel, prefID);
                        }
                        else
                        {
                            DeletePreferenceValues(conn.ConnectionPostgres, prefLevel, prefID);
                            DeletePreference(conn.ConnectionPostgres, prefLevel, prefID);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void DeletePreference(OracleConnection conn, PreferenceLevel prefLevel, String prefID)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;

                //client varchar or clob pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DEL_PREF_CLIENT;

                //global varchar or clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = KDM_DB_PROC_NAMES.DEL_PREF_GLOBAL;

                p = new OracleParameter();
                p.ParameterName = "p_pref_id";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }
        }

        private void DeletePreference(NpgsqlConnection conn, PreferenceLevel prefLevel, String prefID)
        {
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;

                //client varchar or clob pref
                if (prefLevel == PreferenceLevel.CLIENT_VARCHAR || prefLevel == PreferenceLevel.CLIENT_CLOB)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DEL_PREF_CLIENT + "(:p_pref_id)";

                //global varchar or clob pref
                else if (prefLevel == PreferenceLevel.GLOBAL_VARCHAR || prefLevel == PreferenceLevel.GLOBAL_CLOB)
                    cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.DEL_PREF_GLOBAL + "(:p_pref_id)";

                p = new NpgsqlParameter();
                p.ParameterName = "p_pref_id";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = prefID;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();
            }
        }

        private String GetLoggedOnOwnerBAIDOracle(OracleConnection conn)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                OracleParameter p;

                cmd.CommandText = KDM_DB_PROC_NAMES.GET_LOGGED_ON_OWNER_BA_ID;

                p = new OracleParameter();
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.ReturnValue;
                p.Size = 2000;//IMPORTANT: you must set the size for return values or will get "ORA-06502: PL/SQL: numeric or value error: character string buffer too small\nORA-06512: at line 1"
                cmd.Parameters.Add(p);//IMPORTANT: you must add any return value as the first parameter or you will always get a DBNull return value

                p = new OracleParameter();
                p.ParameterName = "p_db_user_name";
                p.OracleDbType = OracleDbType.Varchar2;
                p.Direction = ParameterDirection.Input;
                p.Value = _ds.DatabaseLogon.ID;
                cmd.Parameters.Add(p);

                cmd.ExecuteNonQuery();

                if (((Oracle.ManagedDataAccess.Types.OracleString)cmd.Parameters[0].Value).IsNull)
                    return String.Empty;
                else
                    return cmd.Parameters[0].Value.ToString();
            }
        }

        private String GetLoggedOnOwnerBAIDPostgres(NpgsqlConnection conn)
        {
            String ret = String.Empty;
            using (NpgsqlCommand cmd = new NpgsqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                NpgsqlParameter p;

                cmd.CommandText = "SELECT " + KDM_DB_PROC_NAMES.GET_LOGGED_ON_OWNER_BA_ID + "(:p_db_user_name)";

                p = new NpgsqlParameter();
                p.ParameterName = "p_db_user_name";
                p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                p.Value = _ds.DatabaseLogon.ID;
                cmd.Parameters.Add(p);

                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        ret = dr.GetString(0);
                }
                return ret;
            }
        }

        public String GetLoggedOnOwnerBAID()
        {
            String ret = String.Empty;
            using (ConnectionTypeHandler conn = new ConnectionTypeHandler(_ds))
            {
                try
                {
                    if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                        ret = GetLoggedOnOwnerBAIDOracle(conn.ConnectionOracle);
                    else
                        ret = GetLoggedOnOwnerBAIDPostgres(conn.ConnectionPostgres);
                }
                finally
                {
                    conn.Close();
                }
            }
            return ret;
        }

        public void AddJobNumAndIndexToInventory(String physID, String jobNum, String user)
        {
            if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                AddJobNumAndIndexToInventoryOracle(physID, jobNum, user);
            else
                AddJobNumAndIndexToInventoryPostgres(physID, jobNum, user);
        }

        private void AddJobNumAndIndexToInventoryOracle(String physID, String jobNum, String user)
        {
            using (OracleConnection conn = new OracleConnection(_ds.ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        OracleParameter p;

                        cmd.CommandText = KDM_DB_PROC_NAMES.ADD_JOB_NUM_INDEX_TO_INVENTORY;

                        p = new OracleParameter();
                        p.ParameterName = "p_pid";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = physID;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_job";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = jobNum;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_user";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = user;
                        cmd.Parameters.Add(p);

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void AddJobNumAndIndexToInventoryPostgres(String physID, String jobNum, String user)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(_ds.ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        NpgsqlParameter p;

                        cmd.CommandText = "EXEC " + KDM_DB_PROC_NAMES.ADD_JOB_NUM_INDEX_TO_INVENTORY + "(:p_pid, :p_job, :p_user)";

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_pid";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = physID;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_job";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = jobNum;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_user";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = user;
                        cmd.Parameters.Add(p);

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public void SetKDMItemAsSourceOfNavigation(String ownerBa, String kdmItemID, String kdmSeismicId, String userName)
        {
            if (_ds.DBType == DataSource.DatabaseTypes.ORACLE)
                SetKDMItemAsSourceOfNavigationOracle(ownerBa, kdmItemID, kdmSeismicId, userName);
            else
                SetKDMItemAsSourceOfNavigationPostgres(ownerBa, kdmItemID, kdmSeismicId, userName);
        }

        public void SetKDMItemAsSourceOfNavigationOracle(String ownerBa, String kdmItemID, String kdmSeismicId, String userName)
        {
            using (OracleConnection conn = new OracleConnection(_ds.ConnectionString))
            {
                conn.Open();
                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        OracleParameter p;
                        cmd.CommandType = CommandType.StoredProcedure;

                        p = new OracleParameter();
                        p.ParameterName = "p_owner_ba";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = ownerBa;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_piid";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = kdmItemID;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_ssid";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = kdmSeismicId;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_user";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = userName;
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_function";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = "ASSOCIATE";
                        cmd.Parameters.Add(p);

                        p = new OracleParameter();
                        p.ParameterName = "p_raise_errors";
                        p.OracleDbType = OracleDbType.Varchar2;
                        p.Direction = ParameterDirection.Input;
                        p.Value = "Y";
                        cmd.Parameters.Add(p);

                        cmd.CommandText = KDM_DB_PROC_NAMES.SET_ITEM_AS_NAV_SOURCE;

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public void SetKDMItemAsSourceOfNavigationPostgres(String ownerBa, String kdmItemID, String kdmSeismicId, String userName)
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(_ds.ConnectionString))
            {
                conn.Open();
                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        NpgsqlParameter p;
                        cmd.CommandType = CommandType.Text;

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_owner_ba";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = ownerBa;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_piid";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = kdmItemID;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_ssid";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = kdmSeismicId;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_user";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = userName;
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_function";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = "ASSOCIATE";
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_raise_errors";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = "Y";
                        cmd.Parameters.Add(p);

                        p = new NpgsqlParameter();
                        p.ParameterName = "p_user_id";
                        p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar;
                        p.Value = DBNull.Value;
                        cmd.Parameters.Add(p);

                        cmd.CommandText = "EXEC " + KDM_DB_PROC_NAMES.SET_ITEM_AS_NAV_SOURCE + "(:p_owner_ba, :p_piid, :p_ssid, :p_user, :p_function, :p_raise_errors, :p_user_id)";

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

    }
}
