﻿using Framework;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using System;

namespace FrameworkKDM
{
    internal class ConnectionTypeHandler : IDisposable
    {
        public OracleConnection ConnectionOracle { get; }

        public NpgsqlConnection ConnectionPostgres { get; }

        public DataSource DataSource { get; }

        //implementing IDisposable 
        private Boolean _disposed = false; // to detect redundant calls

        public ConnectionTypeHandler(DataSource ds)
        {
            DataSource = ds;
            if (DataSource.DBType == DataSource.DatabaseTypes.ORACLE)
            {
                ConnectionOracle = new OracleConnection(DataSource.ConnectionString);
                ConnectionOracle.Open();
            }
            else
            {
                ConnectionPostgres = new NpgsqlConnection(DataSource.ConnectionString);
                ConnectionPostgres.Open();
            }
        }

        public void Close()
        {
            if (DataSource.DBType == DataSource.DatabaseTypes.ORACLE)
                ConnectionOracle.Close();
            else
                ConnectionPostgres.Close();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (ConnectionOracle != null) ConnectionOracle.Dispose();
                    if (ConnectionPostgres != null) ConnectionPostgres.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

    }
}
