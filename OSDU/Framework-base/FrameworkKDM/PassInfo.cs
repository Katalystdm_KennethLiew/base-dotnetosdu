﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using Framework;

namespace FrameworkKDM
{
    //This is taken from public class PassInfo:
    //base-java\kdm-lib\util\src\main\java\com\kelman\passInfo\PassInfo.java
    public class PassInfo
    {
        /*
        PassInfo attempts to read this file in order to map a database name
        to a host name.  It looks for this file in the same directory as
        the password file.
        */
        private const String PASSINFO_PROPS_FILE = "PassInfo.properties";

        //The file containing passwords
        public const String PWD_FILE = ".pwd";

        private const String DEFAULT_PORT = "1521";

        public const String PWD_FILE_LOCATION = "L:\\";

        public enum PASSWORD_FILE_KEYS
        {
            SERVER,
            USER,
            FROM_OWNER,
            MPASS,
            MODE_NAME,
            DB_USER,
            DB_PASS,
            DB_NAME,
            IP,
            DESC
        }

        // Field names
        private readonly static String[] _fields = new String[]
        { "server",
        "user",
        "fromowner",
        "Mpass",
        "nodename",
        "dbuser",
        "dbpass",
        "dbname",
        "IP",
        "description" };

        // Field lengths in bytes
        private readonly static int[] _lengths = new int[]
        { 64,    // server
		64,    // user
		64,    // fromowner
		64,    // Mpass
		64,    // nodename
		64,    // dbuser
		64,    // dbpass
		64,    // dbname
		32,    // IP
		256 }; // description

        // Total size in bytes of each entry
        private readonly static int _totalLength =
        64 +   // server
        64 +   // user
        64 +   // fromowner
        64 +   // Mpass
        64 +   // nodename
        64 +   // dbuser
        64 +   // dbpass
        64 +   // dbname
        32 +   // IP
        256;   // description

        public List<Dictionary<String, String>> PassEntries { get; private set; } = new List<Dictionary<String, String>>();
        public Dictionary<String, String> DBHostEntries { get; private set; } = new Dictionary<String, String>();
        public Dictionary<String, String> DBPortEntries { get; private set; } = new Dictionary<String, String>();

        public String GetKeyName(PASSWORD_FILE_KEYS pfk)
        {
            return _fields[(int)pfk];
        }

        public Boolean LoadPasswords(DirectoryInfo path)
        {
            if (ReadPasswordFile(path))
            {
                if (ReadDBFile(path))
                {
                    //set default port values for each host if not given
                    foreach (KeyValuePair<String, String> kvp in DBHostEntries)
                        if (!DBPortEntries.ContainsKey(kvp.Key))
                            DBPortEntries.Add(kvp.Key, DEFAULT_PORT);
                    return true;
                }
            }
            return false;
        }

        private Boolean ReadDBFile(DirectoryInfo path)
        {
            if (path != null)
            {
                String filePathName = Path.Combine(path.FullName, PASSINFO_PROPS_FILE);
                if (File.Exists(filePathName))
                {
                    using (StreamReader file = new StreamReader(filePathName))
                    {
                        String line = file.ReadLine();
                        String db;
                        while (line != null)
                        {
                            if (!line.StartsWith("#") && !String.IsNullOrEmpty(line))
                            {
                                db = line.Split('=')[0].Split('.')[1];
                                if (line.Split('=')[0].Split('.')[2].Trim().Equals("host"))
                                    DBHostEntries.Add(db, line.Split('=')[1].Trim());
                                else if (line.Split('=')[0].Split('.')[2].Trim().Equals("port"))
                                    DBPortEntries.Add(db, line.Split('=')[1].Trim());
                            }
                            line = file.ReadLine();
                        }
                        file.Close();

                        if (DBHostEntries.Count > 0)
                            return true;
                    }
                }
            }
            return false;
        }

        private Boolean ReadPasswordFile(DirectoryInfo path)
        {
            if (path != null)
            {
                String filePathName = Path.Combine(path.FullName, PWD_FILE);
                if (File.Exists(filePathName))
                {
                    using (FileStream file = new FileStream(filePathName, FileMode.Open, FileAccess.Read))
                    {
                        byte[] buffer = new byte[_totalLength];
                        int totalRead = 0;
                        while (file.Read(buffer, 0, buffer.Length) > 0)
                        {
                            totalRead += buffer.Length;

                            int offset = 0;
                            Dictionary<String, String> entry = new Dictionary<String, String>();
                            for (int i = 0; i < _fields.Length; i++)
                            {
                                entry.Add(_fields[i], Decrypt(buffer, offset, _lengths[i]));
                                offset += _lengths[i];
                            }
                            PassEntries.Add(entry);

                        }
                        file.Close();

                        if (PassEntries.Count > 0)
                            return true;
                    }
                }
            }
            return false;
        }

        private String Decrypt(byte[] buffer, int start, int length)
        {
            byte[] decrypted = new byte[length];
            for (int dIdx = 0; dIdx < length; dIdx++)
            {
                // Early retirement on NULL character
                if (buffer[dIdx + start] == 0x00)
                    return System.Text.Encoding.ASCII.GetString(decrypted, 0, dIdx);

                decrypted[dIdx] = (byte)(Rol((byte)(buffer[dIdx + start] - (byte)0x20), 2) ^ (byte)0x4A);
                decrypted[dIdx] += (byte)0x20;

            }

            return System.Text.Encoding.ASCII.GetString(decrypted);
        }

        /*
        Goofy rotate left that actually only rotates the bottom 7 bits
        and preserves the sign bit. Copied from encrypt.cc
        */
        private byte Rol(byte ch, int num)
        {
            byte sign = (byte)(ch & (byte)0x80);
            for (int i = 0; i < num; i++)
            {
                ch = (byte)((byte)(ch << 1) | (byte)((ch & 0x40) >> 6));
            }
            return (byte)((byte)(ch & (byte)0x7f) | sign);
        }

        public DirectoryInfo PromptForPasswordFile(DirectoryInfo initialDir, Form parentForm)
        {
            DirectoryInfo ret = null;
            FileDialog fd = new OpenFileDialog();
            fd.Title = "Locate KDM Password File";
            if (initialDir != null)
                fd.InitialDirectory = initialDir.FullName;
            fd.CheckPathExists = true;
            fd.CheckFileExists = true;
            fd.Filter = "Password File (" + PassInfo.PWD_FILE + ")|" + PassInfo.PWD_FILE;
            if (fd.ShowDialog(parentForm) != DialogResult.Cancel)
            {
                ret = new DirectoryInfo(fd.FileName.Substring(0, fd.FileName.LastIndexOf("\\") + 1));
            }
            fd.Dispose();
            return ret;
        }

        public void LoadPasswordFileAsKEDIT(DatabaseLogonList dbLogonList, String userName)
        {
            userName += "_kedit";
            LoadPasswordFile(dbLogonList, userName);
        }

        public void LoadPasswordFile(DatabaseLogonList dbLogonList, String userName)
        {
            dbLogonList.Items.Clear();

            /*For outputting entries so can debug bad entries...
            if (PassEntries.Count > 0)
            {
                foreach (KeyValuePair<String, String> kvp in PassEntries[0])
                    System.Diagnostics.Debug.Write(kvp.Key + ",");
                System.Diagnostics.Debug.WriteLine(String.Empty);
                foreach (Dictionary<String, String> entry in PassEntries)
                {
                    foreach (KeyValuePair<String, String> kvp in entry)
                        System.Diagnostics.Debug.Write(kvp.Value + ",");
                    System.Diagnostics.Debug.WriteLine(String.Empty);
                }
            }
            */


            List<Dictionary<String, String>> foundEntries = new List<Dictionary<String, String>>();
            foreach (Dictionary<String, String> entry in PassEntries)
                foreach (KeyValuePair<String, String> kvp in entry)
                    if (kvp.Key.Equals(GetKeyName(PassInfo.PASSWORD_FILE_KEYS.DB_USER)) && kvp.Value.ToLower().Equals(userName.ToLower()))
                        foundEntries.Add(entry);

            foreach (Dictionary<String, String> entry in foundEntries)
            {
                DatabaseLogon dbLogon = new DatabaseLogon();

                foreach (KeyValuePair<String, String> kvp in entry)
                {
                    if (kvp.Key.Equals(GetKeyName(PassInfo.PASSWORD_FILE_KEYS.DB_USER)))
                        dbLogon.ID = kvp.Value;
                    else if (kvp.Key.Equals(GetKeyName(PassInfo.PASSWORD_FILE_KEYS.DB_PASS)))
                        dbLogon.PWD = kvp.Value;
                    else if (kvp.Key.Equals(GetKeyName(PassInfo.PASSWORD_FILE_KEYS.DB_NAME)))
                        dbLogon.ServiceName = kvp.Value;
                }

                foreach (KeyValuePair<String, String> kvpHost in DBHostEntries)
                    if (kvpHost.Key.Equals(dbLogon.ServiceName))
                        dbLogon.Host = kvpHost.Value;

                foreach (KeyValuePair<String, String> kvpPort in DBPortEntries)
                    if (kvpPort.Key.Equals(dbLogon.ServiceName))
                        dbLogon.Port = kvpPort.Value;

                if (!String.IsNullOrEmpty(dbLogon.ServiceName) && !String.IsNullOrEmpty(dbLogon.Host) && !String.IsNullOrEmpty(dbLogon.Port) && !String.IsNullOrEmpty(dbLogon.ID) && !String.IsNullOrEmpty(dbLogon.PWD))
                    dbLogonList.Items.Add(dbLogon);
            }
        }

    }
}
