﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace FrameworkKDM
{
    public class Crypto : IDisposable
    {
        //implementing IDisposable 
        private Boolean _disposed = false; // to detect redundant calls

        private byte[] GeneratePrivateKey()
        {
            byte[] key = new byte[32];
            for (int i = 0; i < key.Length; i++)
            {
                key[i] = Convert.ToByte((64 * 3) / (i + 1));
            }
            return key;
        }

        private byte[] GenerateIV()
        {
            byte[] key = new byte[16];
            for (int i = 0; i < key.Length; i++)
            {
                key[i] = Convert.ToByte((59 * 2) / (i + 1));
            }
            return key;
        }

        public String Encrypt(String value)
        {
            // Check arguments.
            if (String.IsNullOrEmpty(value))
                throw new ArgumentNullException("Nothing to encrypt.");

            byte[] encrypted;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = GeneratePrivateKey();
                aesAlg.IV = GenerateIV();

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(value);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream as a String
            return Convert.ToBase64String(encrypted);
        }

        public string Decrypt(String value)
        {
            string plaintext = null;

            if (String.IsNullOrEmpty(value))
                throw new ArgumentNullException("Nothing to dycrypt.");

            byte[] cipherText = Convert.FromBase64String(value);

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = GeneratePrivateKey();
                aesAlg.IV = GenerateIV();

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

    }
}
