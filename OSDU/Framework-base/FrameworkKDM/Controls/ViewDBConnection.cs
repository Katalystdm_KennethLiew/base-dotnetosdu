﻿using Framework;
using System;
using System.Windows.Forms;

namespace FrameworkKDM.Controls
{
    public partial class ViewDBConnection : Form
    {
        private readonly DatabaseLogon _dbLogon;

        public ViewDBConnection(ref DatabaseLogon dbLogon)
        {
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
            if (dbLogon == null)
                dbLogon = new DatabaseLogon();
            _dbLogon = dbLogon;
            PopulateControls();
        }

        private void PopulateControls()
        {
            txtHost.Text = _dbLogon.Host;
            txtService.Text = _dbLogon.ServiceName;
            txtPort.Text = _dbLogon.Port;
            txtUser.Text = _dbLogon.ID;
            txtPwd.Text = _dbLogon.PWD;
            ckbSavePwd.Checked = _dbLogon.SavePWD;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnApply_Click(object sender, EventArgs e)
        {
            if (txtHost.Text.Trim().Length == 0 || txtService.Text.Trim().Length == 0 || txtPort.Text.Trim().Length == 0 || txtUser.Text.Trim().Length == 0 || txtPwd.Text.Trim().Length == 0)
                MessageBox.Show("Please enter all settings.", btnApply.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _dbLogon.Host = txtHost.Text;
                _dbLogon.ServiceName = txtService.Text;
                _dbLogon.Port = txtPort.Text;
                _dbLogon.ID = txtUser.Text;
                _dbLogon.PWD = txtPwd.Text;
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void CkbSavePwd_CheckedChanged(object sender, EventArgs e)
        {
            _dbLogon.SavePWD = ckbSavePwd.Checked;
        }

    }
}
