﻿using Framework;
using Framework.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing;
using System.Text.Json;

namespace FrameworkKDM.Controls
{
    public partial class ViewSearchLookup : Form
    {
        public enum SEARCH_LOOKUPS
        {
            SURVEY,
            SEISMIC,
            WELL,
            BA
        }
        public struct SEARCH_RESULT_COLUMNS
        {
            public const String ID = "ID";
            public const String Name = "Name";

            public static class SEISMIC
            {
                public const String DimensionID = "DIMENSION";
                public const String SurveyID = "SEIS_ACQTN_SURVEY_ID";
            }
        }

        private const String PREF_TYPE = "SEARCH_LOOKUP";
        private readonly String _appPrefType;
        private const String PREF_PARAM_SEARCH_CRITERIA = "SEARCH_CRITERIA";
        private const String PREF_PARAM_RESULT_GRID = "RESULT_GRID";
        private const Int32 ROWS_PER_COLUMN = 4;

        private SearchPropertyList _searchPropertyList;
        private readonly ToolTip _toolTip = new ToolTip();
        private String _selectClause;
        private readonly DatabaseLogon _databaseLogon;
        private readonly Control _invokingControl;
        private Boolean _selectionMade = false;
        private readonly List<String> _columnDisplayIndexChanged = new List<String>();
        private Boolean _dataGridColumnClicked = false;

        public ViewSearchLookup(Control invokingControl, String applicationName, SEARCH_LOOKUPS searchLookup, DatabaseLogon databaseLogon, Boolean allowMultiSelect)
        {
            InitializeComponent();

            Text = Common.ConvertToPascalCasing(searchLookup.ToString() + " Lookup");

            //TODO: would like to allow cell select also so can copy/paste values out but doing that messes up GetSelectionAsDataTable()
            dgvResults.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            if (allowMultiSelect)
                dgvResults.MultiSelect = true;
            else
                dgvResults.MultiSelect = false;

            _invokingControl = invokingControl;

            _databaseLogon = databaseLogon;

            dgvResults.AllowUserToAddRows = false;
            dgvResults.AllowUserToOrderColumns = true;
            dgvResults.AddContextMenu(true, true, true, true, true, true, false, false, false, false, null, new EventHandler(SaveDisplayFormatEvent), null);
            dgvResults.AddColumnPreferencesEventHandler(new EventHandler<ContextMenuColumnPreferences.ColumnPreferencesEventArgs>(ColumnPreferencesEvent));
            dgvResults.ColumnDisplayIndexChanged += new DataGridViewColumnEventHandler(DataGridView_ColumnDisplayIndexChanged);
            dgvResults.MouseDown += new MouseEventHandler(DataGridView_MouseDown);

            //retrieve or create the SearchPropertyList
            //Must do this AFTER AddContextMenu() call above!
            _appPrefType = applicationName + "-" + PREF_TYPE;

            //get the SearchPropertyList and apply to the UI
            GetSearchPropertyList(searchLookup);
            PopulateUI(true);
        }

        private readonly Dictionary<String, String> _initialSearchCriteria = new Dictionary<String, String>();

        public void SetInitialSearchCriteria(String searchResultColumn, String criteria)
        {
            if (_initialSearchCriteria.ContainsKey(searchResultColumn))
                _initialSearchCriteria.Remove(searchResultColumn);
            _initialSearchCriteria.Add(searchResultColumn, criteria);
        }

        public void ColumnPreferencesEvent(object sender, ContextMenuColumnPreferences.ColumnPreferencesEventArgs e)
        {
            if (e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.GoTo)
            {
                ScrollControlIntoView(e.DataGridViewColumn.Name);
            }
            else if (e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.Hide
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.HideAllToRight
            //|| e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.Insert  **This commented out so can be handled by DataGridView_ColumnDisplayIndexChanged() instead**
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.UnHide
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.UnHideAll
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.Rename
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.ResetColumn
            || e.Operation == ContextMenuColumnPreferences.ColumnPreferencesEventArgs.OPERATION.ResetDisplayPrefs)
            {
                //for this set of operations we can refresh the search criteria from what the grid is now displaying
                UpdateSearchPropertyListFromGridDisplayFormat();
                PopulateUI(false);
                //if given a column, scroll to it's display index (which will now reflect correctly)
                if (e.DataGridViewColumn != null)
                    ScrollControlIntoView(e.DataGridViewColumn.Name);
            }
        }

        //Handling column re-ordering here since this will also cover a manual column drag/drop by end-user.
        //Above method of using ColumnPreferencesEventArgs will only cover it via ContextMenuColumnPreferences selection.
        private void DataGridView_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
        {
            if (!((DataGridViewStyled)e.Column.DataGridView).SuspendCustomActions && _dataGridColumnClicked)
            {
                _columnDisplayIndexChanged.Add(e.Column.Name);

                //checking this property ensures only the last column is considered
                //otherwise, all columns affected by a single column re-ordering are considered
                //we only need to execute this code once at the end, not for every column
                //but we need to know the very 1st column (which is the one to scroll to)
                PropertyInfo pi = typeof(DataGridViewColumn).GetProperty("DisplayIndexHasChanged", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (!e.Column.DataGridView.Columns.Cast<DataGridViewColumn>().Any(x => (bool)pi.GetValue(x)))
                {
                    UpdateSearchPropertyListFromGridDisplayFormat();
                    PopulateUI(false);
                    ScrollControlIntoView(_columnDisplayIndexChanged[0]);
                    _columnDisplayIndexChanged.Clear();
                }
            }
        }

        private void DataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            //this prevents the DataGridView_ColumnDisplayIndexChanged() event from being executed until the user clicks on a column
            //otherwise, it gets triggered during instantiation of the grid/form when we don't want it to
            _dataGridColumnClicked = true;
        }

        private void UpdateGridDisplayFormatFromSearchPropertyList()
        {
            //ensure to sort by display index
            SortedList<Int32, SearchProperty> sorted = new SortedList<Int32, SearchProperty>();
            foreach (SearchProperty sp in _searchPropertyList.SPs)
                sorted.Add(sp.DI, sp);

            foreach (SearchProperty sp in sorted.Values)
            {
                DataGridViewColumn dgc = dgvResults.Columns[sp.CN];
                //With Postgres, returned column names will come back as lower-case even when SQL is sent in upper-case so must upper-case here to ensure matches are found.
                dgc.Name = dgc.Name.ToUpper();
                //grid column display index is zero-based but SearchProperty.DI is one-based (on purpose)!!
                dgc.DisplayIndex = sp.DI - 1;
                dgc.HeaderText = sp.DN;
                dgc.Visible = !sp.H;
            }
        }

        private void UpdateSearchPropertyListFromGridDisplayFormat()
        {
            //re-create list as sorted by DisplayIndex since must set DisplayIndex in ascending order 
            //otherwise, doesn't work correctly since setting one column's DisplayIndex affects all the others
            SortedList<Int32, DataGridViewColumn> sorted = new SortedList<Int32, DataGridViewColumn>();
            foreach (DataGridViewColumn dgc in dgvResults.Columns)
            {
                sorted.Add(dgc.DisplayIndex, dgc);
            }

            foreach (DataGridViewColumn dgc in sorted.Values)
            {
                SearchProperty sp = _searchPropertyList.SPs.Where(x => x.CN.Equals(dgc.Name)).First();
                sp.DI = dgc.DisplayIndex + 1;//grid column display index is zero-based but SearchProperty.DI is one-based (on purpose)!!
                sp.DN = dgc.HeaderText;
                sp.H = !dgc.Visible;
            }
        }

        private void ScrollControlIntoView(String name)
        {
            SearchProperty sp = GetSearchPropertyFromName(name);
            Control ctrl = GetControlFromSearchProperty(sp);
            if (ctrl != null)
            {
                panel.ScrollControlIntoView(ctrl);
                ctrl.Focus();
                //2nd scroll here ensures label is displayed if happen to be scrolling right -> left
                //has no effect if scrolling left -> right
                panel.ScrollControlIntoView(GetLabelFromSearchProperty(sp));
            }
        }

        public void SaveDisplayFormatEvent(object sender, EventArgs e)
        {
            String ret = String.Empty;
            try
            {
                using (DataSource ds = new DataSource(_databaseLogon))
                {
                    KDMDBFunctions dbf = new KDMDBFunctions(ds);
                    ret = dbf.SavePreference(KDMDBFunctions.PreferenceLevel.GLOBAL_CLOB, _appPrefType, _searchPropertyList.Type.ToString(), PREF_PARAM_RESULT_GRID, new List<String>() { ((DataGridViewStyled)sender).RetrieveJsonFormat(_searchPropertyList.Type.ToString()) });
                    //here also save the SearchPropertyList
                    //TODO: pending business feedback may want to save these 2 things separately from each other
                    //But there are further complications to do this such as renaming columns... likely still need to sync some properties but not others like display index
                    //Note usage of UpdateGridDisplayFormatFromSearchPropertyList() and UpdateSearchPropertyListFromGridDisplayFormat()
                    //If separate these, we can likely overload ContextMenuColumnPreferences() instantiator to accept a ViewSearchLookup (instead of DataGridView) and have 2 context menus.
                    if (String.IsNullOrEmpty(ret))
                        ret = dbf.SavePreference(KDMDBFunctions.PreferenceLevel.GLOBAL_CLOB, _appPrefType, _searchPropertyList.Type.ToString(), PREF_PARAM_SEARCH_CRITERIA, new List<String>() { JsonSerializer.Serialize<SearchPropertyList>(_searchPropertyList) });
                }
            }
            catch (Exception ex)
            {
                ret = ex.ToString();
            }
            finally
            {
                if (!String.IsNullOrEmpty(ret))
                    MessageBox.Show(this, "Unexpected error saving display preferences:\r\n" + ret, "Saving Display Preferences", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public DialogResult ShowDialogAtInvokingControlLocation()
        {
            if (_invokingControl != null)
            {
                if (_invokingControl.Parent != null)
                    this.Location = _invokingControl.Parent.PointToScreen(_invokingControl.Location);
                else
                    this.Location = _invokingControl.FindForm().PointToScreen(_invokingControl.Location);

                StartPosition = FormStartPosition.Manual;
                return this.ShowDialog(_invokingControl.FindForm());
            }
            else
            {
                StartPosition = FormStartPosition.CenterScreen;
                return this.ShowDialog();
            }
        }

        public DataTable GetSelectionAsDataTable()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(SEARCH_RESULT_COLUMNS.ID, typeof(String));
            dt.Columns.Add(SEARCH_RESULT_COLUMNS.Name, typeof(String));

            foreach (DataGridViewRow dgr in dgvResults.SelectedRows)
            {
                DataRow drNew = dt.NewRow();
                if (_searchPropertyList.Type == SEARCH_LOOKUPS.SURVEY)
                {
                    drNew[SEARCH_RESULT_COLUMNS.ID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.ID)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.Name] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.Name)].Value.ToString();
                }
                else if (_searchPropertyList.Type == SEARCH_LOOKUPS.SEISMIC)
                {
                    dt.Columns.Add(SEARCH_RESULT_COLUMNS.SEISMIC.DimensionID, typeof(String));
                    dt.Columns.Add(SEARCH_RESULT_COLUMNS.SEISMIC.SurveyID, typeof(String));

                    drNew[SEARCH_RESULT_COLUMNS.ID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.ID)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.Name] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.Name)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.SEISMIC.DimensionID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.SEISMIC.DimensionID)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.SEISMIC.SurveyID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.SEISMIC.SurveyID)].Value.ToString();
                }
                else if (_searchPropertyList.Type == SEARCH_LOOKUPS.WELL)
                {
                    drNew[SEARCH_RESULT_COLUMNS.ID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.ID)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.Name] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.Name)].Value.ToString();
                }
                else if (_searchPropertyList.Type == SEARCH_LOOKUPS.BA)
                {
                    drNew[SEARCH_RESULT_COLUMNS.ID] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.ID)].Value.ToString();
                    drNew[SEARCH_RESULT_COLUMNS.Name] = dgr.Cells[GetColumnNameFromSearchResultColumn(SEARCH_RESULT_COLUMNS.Name)].Value.ToString();
                }
                dt.Rows.Add(drNew);
            }
            dt.AcceptChanges();

            //set the primary key so any calling code can perform a .Merge() against a previously returned DataTable
            //so rows will be added but duplicates ignored
            dt.PrimaryKey = new DataColumn[] { dt.Columns[SEARCH_RESULT_COLUMNS.ID] };

            return dt;
        }

        private String GetColumnNameFromSearchResultColumn(String searchResultColumn)
        {
            if (_searchPropertyList.Type == SEARCH_LOOKUPS.SURVEY)
            {
                if (searchResultColumn == SEARCH_RESULT_COLUMNS.ID)
                    return "SEIS_ACQTN_SURVEY_ID";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.Name)
                    return "ACQTN_SURVEY_NAME";
                else
                    throw new NotSupportedException(_searchPropertyList.Type.ToString() + " + " + searchResultColumn);
            }
            else if (_searchPropertyList.Type == SEARCH_LOOKUPS.SEISMIC)
            {
                if (searchResultColumn == SEARCH_RESULT_COLUMNS.ID)
                    return "SEIS_LINE_ID";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.Name)
                    return "PREFERRED_NAME";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.SEISMIC.DimensionID)
                    return "DIMENSION";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.SEISMIC.SurveyID)
                    return "SEIS_ACQTN_SURVEY_ID";
                else
                    throw new NotSupportedException(_searchPropertyList.Type.ToString() + " + " + searchResultColumn);
            }
            else if (_searchPropertyList.Type == SEARCH_LOOKUPS.WELL)
            {
                if (searchResultColumn == SEARCH_RESULT_COLUMNS.ID)
                    return "UWI";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.Name)
                    return "WELL_NAME";
                else
                    throw new NotSupportedException(_searchPropertyList.Type.ToString() + " + " + searchResultColumn);
            }
            else if (_searchPropertyList.Type == SEARCH_LOOKUPS.BA)
            {
                if (searchResultColumn == SEARCH_RESULT_COLUMNS.ID)
                    return "BUSINESS_ASSOCIATE";
                else if (searchResultColumn == SEARCH_RESULT_COLUMNS.Name)
                    return "BA_NAME";
                else
                    throw new NotSupportedException(_searchPropertyList.Type.ToString() + " + " + searchResultColumn);
            }
            else
                throw new NotSupportedException(_searchPropertyList.Type.ToString() + " + " + searchResultColumn);
        }

        private void PopulateUI(Boolean refreshGrid)
        {
            panel.Controls.Clear();

            if (_searchPropertyList != null && _searchPropertyList.SPs.Count > 0)
            {
                //ensure to sort by display index
                SortedList<Int32, SearchProperty> sorted = new SortedList<Int32, SearchProperty>();
                foreach (SearchProperty sp in _searchPropertyList.SPs)
                    sorted.Add(sp.DI, sp);

                _selectClause = String.Empty;
                Int32 di = 1;
                _toolTip.InitialDelay = 0;
                _toolTip.AutoPopDelay = 32767;

                Graphics g = this.CreateGraphics();

                foreach (SearchProperty sp in sorted.Values)
                {
                    //TODO: limit select clause to what is not hidden
                    //Need to consider Unhide & UnhideAll... tracking these will take some extra logic but should be doable since always initially fetching a single/empty row
                    _selectClause += sp.CN + ", ";

                    if (!sp.H)
                    {
                        Label lbl = new Label();
                        lbl.Name = GetLabelNameForSearchProperty(sp);
                        lbl.Size = new Size(117, 20);
                        lbl.Location = GetControlLocationFromDisplayIndex(di, true);
                        lbl.Text = sp.DN + ":";
                        //if label text is too long for lable width then shorten & add ... to the end until fits
                        if (g.MeasureString(lbl.Text, lbl.Font).Width > lbl.Width)
                        {
                            while (g.MeasureString(lbl.Text + "...", lbl.Font).Width > lbl.Width)
                                lbl.Text = lbl.Text.Substring(0, lbl.Text.Length - 1);
                            lbl.Text += "...";
                        }
                        lbl.TextAlign = ContentAlignment.MiddleRight;
                        panel.Controls.Add(lbl);
                        _toolTip.SetToolTip(lbl, sp.DN);

                        //TODO: need to add support for drop downs and extended lookups
                        //SearchProperty.PROPERTY_TYPE

                        TextBoxStyled txt = new TextBoxStyled();
                        txt.Name = GetControlNameForSearchProperty(sp);
                        txt.Size = new Size(130, 20);
                        txt.Location = GetControlLocationFromDisplayIndex(di, false);
                        txt.TextAlign = HorizontalAlignment.Left;
                        if (sp.DT == SearchProperty.DATA_TYPE.NUMBER)
                            txt.Validating += new CancelEventHandler(this.TxtBox_ValidatingAsNumber);
                        else if (sp.DT == SearchProperty.DATA_TYPE.DECIMAL)
                            txt.Validating += new CancelEventHandler(this.TxtBox_ValidatingAsDecimal);
                        else if (sp.DT == SearchProperty.DATA_TYPE.DATE)
                            txt.Validating += new CancelEventHandler(this.TxtBox_ValidatingAsDate);
                        panel.Controls.Add(txt);
                        txt.TabIndex = di;

                        //allow enter key to execute search criteria
                            txt.KeyDown += new KeyEventHandler(ControlSearchCriteria_KeyDown);

                        //TODO: add DateTimePickerStyled as a button

                        di++;
                    }
                }
                _selectClause = _selectClause.TrimEnd(new char[]{' ', ','});

                Int32 i = _searchPropertyList.SPs.Count + 1;
                btnSearch.TabIndex = i++;
                btnClear.TabIndex = i++;
                btnSelectAndClose.TabIndex = i++;
                btnCancel.TabIndex = i++;
                dgvResults.TabIndex = i++;

                //when first open, need to fetch an empty record so user can see grid columns in order to use GoTo, Insert, Hide, etc.
                //the search criteria boxes will reflect the grid columns for these purposes
                if (refreshGrid)
                {
                    using (DataSource ds = new DataSource(_databaseLogon))
                    {
                        ds.AddSQL("SELECT " + _selectClause + " FROM " + _searchPropertyList.TV + " WHERE 1 <> 1");
                        SetResultGrid(ds.GetDataTables()[0]);
                    }
                }
            }
        }

        private void ControlSearchCriteria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch.PerformClick();
        }

        private void TxtBox_ValidatingAsNumber(object sender, CancelEventArgs e)
        {
            String val = ((TextBox)sender).Text.Trim();
            if (val.Length > 0)
            {
                if (val.Contains(","))
                {
                    ((TextBox)sender).Text = String.Empty;
                    foreach (String splitVal in val.Split(','))
                    {
                        String orVal = splitVal.Trim();
                        if (orVal.Length > 0)
                        {
                            if (!Int64.TryParse(orVal, out Int64 i))
                            {
                                e.Cancel = true;
                                MessageBox.Show(this, "Please enter a numeric value without decimals.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {
                                ((TextBox)sender).Text += i.ToString() + ", ";
                            }
                        }
                    }
                    ((TextBox)sender).Text = ((TextBox)sender).Text.TrimEnd(new char[] { ' ', ',' });
                }
                else
                {
                    if (!Int64.TryParse(val, out Int64 i))
                    {
                        e.Cancel = true;
                        MessageBox.Show(this, "Please enter a numeric value without decimals.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ((TextBox)sender).Text = i.ToString();
                    }
                }
            }
        }

        private void TxtBox_ValidatingAsDecimal(object sender, CancelEventArgs e)
        {
            String val = ((TextBox)sender).Text.Trim();
            if (val.Length > 0)
            {
                if (val.Contains(","))
                {
                    ((TextBox)sender).Text = String.Empty;
                    foreach (String splitVal in val.Split(','))
                    {
                        String orVal = splitVal.Trim();
                        if (orVal.Length > 0)
                        {
                            if (!Decimal.TryParse(orVal, out Decimal d))
                            {
                                e.Cancel = true;
                                MessageBox.Show(this, "Please enter a numeric value.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {
                                ((TextBox)sender).Text += d.ToString() + ", ";
                            }
                        }
                    }
                    ((TextBox)sender).Text = ((TextBox)sender).Text.TrimEnd(new char[] { ' ', ',' });
                }
                else
                {
                    if (!Decimal.TryParse(val, out Decimal d))
                    {
                        e.Cancel = true;
                        MessageBox.Show(this, "Please enter a numeric value.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ((TextBox)sender).Text = d.ToString();
                    }
                }
            }
        }

        private void TxtBox_ValidatingAsDate(object sender, CancelEventArgs e)
        {
            String val = ((TextBox)sender).Text.Trim();
            if (val.Length > 0)
            {
                if (val.Contains(","))
                {
                    ((TextBox)sender).Text = String.Empty;
                    foreach (String splitVal in val.Split(','))
                    {
                        String orVal = splitVal.Trim();
                        if (orVal.Length > 0)
                        {
                            if (!DateTime.TryParse(orVal, out DateTime dt))
                            {
                                e.Cancel = true;
                                MessageBox.Show(this, "Please enter a date value.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {
                                ((TextBox)sender).Text += dt.ToString("yyyy-MM-dd") + ", ";
                            }
                        }
                    }
                    ((TextBox)sender).Text = ((TextBox)sender).Text.TrimEnd(new char[] { ' ', ',' });
                }
                else
                {
                    if (!DateTime.TryParse(val, out DateTime dt))
                    {
                        e.Cancel = true;
                        MessageBox.Show(this, "Please enter a date value.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        ((TextBox)sender).Text = dt.ToString("yyyy-MM-dd");
                    }
                }
            }
        }

        private String GetControlNameForSearchProperty(SearchProperty sp)
        {
            if (sp.DT == SearchProperty.DATA_TYPE.TEXT || sp.DT == SearchProperty.DATA_TYPE.NUMBER || sp.DT == SearchProperty.DATA_TYPE.DECIMAL || sp.DT == SearchProperty.DATA_TYPE.DATE)
                return "txt" + sp.CN;
            else
                throw new NotSupportedException(sp.DT.ToString() + " and " + sp.PT.ToString());
        }

        private String GetLabelNameForSearchProperty(SearchProperty sp)
        {
            if (sp.DT == SearchProperty.DATA_TYPE.TEXT || sp.DT == SearchProperty.DATA_TYPE.NUMBER || sp.DT == SearchProperty.DATA_TYPE.DECIMAL || sp.DT == SearchProperty.DATA_TYPE.DATE)
                return "lbl" + sp.CN;
            else
                throw new NotSupportedException(sp.DT.ToString() + " and " + sp.PT.ToString());
        }

        private SearchProperty GetSearchPropertyFromName(String name)
        {
            return _searchPropertyList.SPs.Where(x => x.CN.Equals(name)).First();
        }

        private SearchProperty GetSearchPropertyFromControl(Control ctrl)
        {
            return _searchPropertyList.SPs.Where(x => x.CN.Equals(ctrl.Name.Substring(3))).First();
        }

        private Control GetControlFromSearchProperty(SearchProperty sp)
        {
            foreach (Control ctrl in panel.Controls)
                if (ctrl.Name.Equals(GetControlNameForSearchProperty(sp)))
                    return ctrl;
            return null;
        }

        private Control GetLabelFromSearchProperty(SearchProperty sp)
        {
            foreach (Control ctrl in panel.Controls)
                if (ctrl is Label && ctrl.Name.Equals(GetLabelNameForSearchProperty(sp)))
                    return ctrl;
            return null;
        }

        private void GetSearchPropertyList(SEARCH_LOOKUPS searchLookup)
        {
            List<String> excludes = new List<String>();
            using (DataSource ds = new DataSource(_databaseLogon))
            {
                //check for any saved SearchPropertyList
                KDMDBFunctions dbf = new KDMDBFunctions(ds);
                List<String> json = dbf.GetSavedPreference(KDMDBFunctions.PreferenceLevel.GLOBAL_CLOB, _appPrefType, searchLookup.ToString(), PREF_PARAM_SEARCH_CRITERIA);
                if (json.Count > 0)
                {
                    //if saved then deserialize and use as is
                    _searchPropertyList = JsonSerializer.Deserialize<SearchPropertyList>(json[0]);
                }
                //not saved, create new SearchPropertyList by fetching empty record of columns
                else if (searchLookup== SEARCH_LOOKUPS.WELL)
                {
                    //TODO: use a hard-coded set here when there is none, this could be managed better somehow 
                    _searchPropertyList = JsonSerializer.Deserialize<SearchPropertyList>("{\"Type\":2,\"K\":null,\"TV\":\"KVS_WELL\",\"SPs\":[{\"CN\":\"UWI\",\"DN\":\"KDM Well ID\",\"DI\":1,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ABANDONMENT_DATE\",\"DN\":\"Abandonment Date\",\"DI\":10,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"ACTIVE_IND\",\"DN\":\"Active Ind\",\"DI\":11,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ASSIGNED_FIELD\",\"DN\":\"Assigned Field\",\"DI\":12,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"BASE_NODE_ID\",\"DN\":\"Base Node Id\",\"DI\":13,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"BOTTOM_HOLE_LATITUDE\",\"DN\":\"Bottom Hole Latitude\",\"DI\":14,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"BOTTOM_HOLE_LONGITUDE\",\"DN\":\"Bottom Hole Longitude\",\"DI\":15,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"CASING_FLANGE_ELEV\",\"DN\":\"Casing Flange Elev\",\"DI\":16,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"CASING_FLANGE_ELEV_OUOM\",\"DN\":\"Casing Flange Elev Ouom\",\"DI\":17,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"COMPLETION_DATE\",\"DN\":\"Completion Date\",\"DI\":9,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"CONFIDENTIAL_DATE\",\"DN\":\"Confidential Date\",\"DI\":18,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"CONFIDENTIAL_DEPTH\",\"DN\":\"Confidential Depth\",\"DI\":19,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"CONFIDENTIAL_DEPTH_OUOM\",\"DN\":\"Confidential Depth Ouom\",\"DI\":20,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"CONFIDENTIAL_TYPE\",\"DN\":\"Confidential Type\",\"DI\":21,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"CONFID_STRAT_NAME_SET_ID\",\"DN\":\"Confid Strat Name Set Id\",\"DI\":22,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"CONFID_STRAT_UNIT_ID\",\"DN\":\"Confid Strat Unit Id\",\"DI\":23,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"COUNTRY\",\"DN\":\"Country\",\"DI\":24,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"COUNTY\",\"DN\":\"County\",\"DI\":25,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"CURRENT_CLASS\",\"DN\":\"Current Class\",\"DI\":26,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"CURRENT_STATUS_DATE\",\"DN\":\"Current Status Date\",\"DI\":27,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"DEEPEST_DEPTH\",\"DN\":\"Deepest Depth\",\"DI\":28,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"DEEPEST_DEPTH_OUOM\",\"DN\":\"Deepest Depth Ouom\",\"DI\":29,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DEPTH_DATUM\",\"DN\":\"Depth Datum\",\"DI\":30,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DEPTH_DATUM_ELEV\",\"DN\":\"Depth Datum Elev\",\"DI\":31,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"DEPTH_DATUM_ELEV_OUOM\",\"DN\":\"Depth Datum Elev Ouom\",\"DI\":32,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DERRICK_FLOOR_ELEV\",\"DN\":\"Derrick Floor Elev\",\"DI\":33,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"DERRICK_FLOOR_ELEV_OUOM\",\"DN\":\"Derrick Floor Elev Ouom\",\"DI\":34,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DIFFERENCE_LAT_MSL\",\"DN\":\"Difference Lat Msl\",\"DI\":35,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"DISCOVERY_IND\",\"DN\":\"Discovery Ind\",\"DI\":36,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DISTRICT\",\"DN\":\"District\",\"DI\":37,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"DRILL_TD\",\"DN\":\"Drill Td\",\"DI\":38,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"DRILL_TD_OUOM\",\"DN\":\"Drill Td Ouom\",\"DI\":39,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"EFFECTIVE_DATE\",\"DN\":\"Effective Date\",\"DI\":40,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"ELEV_REF_DATUM\",\"DN\":\"Elev Ref Datum\",\"DI\":41,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ENVIRONMENT_TYPE\",\"DN\":\"Environment Type\",\"DI\":42,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"EXPIRY_DATE\",\"DN\":\"Expiry Date\",\"DI\":43,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"FAULTED_IND\",\"DN\":\"Faulted Ind\",\"DI\":44,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"FINAL_DRILL_DATE\",\"DN\":\"Final Drill Date\",\"DI\":45,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"FINAL_TD\",\"DN\":\"Final Td\",\"DI\":46,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"FINAL_TD_OUOM\",\"DN\":\"Final Td Ouom\",\"DI\":47,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"GEOGRAPHIC_REGION\",\"DN\":\"Geographic Region\",\"DI\":48,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"GEOLOGIC_PROVINCE\",\"DN\":\"Geologic Province\",\"DI\":49,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"GROUND_ELEV\",\"DN\":\"Ground Elev\",\"DI\":50,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"GROUND_ELEV_OUOM\",\"DN\":\"Ground Elev Ouom\",\"DI\":51,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"GROUND_ELEV_TYPE\",\"DN\":\"Ground Elev Type\",\"DI\":52,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"INITIAL_CLASS\",\"DN\":\"Initial Class\",\"DI\":53,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"KB_ELEV\",\"DN\":\"Kb Elev\",\"DI\":54,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"KB_ELEV_OUOM\",\"DN\":\"Kb Elev Ouom\",\"DI\":55,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"LEASE_NAME\",\"DN\":\"Lease Name\",\"DI\":56,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"LEASE_NUM\",\"DN\":\"Lease Num\",\"DI\":57,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"LEGAL_SURVEY_TYPE\",\"DN\":\"Legal Survey Type\",\"DI\":58,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"LOCATION_TYPE\",\"DN\":\"Location Type\",\"DI\":59,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"LOG_TD\",\"DN\":\"Log Td\",\"DI\":60,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"LOG_TD_OUOM\",\"DN\":\"Log Td Ouom\",\"DI\":61,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"MAX_TVD\",\"DN\":\"Max Tvd\",\"DI\":62,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"MAX_TVD_OUOM\",\"DN\":\"Max Tvd Ouom\",\"DI\":63,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"NET_PAY\",\"DN\":\"Net Pay\",\"DI\":64,\"H\":false,\"DT\":1,\"PT\":0},{\"CN\":\"NET_PAY_OUOM\",\"DN\":\"Net Pay Ouom\",\"DI\":65,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"OLDEST_STRAT_AGE\",\"DN\":\"Oldest Strat Age\",\"DI\":66,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"OLDEST_STRAT_NAME_SET_AGE\",\"DN\":\"Oldest Strat Name Set Age\",\"DI\":67,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"OLDEST_STRAT_NAME_SET_ID\",\"DN\":\"Oldest Strat Name Set Id\",\"DI\":68,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"OLDEST_STRAT_UNIT_ID\",\"DN\":\"Oldest Strat Unit Id\",\"DI\":69,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"OPERATOR\",\"DN\":\"Operator\",\"DI\":70,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PLATFORM_ID\",\"DN\":\"Platform Id\",\"DI\":71,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PLATFORM_SF_TYPE\",\"DN\":\"Platform Sf Type\",\"DI\":72,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PLOT_NAME\",\"DN\":\"Plot Name\",\"DI\":73,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PLOT_SYMBOL\",\"DN\":\"Plot Symbol\",\"DI\":74,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PLUGBACK_DEPTH\",\"DN\":\"Plugback Depth\",\"DI\":75,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"PLUGBACK_DEPTH_OUOM\",\"DN\":\"Plugback Depth Ouom\",\"DI\":76,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PPDM_GUID\",\"DN\":\"Ppdm Guid\",\"DI\":77,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PRIMARY_SOURCE\",\"DN\":\"Primary Source\",\"DI\":78,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PROFILE_TYPE\",\"DN\":\"Profile Type\",\"DI\":79,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"PROVINCE_STATE\",\"DN\":\"Province State\",\"DI\":80,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"REGULATORY_AGENCY\",\"DN\":\"Regulatory Agency\",\"DI\":81,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"REMARK\",\"DN\":\"Remark\",\"DI\":82,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"RIG_ON_SITE_DATE\",\"DN\":\"Rig On Site Date\",\"DI\":83,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"RIG_RELEASE_DATE\",\"DN\":\"Rig Release Date\",\"DI\":7,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"ROTARY_TABLE_ELEV\",\"DN\":\"Rotary Table Elev\",\"DI\":84,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"ROTARY_TABLE_ELEV_OUOM\",\"DN\":\"Rotary Table Elev Ouom\",\"DI\":85,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"SOURCE_DOCUMENT\",\"DN\":\"Source Document\",\"DI\":86,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"SPUD_DATE\",\"DN\":\"Spud Date\",\"DI\":8,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"SUBSEA_ELEV_REF_TYPE\",\"DN\":\"Subsea Elev Ref Type\",\"DI\":87,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"SURFACE_LATITUDE\",\"DN\":\"Surface Latitude\",\"DI\":88,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"SURFACE_LONGITUDE\",\"DN\":\"Surface Longitude\",\"DI\":89,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"SURFACE_NODE_ID\",\"DN\":\"Surface Node Id\",\"DI\":90,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"TAX_CREDIT_CODE\",\"DN\":\"Tax Credit Code\",\"DI\":91,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"TD_STRAT_AGE\",\"DN\":\"Td Strat Age\",\"DI\":92,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"TD_STRAT_NAME_SET_AGE\",\"DN\":\"Td Strat Name Set Age\",\"DI\":93,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"TD_STRAT_NAME_SET_ID\",\"DN\":\"Td Strat Name Set Id\",\"DI\":94,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"TD_STRAT_UNIT_ID\",\"DN\":\"Td Strat Unit Id\",\"DI\":95,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WATER_ACOUSTIC_VEL\",\"DN\":\"Water Acoustic Vel\",\"DI\":96,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"WATER_ACOUSTIC_VEL_OUOM\",\"DN\":\"Water Acoustic Vel Ouom\",\"DI\":97,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WATER_DEPTH\",\"DN\":\"Water Depth\",\"DI\":98,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"WATER_DEPTH_DATUM\",\"DN\":\"Water Depth Datum\",\"DI\":99,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WATER_DEPTH_OUOM\",\"DN\":\"Water Depth Ouom\",\"DI\":100,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_EVENT_NUM\",\"DN\":\"Well Event Num\",\"DI\":101,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_GOVERNMENT_ID\",\"DN\":\"Well Government Id\",\"DI\":6,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_INTERSECT_MD\",\"DN\":\"Well Intersect Md\",\"DI\":102,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"WELL_LEVEL_TYPE\",\"DN\":\"Well Level Type\",\"DI\":103,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_NAME\",\"DN\":\"Well Name\",\"DI\":3,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_NUM\",\"DN\":\"Well Num\",\"DI\":5,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"WELL_NUMERIC_ID\",\"DN\":\"Well Numeric Id\",\"DI\":104,\"H\":false,\"DT\":1,\"PT\":0},{\"CN\":\"WHIPSTOCK_DEPTH\",\"DN\":\"Whipstock Depth\",\"DI\":105,\"H\":false,\"DT\":2,\"PT\":0},{\"CN\":\"WHIPSTOCK_DEPTH_OUOM\",\"DN\":\"Whipstock Depth Ouom\",\"DI\":106,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CHANGED_BY\",\"DN\":\"Row Changed By\",\"DI\":107,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CHANGED_DATE\",\"DN\":\"Row Changed Date\",\"DI\":108,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"ROW_CREATED_BY\",\"DN\":\"Row Created By\",\"DI\":109,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CREATED_DATE\",\"DN\":\"Row Created Date\",\"DI\":110,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"ROW_QUALITY\",\"DN\":\"Row Quality\",\"DI\":111,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_OWNER_BA\",\"DN\":\"K Owner Ba\",\"DI\":112,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_ROW_CREATED_PROCESS\",\"DN\":\"K Row Created Process\",\"DI\":113,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_ROW_CHANGED_PROCESS\",\"DN\":\"K Row Changed Process\",\"DI\":114,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_HAS_GEOMETRY\",\"DN\":\"K Has Geometry\",\"DI\":115,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_UWI\",\"DN\":\"UWI\",\"DI\":2,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_WELL_STATUS\",\"DN\":\"K Well Status\",\"DI\":116,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_LICENSE_NUM\",\"DN\":\"License\",\"DI\":4,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_QC_NAME\",\"DN\":\"K Qc Name\",\"DI\":117,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_QC_DATE\",\"DN\":\"K Qc Date\",\"DI\":118,\"H\":false,\"DT\":3,\"PT\":0},{\"CN\":\"K_OWNERSHIP_CLASS\",\"DN\":\"K Ownership Class\",\"DI\":119,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_ORIGINAL_OWNER\",\"DN\":\"K Original Owner\",\"DI\":120,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"K_CURRENT_OWNER\",\"DN\":\"K Current Owner\",\"DI\":121,\"H\":false,\"DT\":0,\"PT\":0}]}");
                }
                //not saved, create new SearchPropertyList by fetching empty record of columns
                else if (searchLookup == SEARCH_LOOKUPS.BA)
                {
                    //TODO: use a hard-coded set here when there is none, this could be managed better somehow 
                    _searchPropertyList = JsonSerializer.Deserialize<SearchPropertyList>("{\"Type\":3,\"K\":null,\"TV\":\"BUSINESS_ASSOCIATE\",\"SPs\":[{\"CN\":\"BUSINESS_ASSOCIATE\",\"DN\":\"ID\",\"DI\":1,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"ACTIVE_IND\",\"DN\":\"Active Ind\",\"DI\":6,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"BA_ABBREVIATION\",\"DN\":\"Ba Abbreviation\",\"DI\":7,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"BA_CATEGORY\",\"DN\":\"Ba Category\",\"DI\":8,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"BA_CODE\",\"DN\":\"Code\",\"DI\":4,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"BA_NAME\",\"DN\":\"Name\",\"DI\":2,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"BA_SHORT_NAME\",\"DN\":\"Short Name\",\"DI\":3,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"BA_TYPE\",\"DN\":\"Ba Type\",\"DI\":9,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"CREDIT_CHECK_DATE\",\"DN\":\"Credit Check Date\",\"DI\":10,\"H\":true,\"DT\":3,\"PT\":0},{\"CN\":\"CREDIT_CHECK_IND\",\"DN\":\"Credit Check Ind\",\"DI\":11,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"CREDIT_CHECK_SOURCE\",\"DN\":\"Credit Check Source\",\"DI\":12,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"CREDIT_RATING\",\"DN\":\"Credit Rating\",\"DI\":13,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"CREDIT_RATING_SOURCE\",\"DN\":\"Credit Rating Source\",\"DI\":14,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"CURRENT_STATUS\",\"DN\":\"Current Status\",\"DI\":15,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"EFFECTIVE_DATE\",\"DN\":\"Effective Date\",\"DI\":16,\"H\":true,\"DT\":3,\"PT\":0},{\"CN\":\"EXPIRY_DATE\",\"DN\":\"Expiry Date\",\"DI\":17,\"H\":true,\"DT\":3,\"PT\":0},{\"CN\":\"FIRST_NAME\",\"DN\":\"First Name\",\"DI\":18,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"LAST_NAME\",\"DN\":\"Last Name\",\"DI\":19,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"MIDDLE_INITIAL\",\"DN\":\"Middle Initial\",\"DI\":20,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"PPDM_GUID\",\"DN\":\"Ppdm Guid\",\"DI\":21,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"REMARK\",\"DN\":\"Remark\",\"DI\":5,\"H\":false,\"DT\":0,\"PT\":0},{\"CN\":\"SERVICE_PERIOD\",\"DN\":\"Service Period\",\"DI\":22,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"SOURCE\",\"DN\":\"Source\",\"DI\":23,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CHANGED_BY\",\"DN\":\"Row Changed By\",\"DI\":24,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CHANGED_DATE\",\"DN\":\"Row Changed Date\",\"DI\":25,\"H\":true,\"DT\":3,\"PT\":0},{\"CN\":\"ROW_CREATED_BY\",\"DN\":\"Row Created By\",\"DI\":26,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"ROW_CREATED_DATE\",\"DN\":\"Row Created Date\",\"DI\":27,\"H\":true,\"DT\":3,\"PT\":0},{\"CN\":\"ROW_QUALITY\",\"DN\":\"Row Quality\",\"DI\":28,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_OWNER_BA\",\"DN\":\"K Owner Ba\",\"DI\":29,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_ORIG_KTI_ID\",\"DN\":\"K Orig Kti Id\",\"DI\":30,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_ROW_CREATED_PROCESS\",\"DN\":\"K Row Created Process\",\"DI\":31,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_ROW_CHANGED_PROCESS\",\"DN\":\"K Row Changed Process\",\"DI\":32,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_PREFERRED_IND\",\"DN\":\"K Preferred Ind\",\"DI\":33,\"H\":true,\"DT\":0,\"PT\":0},{\"CN\":\"K_NEW_BA_ID\",\"DN\":\"K New Ba Id\",\"DI\":34,\"H\":true,\"DT\":0,\"PT\":0}]}");
                }
                else
                {
                    _searchPropertyList = new SearchPropertyList();
                    _searchPropertyList.Type = searchLookup;
                    _searchPropertyList.TV = GetTableViewNameFromSearchLookup(searchLookup);

                    ds.AddSQL("SELECT * FROM " + GetTableViewNameFromSearchLookup(searchLookup) + " WHERE 1 <> 1");
                    using (DataTable dt = ds.GetDataTables()[0])
                    {
                        //add each column as a search criteria
                        Int32 i = 1;
                        Boolean include;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            //only include simple types
                            include = true;
                            SearchProperty sp = new SearchProperty(dc.ColumnName);

                            if (dc.DataType == System.Type.GetType("System.String"))
                                sp.DT = SearchProperty.DATA_TYPE.TEXT;
                            else if (dc.DataType == System.Type.GetType("System.Byte") ||
                            dc.DataType == System.Type.GetType("System.SByte") ||
                            dc.DataType == System.Type.GetType("System.Int16") ||
                            dc.DataType == System.Type.GetType("System.Int32") ||
                            dc.DataType == System.Type.GetType("System.Int64"))
                                sp.DT = SearchProperty.DATA_TYPE.NUMBER;
                            else if (dc.DataType == System.Type.GetType("System.Single") ||
                            dc.DataType == System.Type.GetType("System.Float") ||
                                dc.DataType == System.Type.GetType("System.Double") ||
                            dc.DataType == System.Type.GetType("System.Decimal"))
                                sp.DT = SearchProperty.DATA_TYPE.DECIMAL;
                            else if (dc.DataType == System.Type.GetType("System.DateTime"))
                                sp.DT = SearchProperty.DATA_TYPE.DATE;
                            else
                            {
                                include = false;
                                if (!excludes.Contains(dc.DataType.ToString()))
                                    excludes.Add(dc.DataType.ToString());
                            }

                            if (include)
                            {
                                sp.DI = i++;
                                _searchPropertyList.SPs.Add(sp);
                            }
                        }
                    }
                }
            }

            //SJG: used to check data types not being covered yet
            //So far is only System.Object for Oracle spatial columns
            /*
            if (excludes.Count > 0)
                MessageBox.Show(this, "Excluding the following data types:\r\n" + String.Join("\r\n", excludes));
            */

        }

        private String GetTableViewNameFromSearchLookup(SEARCH_LOOKUPS searchLookup)
        {
            if (searchLookup == SEARCH_LOOKUPS.SURVEY)
                return "SEIS_ACQTN_SURVEY";
            else if (searchLookup == SEARCH_LOOKUPS.SEISMIC)
                return "KVS_SEIS_LINE";
            else if (searchLookup == SEARCH_LOOKUPS.WELL)
                return "KVS_WELL";
            else if (searchLookup == SEARCH_LOOKUPS.BA)
                return "BUSINESS_ASSOCIATE";
            else
                throw new NotSupportedException(searchLookup.ToString());
        }

        private Point GetControlLocationFromDisplayIndex(Int32 displayIndex, Boolean isLabel)
        {
            Int32 column;
            Int32 row;
            if (displayIndex <= ROWS_PER_COLUMN)
                column = 1;
            else if (displayIndex % ROWS_PER_COLUMN == 0)
                column = (displayIndex / ROWS_PER_COLUMN);
            else
                column = (displayIndex / ROWS_PER_COLUMN) + 1;
            if ((displayIndex % ROWS_PER_COLUMN) == 0)
                row = ROWS_PER_COLUMN;
            else
                row = displayIndex % ROWS_PER_COLUMN;

            if (isLabel)
                return new Point(8 + ((column-1) * 249), 8 + ((row - 1) * 27));
            else
                return new Point(126 + ((column - 1) * 249), 9 + ((row - 1) * 26));
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            _selectionMade = false;
            Close();
        }

        private void BtnSelectAndClose_Click(object sender, EventArgs e)
        {
            _selectionMade = true;
            Close();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                String sqlWhere = String.Empty;
                String val;
                SearchProperty sp;
                List<Object> orVals;
                using (DataSource ds = new DataSource(_databaseLogon))
                {
                    foreach (Control ctrl in panel.Controls)
                    {
                        if (ctrl is TextBoxStyled tbs)
                        {
                            val = tbs.Text.Trim();
                            if (val.Length > 0)
                            {
                                sp = GetSearchPropertyFromControl(ctrl);

                                //TODO: add support for:
                                //greater-than/less-than searching
                                //between date searching
                                //between numeric searching
                                //searching for criteria with commas (by wrapping in double-quotes)
                                //mixed case searching... for columns we know are always uppercased, we have a ForceUppercase property for each SearchProperty

                                if (val.Contains(","))
                                {
                                    //TODO: will max csv to DB surpass what can be handled by TextBoxStyled.TxtBox_KeyDown?
                                    orVals = new List<Object>();
                                    foreach (String splitVal in val.Split(','))
                                    {
                                        String orVal = splitVal.Trim();
                                        if (orVal.Length > 0)
                                        {
                                            if (sp.DT == SearchProperty.DATA_TYPE.TEXT)
                                                orVals.Add(orVal);
                                            else if (sp.DT == SearchProperty.DATA_TYPE.NUMBER)
                                                orVals.Add(Int64.Parse(orVal));
                                            else if (sp.DT == SearchProperty.DATA_TYPE.NUMBER)
                                                orVals.Add(Decimal.Parse(orVal));
                                            else if (sp.DT == SearchProperty.DATA_TYPE.DATE)
                                                orVals.Add(DateTime.Parse(orVal));
                                            else
                                                throw new NotSupportedException(sp.DT.ToString());
                                        }
                                    }
                                    if (sp.DT == SearchProperty.DATA_TYPE.DATE)
                                    {
                                        //dates require special handling to ignore time stamps
                                        sqlWhere += "(";
                                        foreach (Object orVal in orVals)
                                            sqlWhere += "(" + sp.CN + " >= " + ds.AddParameter(DateTime.Parse(orVal.ToString())) + " AND " + sp.CN + " < " + ds.AddParameter(DateTime.Parse(orVal.ToString()).AddDays(1)) + ") OR ";
                                        sqlWhere = sqlWhere.Substring(0, sqlWhere.LastIndexOf(" OR "));
                                        sqlWhere += ") AND ";
                                    }
                                    else
                                        sqlWhere += sp.CN + " IN (" + ds.AddListOfINParameters(orVals) + ") AND ";
                                }
                                else if (val.Contains("%") && sp.DT == SearchProperty.DATA_TYPE.TEXT)
                                {
                                    sqlWhere += sp.CN + " LIKE " + ds.AddParameter(val) + " AND ";
                                }
                                else
                                {
                                    if (sp.DT == SearchProperty.DATA_TYPE.TEXT)
                                        sqlWhere += sp.CN + " = " + ds.AddParameter(val) + " AND ";
                                    else if (sp.DT == SearchProperty.DATA_TYPE.NUMBER)
                                        sqlWhere += sp.CN + " = " + ds.AddParameter(Int64.Parse(val)) + " AND ";
                                    else if (sp.DT == SearchProperty.DATA_TYPE.NUMBER)
                                        sqlWhere += sp.CN + " = " + ds.AddParameter(Decimal.Parse(val)) + " AND ";
                                    else if (sp.DT == SearchProperty.DATA_TYPE.DATE)
                                        //dates require special handling to ignore time stamps
                                        sqlWhere += "(" + sp.CN + " >= " + ds.AddParameter(DateTime.Parse(val)) + " AND " + sp.CN + " < " + ds.AddParameter(DateTime.Parse(val).AddDays(1)) + ") AND ";
                                    else
                                        throw new NotSupportedException(sp.DT.ToString());
                                }
                            }
                        }
                    }

                    if (sqlWhere.Length == 0)
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show(this, "Please enter search criteria.", btnSearch.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        sqlWhere = sqlWhere.Substring(0, sqlWhere.LastIndexOf(" AND "));
                        ds.AddSQL("SELECT " + _selectClause + " FROM " + _searchPropertyList.TV + " WHERE " + sqlWhere);
                        //TODO: set a max row count to return to f.ex. 10,001 and prompt user if hits that limit with:
                        //"Displaying first 10,000 results.  More results are being excluded, please adjust your search criteria to return less results."

                        //TODO: add a record count and time elapsed

                        SetResultGrid(ds.GetDataTables()[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(this, "Unexpected error:\r\n" + ex.ToString(), btnSearch.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void SetResultGrid(DataTable dt)
        {
            try
            {
                //this prevents the DataGridView_ColumnDisplayIndexChanged() code from being executed while the grid is being loaded
                dgvResults.SuspendLayout();
                dgvResults.SuspendCustomActions = true;

                dgvResults.DataSource = dt;

                //must do these after setting datasource, not before
                dgvResults.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvResults.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvResults.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                dgvResults.AllowUserToAddRows = false;

                //if already has a display format then reset it
                //always need to re-apply display prefs when grid is first loaded or re-loaded, columns may get shifted after re-loading and need to re-apply
                if (dgvResults.HasDisplayFormat)
                {
                    dgvResults.ResetFormat();
                }
                //otherwise, try to retrieve one
                else
                {
                    String ret = String.Empty;
                    using (DataSource ds = new DataSource(_databaseLogon))
                    {
                        KDMDBFunctions dbf = new KDMDBFunctions(ds);
                        List<String> json = dbf.GetSavedPreference(KDMDBFunctions.PreferenceLevel.GLOBAL_CLOB, _appPrefType, _searchPropertyList.Type.ToString(), PREF_PARAM_RESULT_GRID);
                        if (json.Count > 0)
                            ret = dgvResults.ApplyJsonFormat(json[0]);
                        else
                            //no format created or saved yet, in this case we want to use the given SearchPropertyList to format the grid
                            UpdateGridDisplayFormatFromSearchPropertyList();

                        //just have to give this a value so doesn't prompt user, not used to save or retrieve
                        dgvResults.SelectedDisplayFormat = PREF_PARAM_RESULT_GRID;

                        if (!String.IsNullOrEmpty(ret))
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show(this, ret, "Applying Display Preferences", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
                dgvResults.RefreshColumnHeaderContextMenu();
            }
            finally
            {
                dgvResults.SuspendCustomActions = false;
                dgvResults.ResumeLayout();
            }
        }

        private void ViewSearchLookup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_selectionMade && dgvResults.SelectedRows != null && dgvResults.SelectedRows.Count > 0)
                DialogResult = DialogResult.OK;
            else
                dgvResults.ClearSelection();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            foreach (Control ctrl in panel.Controls)
            {
                if (ctrl is TextBoxStyled tbs)
                {
                    tbs.Text = String.Empty;
                }
            }
        }

        private void DgvResults_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            BtnSelectAndClose_Click(dgvResults, null);
        }

        private void ViewSearchLookup_Shown(object sender, EventArgs e)
        {
            //set initial search criteria if given and invoke the search
            if (_initialSearchCriteria.Count > 0)
            {
                foreach (KeyValuePair<String, String> kvp in _initialSearchCriteria)
                    GetControlFromSearchProperty(_searchPropertyList.SPs.Where(x => x.CN.Equals(GetColumnNameFromSearchResultColumn(kvp.Key))).First()).Text = kvp.Value;
                BtnSearch_Click(this, null);
            }
        }
    }
}
