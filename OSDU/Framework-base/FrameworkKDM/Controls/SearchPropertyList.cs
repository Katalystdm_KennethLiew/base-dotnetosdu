﻿using Framework;
using System;
using System.Collections.Generic;

namespace FrameworkKDM.Controls
{
    class SearchPropertyList
    {
        public ViewSearchLookup.SEARCH_LOOKUPS Type { get; set; }

        //Key
        public String K { get; set; } //get/set required for serialization

        //table or view name
        public String TV { get; set; }

        //List of Columns from Table or View plus additional attributes
        public List<SearchProperty> SPs { get; set; }

        public SearchPropertyList()
        {
            SPs = new List<SearchProperty>();
        }

    }

    class SearchProperty
    {
        public enum DATA_TYPE
        {
            TEXT,
            NUMBER,
            DECIMAL,
            DATE
        }
        public enum PROPERTY_TYPE
        {
            REGULAR,
            DROP_DOWN,
            EXTENDED_LOOKUP
        }
        public String CN { get; set; }
        public String DN { get; set; }
        public Int32 DI { get; set; }//one-based
        public Boolean H { get; set; } = false;
        public DATA_TYPE DT { get; set; } = DATA_TYPE.TEXT;
        public PROPERTY_TYPE PT { get; set; } = PROPERTY_TYPE.REGULAR;
        public SearchProperty()
        {
            //required for serialization even though doing nothing 
        }
        public SearchProperty(String columnName)
        {
            CN = columnName;
            DN = Common.ConvertToPascalCasing(CN);
        }
    }

}
