﻿using Framework;
using Framework.Controls;
using FrameworkKDM.Controls;
using System;
using System.IO;
using System.Text.Json;
using System.Windows.Forms;

namespace FrameworkKDM
{
    public class DatabaseConnectionHandler
    {
        private readonly Form _parentForm;
        private readonly DatabaseLogonList _dbLogonList;
        private Int32 _pwdLogonAttempts;

        public DatabaseConnectionHandler(Form parentForm, DatabaseLogonList dbLogonList)
        {
            _parentForm = parentForm;
            _dbLogonList = dbLogonList;
        }

        public Boolean DoesKDMPasswordFileExist()
        {
            return File.Exists(Path.Combine(PassInfo.PWD_FILE_LOCATION, PassInfo.PWD_FILE));
        }

        public Boolean GetOperatorLogonFromKDMPasswordFile(String opsUserName)
        {
            return GetDBLogonFromKDMPasswordFile(opsUserName, false);
        }
        public Boolean GetClientLogonFromKDMPasswordFile(String clientName)
        {
            return GetDBLogonFromKDMPasswordFile(clientName, true);
        }
        private Boolean GetDBLogonFromKDMPasswordFile(String pwdFileUserName, Boolean asKEDIT)
        {
            Boolean ret = false;

            //always use KDM password file if exists
            DirectoryInfo passwordFilePath = new DirectoryInfo(PassInfo.PWD_FILE_LOCATION);
            if (File.Exists(Path.Combine(passwordFilePath.FullName, PassInfo.PWD_FILE)))
            {
                PassInfo pi = new PassInfo();
                if (pi.LoadPasswords(passwordFilePath))
                {
                    if (asKEDIT)
                        pi.LoadPasswordFileAsKEDIT(_dbLogonList, pwdFileUserName);
                    else
                        pi.LoadPasswordFile(_dbLogonList, pwdFileUserName);

                    /* For use when don't have a good password file to use 
                    _dbLogonList.Items.Clear();
                    DatabaseLogon dbLogon;
                    //dbLogon = new DatabaseLogon("houdm-db4-tst", "hkaptst", "1521", "xxx", "xxx", false);//QA
                    //dbLogon = new DatabaseLogon("caldm-db2-dev", "keldev11", "1521", "xxx", "xxx", false);//DEV
                    //PG-TODO: use this until have updated password files to use
                    dbLogon = new DatabaseLogon("hou-db6", "dev1", "5444", "kdm_us_view", "dev1", false);//Postgres
                    _dbLogonList.Items.Add(dbLogon);
                    */

                    //set the selected DB to first one loaded if not set yet
                    if (_dbLogonList.GetSelectedDatabaseLogon() == null && _dbLogonList.Items.Count > 0)
                    {
                        _dbLogonList.SelectedDatabaseLogonHost = _dbLogonList.Items[0].Host;
                        //let calling code know new connection info obtained 
                        ret = true;
                    }
                    else if (_dbLogonList.GetSelectedDatabaseLogon() != null)
                        ret = true;
                }
            }
            return ret;
        }

        public Boolean GetDBLogonFromManuallEntry(Boolean manuallyEnter, Boolean pwdOnly)
        {
            Boolean ret = false;
            DatabaseLogon dbLogon = null;

            //increment logon attempts
            _pwdLogonAttempts++;
            
            //if not manually entering and no logon set yet then attempt to read it from encrypted file
            if (!manuallyEnter && _dbLogonList.GetSelectedDatabaseLogon() == null)
                dbLogon = ReadEncryptedConnectionInfo();

            //if was read from file then use it
            if (dbLogon != null)
            {
                //if pwd not saved then prompt for it
                if (String.IsNullOrEmpty(dbLogon.PWD))
                {
                    _parentForm.Cursor = Cursors.Default;
                    dbLogon.PWD = Common.GetInput("Enter password:", String.Empty, true, _parentForm);
                }

                if (dbLogon.PWD.Trim().Length > 0)
                {
                    _dbLogonList.Items.Add(dbLogon);
                    _dbLogonList.SelectedDatabaseLogonHost = dbLogon.Host;
                    ret = true;
                }
            }
            //otherwise, was either no file to read OR was bad file OR was already read from file and still didn't work
            //in any case, need to prompt user
            else
            {
                //pull what may have already been entered
                dbLogon = _dbLogonList.GetSelectedDatabaseLogon();

                _parentForm.Cursor = Cursors.Default;
                if (pwdOnly)
                {
                    dbLogon.PWD = Common.GetInput("Enter password:", String.Empty, true, _parentForm);
                    if (dbLogon.PWD.Trim().Length > 0)
                        ret = true;
                }
                else
                {
                    //pass into prompt to get connection info
                    if (ShowDatabaseConnectionDialog(_parentForm, ref dbLogon))
                    {
                        //clear and re-add updated logon object
                        if (_dbLogonList.Items.Count > 0)
                            _dbLogonList.Items.Clear();
                        _dbLogonList.Items.Add(dbLogon);
                        _dbLogonList.SelectedDatabaseLogonHost = dbLogon.Host;
                        ret = true;
                    }
                }
            }

            return ret;
        }

        public Boolean IsOkToSignIn()
        {
            if (_pwdLogonAttempts < 3 && _dbLogonList.GetSelectedDatabaseLogon() != null && !_dbLogonList.GetSelectedDatabaseLogon().SavePWD)
                return true;
            else
                return false;
        }

        private Boolean ShowDatabaseConnectionDialog(IWin32Window owner, ref DatabaseLogon dbLogon)
        {
            //prompt user for connection info
            using (ViewDBConnection vDBConn = new ViewDBConnection(ref dbLogon))
            {
                if (vDBConn.ShowDialog(owner) == DialogResult.OK)
                {
                    //if not saving pwd then back it up prior to saving to file
                    String pwd = dbLogon.PWD ?? String.Empty;
                    if (!dbLogon.SavePWD)
                        dbLogon.PWD = String.Empty;

                    //save to encryped file
                    SaveEncryptedConnectionInfo(dbLogon);

                    //if not saving pwd then get it back
                    if (!dbLogon.SavePWD)
                        dbLogon.PWD = pwd;

                    return true;
                }
            }
            return false;
        }

        private String GetSavedConnectionInfoFileName()
        {
            String dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "KDM");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetEntryAssembly().Location).ProductName + ".tmp");
        }

        private void SaveEncryptedConnectionInfo(DatabaseLogon dbLogon)
        {
            //save to encypted file on local machine under user's profile
            using (Crypto crypt = new Crypto())
            {
                using (StreamWriter file = new StreamWriter(GetSavedConnectionInfoFileName()))
                {
                    //Include the current user to be sure the only the user who created this can read it.
                    //Note, this is already stored into the current user's folder but this will prevent another user to copy it into their folder.
                    //Locking this down per user.
                    file.Write(crypt.Encrypt(Environment.UserName + JsonSerializer.Serialize<DatabaseLogon>(dbLogon)));
                    file.Flush();
                    file.Close();
                }
            }
        }

        private DatabaseLogon ReadEncryptedConnectionInfo()
        {
            DatabaseLogon dbLogon = null;
            String fileName = GetSavedConnectionInfoFileName();

            if (File.Exists(fileName))
            {
                //read from encypted file on local machine under user's profile
                using (Crypto crypt = new Crypto())
                {
                    using (StreamReader file = new StreamReader(fileName))
                    {
                        String decrypted = crypt.Decrypt(file.ReadToEnd());
                        //The user who originally created this is included at the beginning so must extract that out separately.
                        //Ensure the same user name who is decrypting was the same one who originally encrypted.
                        if (decrypted.ToUpper().Substring(0, Environment.UserName.Length).Equals(Environment.UserName.ToUpper()))
                            dbLogon = JsonSerializer.Deserialize<DatabaseLogon>(decrypted.Substring(Environment.UserName.Length));
                        file.Close();
                    }
                }
            }
            return dbLogon;
        }

        public void ClearEncryptedConnectionInfo()
        {
            String fileName = GetSavedConnectionInfoFileName();
            if (File.Exists(fileName))
                File.Delete(fileName);
        }

    }
}
