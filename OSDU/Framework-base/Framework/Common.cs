﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Framework
{
    public static class Common
    {
        public static String GetFrameworkVersion()
        {
            return System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
        }

        public static String GetInput(String caption, String defaultInput, Boolean maskInput, Control window)
        {
            return InnerGetInput(caption, defaultInput, maskInput, window, new Point(), CharacterCasing.Normal);
        }
        public static String GetInput(String caption, String defaultInput, Boolean maskInput, Control window, Point location, CharacterCasing casing)
        {
            return InnerGetInput(caption, defaultInput, maskInput, window, location, casing);
        }
        public static String GetInput(String caption, String defaultInput, Boolean maskInput, Control window, CharacterCasing casing)
        {
            return InnerGetInput(caption, defaultInput, maskInput, window, new Point(), casing);
        }
        public static String GetInput(String caption, String defaultInput, Boolean maskInput, Control window, Point location)
        {
            return InnerGetInput(caption, defaultInput, maskInput, window, location, CharacterCasing.Normal);
        }
        private static String InnerGetInput(String caption, String defaultInput, Boolean maskInput, Control window, Point location, CharacterCasing casing)
        {
            using (Form inputBox = new Form())
            {
                inputBox.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                inputBox.ClientSize = new Size(300, 70);
                inputBox.Text = caption;

                //using this label just to get the width of text
                Label lbl = new Label();
                lbl.AutoSize = true;
                lbl.Text = defaultInput;
                inputBox.Width = lbl.Width + 30;
                lbl.Dispose();
                if (inputBox.Width < 300)
                    inputBox.Width = 300;

                TextBox textBox = new TextBox();
                textBox.CharacterCasing = casing;
                textBox.Size = new Size(inputBox.Width - 30, 23);
                textBox.Location = new Point(5, 5);
                textBox.Text = defaultInput;
                if (maskInput)
                    textBox.PasswordChar = '*';
                inputBox.Controls.Add(textBox);

                Button okButton = new Button();
                okButton.DialogResult = DialogResult.OK;
                okButton.Size = new Size(75, 23);
                okButton.Text = "&Ok";
                okButton.Location = new Point(inputBox.Width - 80 - 100, 39);
                inputBox.Controls.Add(okButton);

                Button cancelButton = new Button();
                cancelButton.DialogResult = DialogResult.Cancel;
                cancelButton.Size = new Size(75, 23);
                cancelButton.Text = "&Cancel";
                cancelButton.Location = new Point(inputBox.Width - 100, 39);
                inputBox.Controls.Add(cancelButton);

                inputBox.AcceptButton = okButton;
                inputBox.CancelButton = cancelButton;

                if (!location.IsEmpty)
                {
                    inputBox.StartPosition = FormStartPosition.Manual;
                    inputBox.Location = location;
                }
                else
                {
                    inputBox.StartPosition = FormStartPosition.CenterParent;
                }
                DialogResult result = inputBox.ShowDialog(window);
                if (result == DialogResult.OK)
                    return textBox.Text;
                else
                    return "";
            }
        }

        public static void ShowMessageBoxOkOnly(String caption, String message, Control window, Point location)
        {
            ShowMessageBoxOkCancel(caption, message, "&Ok", String.Empty, window, location);
        }
        public static DialogResult ShowMessageBoxOkCancel(String caption, String message, String leftAcceptButtonText, String rightCancelButtonText, Control window, Point location)
        {
            using (Form msgBox = new Form())
            {
                msgBox.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                msgBox.ClientSize = new Size(300, 70);
                msgBox.Text = caption;

                Label lbl = new Label();
                lbl.AutoSize = true;
                lbl.Location = new Point(5, 5);
                lbl.Text = message;
                msgBox.Controls.Add(lbl);
                msgBox.Width = lbl.Width + 30;
                if (msgBox.Width < 300)
                    msgBox.Width = 300;

                if (!String.IsNullOrEmpty(rightCancelButtonText))
                {
                    Button rightCancelButton = new Button();
                    rightCancelButton.DialogResult = DialogResult.Cancel;
                    rightCancelButton.Size = new Size(75, 23);
                    rightCancelButton.Text = rightCancelButtonText;
                    rightCancelButton.Location = new Point(msgBox.Width - 100, 39);
                    msgBox.Controls.Add(rightCancelButton);
                    msgBox.CancelButton = rightCancelButton;
                }

                Button leftAcceptButton = new Button();
                leftAcceptButton.DialogResult = DialogResult.OK;
                leftAcceptButton.Size = new Size(75, 23);
                leftAcceptButton.Text = leftAcceptButtonText;
                if (!String.IsNullOrEmpty(rightCancelButtonText))
                    leftAcceptButton.Location = new Point(msgBox.Width - 80 - 100, 39);
                else
                    leftAcceptButton.Location = new Point(msgBox.Width - 100, 39);
                msgBox.Controls.Add(leftAcceptButton);
                msgBox.AcceptButton = leftAcceptButton;

                if (!location.IsEmpty)
                {
                    msgBox.StartPosition = FormStartPosition.Manual;
                    msgBox.Location = location;
                }
                else
                {
                    msgBox.StartPosition = FormStartPosition.CenterParent;
                }
                return msgBox.ShowDialog(window);
            }
        }

        public static string ConvertBooleanToYesNo(object value)
        {
            if (Convert.ToBoolean(value))
                return "YES";
            else
                return "NO";
        }

        public static String ConvertToPascalCasing(String input)
        {
            input = input.Replace("_", " ");
            String pascal = String.Empty;
            Boolean prevSpace = false;
            for (Int32 i = 0; i < input.Length; i++)
            {
                if (i == 0 || prevSpace)
                    pascal += input.Substring(i, 1).ToUpper();
                else
                    pascal += input.Substring(i, 1).ToLower();
                if (String.IsNullOrWhiteSpace(input.Substring(i, 1)))
                    prevSpace = true;
                else
                    prevSpace = false;
            }
            return pascal;
        }

        //This returns all rows from selected cells and/or selected rows.
        //This will work regardless if the user has selected full rows or only certain cells within rows.
        //For each fully selected rows, all cells for that row are checked but only the distinct rows are returned.
        //Note that DataGridView.SelectionMode should be RowHeaderSelect to allow selecting individual cells or full rows.
        public static List<DataGridViewRow> GetRowsFromSelectedCells(DataGridView dgv)
        {
            if (dgv != null && dgv.Rows.Count > 0)
                return (dgv.SelectedCells.Cast<DataGridViewCell>().Select(cell => cell.OwningRow).Distinct().OrderBy(row => row.Index)).ToList();
            else
                return new List<DataGridViewRow>();
        }

        //convert tabs and newlines to csv
        //allows copy/paste from spreadsheet or txt file to csv so can OR search criteria
        //TODO: was is the max length a text box can hold?
        public static void Control_KeyDown_TabsAndNewLinesToCSV(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                String clipboard = Clipboard.GetText();
                if (clipboard.Contains("\t"))
                    clipboard = clipboard.Replace("\t", ",").TrimEnd(',');
                if (clipboard.Contains("\r\n"))
                    clipboard = clipboard.Replace("\r\n", ",").TrimEnd(',');
                if (clipboard.Contains("\r"))
                    clipboard = clipboard.Replace("\r", ",").TrimEnd(',');
                if (((TextBox)sender).SelectedText.Length > 0)
                    ((TextBox)sender).SelectedText = clipboard;
                else
                    ((TextBox)sender).Text = clipboard;
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

    }
}
