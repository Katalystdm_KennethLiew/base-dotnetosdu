﻿using System;
using System.Collections.Generic;

namespace Framework
{
    public class DatabaseLogonList
    {
        public List<DatabaseLogon> Items { get; set; } //get/set required for serialization
        public String SelectedDatabaseLogonHost { get; set; } = String.Empty;

        public DatabaseLogonList()
        {
            Items = new List<DatabaseLogon>();
        }

        public Boolean ContainsHost(String host)
        {
            if (!String.IsNullOrEmpty(host))
            {
                foreach (DatabaseLogon dbl in Items)
                {
                    if (dbl.Host.ToLower().Equals(host.ToLower()))
                        return true;
                }
            }
            return false;
        }

        public DatabaseLogon GetDatabaseLogonByHost(String host)
        {
            if (!String.IsNullOrEmpty(host))
            {
                foreach (DatabaseLogon dbl in Items)
                {
                    if (dbl.Host.ToLower().Equals(host.ToLower()))
                        return dbl;
                }
            }
            return null;
        }

        public DatabaseLogon GetSelectedDatabaseLogon()
        {
            if (!String.IsNullOrEmpty(SelectedDatabaseLogonHost))
            {
                foreach (DatabaseLogon dbl in Items)
                {
                    if (dbl.Host.ToLower().Equals(SelectedDatabaseLogonHost.ToLower()))
                        return dbl;
                }
            }
            return null;
        }

    }
}
