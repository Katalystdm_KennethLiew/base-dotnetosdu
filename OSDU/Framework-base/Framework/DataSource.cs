﻿using System;
using System.Data;
using System.Collections.Generic;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Npgsql;

namespace Framework
{
    public class DataSource : IDisposable
    {

        public enum DatabaseTypes
        {
            ORACLE,
            POSTGRES
        }

        //implementing IDisposable 
        private Boolean _disposed = false; // to detect redundant calls

        private readonly List<List<OracleParameter>> _paramsOracle = new List<List<OracleParameter>>();
        private readonly List<List<NpgsqlParameter>> _paramsPostgres = new List<List<NpgsqlParameter>>();

        private const String PARAM_PREFIX = ":p";

        private readonly List<String> _sQLs = new List<String>();

        private readonly List<Int32> _expectedResultCounts = new List<Int32>();

        //PG-TODO: calling code application can toggle this to switch DB types, everything else should be the same
        public DatabaseTypes DBType { get; set; } = DatabaseTypes.ORACLE;

        public DatabaseLogon DatabaseLogon { get; }

        public String ConnectionString
        { 
            get 
            {
                if (DBType == DatabaseTypes.ORACLE)
                    return "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" + DatabaseLogon.Host + ")(PORT=" + DatabaseLogon.Port + "))(CONNECT_DATA=(SID=" + DatabaseLogon.ServiceName + ")));User ID=" + DatabaseLogon.ID + ";Password=" + DatabaseLogon.PWD + ";";
                else
                    //PG-TODO: Pooling=false should not be neccessary but is a workaround for the RLS issue for now
                    return "Host=" + DatabaseLogon.Host + ";Port=" + DatabaseLogon.Port + ";Database=" + DatabaseLogon.ServiceName + ";Username =" + DatabaseLogon.ID + ";Password=" + DatabaseLogon.PWD + ";Pooling=false;SSLMode=Disable;";
            }
        }

        public DataSource(DatabaseLogon dbLogon)
        {
            DatabaseLogon = dbLogon;
        }

        public String AddParameter(Object value)
        {
            if (DBType == DatabaseTypes.ORACLE)
            {
                if (_paramsOracle.Count == 0)
                    _paramsOracle.Add(new List<OracleParameter>());

                using (OracleParameter p = new OracleParameter(PARAM_PREFIX + (_paramsOracle[_paramsOracle.Count - 1].Count + 1).ToString(), value))
                {
                    _paramsOracle[_paramsOracle.Count - 1].Add(p);
                    return p.ParameterName;
                }
            }
            else
            {
                if (_paramsPostgres.Count == 0)
                    _paramsPostgres.Add(new List<NpgsqlParameter>());

                NpgsqlParameter p = new NpgsqlParameter(PARAM_PREFIX + (_paramsPostgres[_paramsPostgres.Count - 1].Count + 1).ToString(), GetNpgSQLParamValue(value));
                _paramsPostgres[_paramsPostgres.Count - 1].Add(p);
                return p.ParameterName;
            }
        }

        private Object GetNpgSQLParamValue(Object value)
        {
            //NpgSQL requires null and empty string values to be passed in as DBNull.Value
            if (value is String strVal && String.IsNullOrEmpty(strVal) || value is null)
                return DBNull.Value;
            else
                return value;
        }

        public String AddParameterCurrentServerDate()
        {
            //expected this not to work for Postgres but for our version of Postgres it works
            return "SYSDATE";
        }

        public String AddListOfINParameters(List<Object> values)
        {
            String paramNames = String.Empty;

            if (values != null && values.Count > 0)
            {
                if (DBType == DatabaseTypes.ORACLE)
                {
                    if (_paramsOracle.Count == 0)
                        _paramsOracle.Add(new List<OracleParameter>());
                }
                else
                {
                    if (_paramsPostgres.Count == 0)
                        _paramsPostgres.Add(new List<NpgsqlParameter>());
                }

                foreach (Object value in values)
                {
                    if (DBType == DatabaseTypes.ORACLE)
                    {
                        using (OracleParameter p = new OracleParameter(PARAM_PREFIX + (_paramsOracle[_paramsOracle.Count - 1].Count + 1).ToString(), value))
                        {
                            _paramsOracle[_paramsOracle.Count - 1].Add(p);
                            paramNames += p.ParameterName + ",";
                        }
                    }
                    else
                    {
                        NpgsqlParameter p = new NpgsqlParameter(PARAM_PREFIX + (_paramsPostgres[_paramsPostgres.Count - 1].Count + 1).ToString(), GetNpgSQLParamValue(value));
                        _paramsPostgres[_paramsPostgres.Count - 1].Add(p);
                        paramNames += p.ParameterName + ",";
                    }
                }
                paramNames = paramNames.TrimEnd(',');
            }
            return paramNames;
        }

        public String AddListOfINParametersFromCSV(String csv)
        {
            String paramNames = String.Empty;

            if (!String.IsNullOrEmpty(csv))
            {
                if (DBType == DatabaseTypes.ORACLE)
                {
                    if (_paramsOracle.Count == 0)
                        _paramsOracle.Add(new List<OracleParameter>());
                }
                else
                {
                    if (_paramsPostgres.Count == 0)
                        _paramsPostgres.Add(new List<NpgsqlParameter>());
                }

                List<Object> values = new List<Object>();
                foreach (String val in csv.Split(','))
                    values.Add(val.Trim());

                paramNames = AddListOfINParameters(values);
            }
            return paramNames;
        }

        public void AddSQL(String sql)
        {
            _expectedResultCounts.Add(0);
            InnerAddSQL(sql);
        }

        public void AddSQL(String sql, Int32 expectedResultCount)
        {
            _expectedResultCounts.Add(expectedResultCount);
            InnerAddSQL(sql);
        }

        private void InnerAddSQL(String sql)
        {
            _sQLs.Add(sql);
            //Each time we add an sql we must increment the list of "parameter listings" ready to accept another list of parameters for the next sql added.
            //Do this even if no parameters given for any one sql to ensure any subsequent sqls with given parameters align with the non-empty "parameter listing".
            if (DBType == DatabaseTypes.ORACLE)
                _paramsOracle.Add(new List<OracleParameter>());
            else
                _paramsPostgres.Add(new List<NpgsqlParameter>());
        }

        public Int32 GetCountSQLs()
        {
            return _sQLs.Count;
        }

        public void Clear()
        {
            if (_sQLs != null)
                _sQLs.Clear();
            if (_expectedResultCounts != null)
                _expectedResultCounts.Clear();
            if (_paramsOracle != null)
            {
                foreach (List<OracleParameter> pList in _paramsOracle)
                    foreach (OracleParameter p in pList)
                        if (p != null)
                            p.Dispose();
                _paramsOracle.Clear();
                _paramsPostgres.Clear();
            }
        }

        public List<DataTable> GetDataTables()
        {
            if (DBType == DatabaseTypes.ORACLE)
                return GetDataTablesOracle();
            else
                return GetDataTablesPostgres();
        }

        private List<DataTable> GetDataTablesOracle()
        {
            List<DataTable> dts = new List<DataTable>();

            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        try
                        {
                            for (Int32 sqlIndex = 0; sqlIndex < _sQLs.Count; sqlIndex++)
                            {
                                cmd.CommandText = _sQLs[sqlIndex];

                                AddParametersOracle(cmd, sqlIndex);

                                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                                {
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        dts.Add(dt);
                                    }
                                }

                                //ensure to clear current OracleCommand to accept new list of parameters (if given)
                                cmd.Parameters.Clear();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            return dts;
        }

        private List<DataTable> GetDataTablesPostgres()
        {
            List<DataTable> dts = new List<DataTable>();

            using (NpgsqlConnection conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        try
                        {
                            for (Int32 sqlIndex = 0; sqlIndex < _sQLs.Count; sqlIndex++)
                            {
                                cmd.CommandText = _sQLs[sqlIndex];

                                AddParametersPostgres(cmd, sqlIndex);

                                using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                                {
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        dts.Add(dt);
                                    }
                                }

                                //ensure to clear current OracleCommand to accept new list of parameters (if given)
                                cmd.Parameters.Clear();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            return dts;
        }

        private void AddParametersOracle(OracleCommand cmd, Int32 sqlIndex)
        {
            if (_paramsOracle.Count > sqlIndex)
            {
                //TODO: Can use this to enhance for verbose logging
                //System.Diagnostics.Debug.WriteLine(SQLs[sqlIndex]);
                foreach (OracleParameter p in _paramsOracle[sqlIndex])
                {
                    //System.Diagnostics.Debug.WriteLine(p.ParameterName + "=" + p.Value);
                    cmd.Parameters.Add(p);
                }
            }
        }

        private void AddParametersPostgres(NpgsqlCommand cmd, Int32 sqlIndex)
        {
            if (_paramsPostgres.Count > sqlIndex)
            {
                //TODO: Can use this to enhance for verbose logging
                //System.Diagnostics.Debug.WriteLine(SQLs[sqlIndex]);
                foreach (NpgsqlParameter p in _paramsPostgres[sqlIndex])
                {
                    //System.Diagnostics.Debug.WriteLine(p.ParameterName + "=" + p.Value);
                    cmd.Parameters.Add(p);
                }
            }
        }

        public List<Int32> ExecuteSQLs()
        {
            if (DBType == DatabaseTypes.ORACLE)
                return ExecuteSQLsOracle();
            else
                return ExecuteSQLsPostgres();
        }

        private List<Int32> ExecuteSQLsOracle()
        {
            List<Int32> affectedRecords = new List<Int32>();

            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        using (OracleTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                for (Int32 sqlIndex = 0; sqlIndex < _sQLs.Count; sqlIndex++)
                                {
                                    cmd.CommandText = _sQLs[sqlIndex];

                                    AddParametersOracle(cmd, sqlIndex);

                                    affectedRecords.Add(cmd.ExecuteNonQuery());

                                    if ((_expectedResultCounts[sqlIndex] != 0) && (affectedRecords[sqlIndex] != _expectedResultCounts[sqlIndex]))
                                        throw new DataSourceExceptionAffectedRecords(affectedRecords[sqlIndex] + " records affected when expecting " + _expectedResultCounts[sqlIndex] + ".\r\n" + GetSQLErrorMessage(cmd.CommandText, cmd.Parameters));

                                    //ensure to clear current OracleCommand to accept new list of parameters (if given)
                                    cmd.Parameters.Clear();

                                }

                                trans.Commit();
                            }
                            catch (DataSourceExceptionAffectedRecords)
                            {
                                trans.Rollback();
                                throw;
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return affectedRecords;
        }

        private List<Int32> ExecuteSQLsPostgres()
        {
            List<Int32> affectedRecords = new List<Int32>();

            using (NpgsqlConnection conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        using (NpgsqlTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                for (Int32 sqlIndex = 0; sqlIndex < _sQLs.Count; sqlIndex++)
                                {
                                    cmd.CommandText = _sQLs[sqlIndex];

                                    AddParametersPostgres(cmd, sqlIndex);

                                    affectedRecords.Add(cmd.ExecuteNonQuery());

                                    if ((_expectedResultCounts[sqlIndex] != 0) && (affectedRecords[sqlIndex] != _expectedResultCounts[sqlIndex]))
                                        throw new DataSourceExceptionAffectedRecords(affectedRecords[sqlIndex] + " records affected when expecting " + _expectedResultCounts[sqlIndex] + ".\r\n" + GetSQLErrorMessage(cmd.CommandText, cmd.Parameters));

                                    //ensure to clear current OracleCommand to accept new list of parameters (if given)
                                    cmd.Parameters.Clear();

                                }

                                trans.Commit();
                            }
                            catch (DataSourceExceptionAffectedRecords)
                            {
                                trans.Rollback();
                                throw;
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return affectedRecords;
        }

        private String GetSQLErrorMessage(String sql, OracleParameterCollection parameters)
        {
            String error = "SQL:\r\n" + sql + "\r\nParameters:";
            foreach (OracleParameter p in parameters)
                error += "\r\n" + p.ParameterName + "(" + p.DbType.ToString() + ")=" + p.Value;
            return error;
        }

        private String GetSQLErrorMessage(String sql, NpgsqlParameterCollection parameters)
        {
            String error = "SQL:\r\n" + sql + "\r\nParameters:";
            foreach (NpgsqlParameter p in parameters)
                error += "\r\n" + p.ParameterName + "(" + p.NpgsqlDbType.ToString() + ")=" + p.Value;
            return error;
        }

        public List<String> ReadCLOBs(String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            if (DBType == DatabaseTypes.ORACLE)
                return ReadCLOBsOracle(clobDBColumName, tableName, whereClauseColumnsValues);
            else
                return ReadCLOBsPostgres(clobDBColumName, tableName, whereClauseColumnsValues);
        }

        private List<String> ReadCLOBsOracle(String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            List<String> ret = new List<String>();

            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        cmd.CommandText = "SELECT " + clobDBColumName + " FROM " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);

                        try
                        {
                            using (OracleDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                    ret.Add(dr.GetOracleClob(0).Value);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        private List<String> ReadCLOBsPostgres(String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            List<String> ret = new List<String>();

            using (NpgsqlConnection conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        cmd.CommandText = "SELECT " + clobDBColumName + " FROM " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);

                        try
                        {
                            using (NpgsqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                    ret.Add(dr.GetString(0));
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        private Dictionary<String, Object> GetParameterNameValues(Dictionary<String, Object> whereClauseColumnsValues, out String whereClause)
        {
            Dictionary<String, Object> paramNameVals = new Dictionary<String, Object>();
            whereClause = String.Empty;

            if (whereClauseColumnsValues != null && whereClauseColumnsValues.Count > 0)
            {
                whereClause = " WHERE ";
                foreach (KeyValuePair<String, Object> kvp in whereClauseColumnsValues)
                {
                    //handle a list of given values for one search criteria as an IN clause
                    if (kvp.Value is List<Object> list)
                    {
                        whereClause += kvp.Key + " IN (";
                        Int32 i = 1;
                        foreach (Object obj in list)
                        {
                            whereClause += PARAM_PREFIX + kvp.Key + i + ",";
                            paramNameVals.Add(PARAM_PREFIX + kvp.Key + i, obj);
                            i += 1;
                        }
                        whereClause = whereClause.Trim(',') + ") AND ";
                    }
                    //otherwise, handle normally
                    else
                    {
                        whereClause += kvp.Key + " = " + PARAM_PREFIX + kvp.Key + " AND ";
                        paramNameVals.Add(PARAM_PREFIX + kvp.Key, kvp.Value);
                    }
                }
                whereClause = whereClause.Substring(0, whereClause.Length - 5);//remove last AND
            }
            return paramNameVals;
        }

        private String ConstructCLOBWhereClause(OracleCommand cmd, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Dictionary<String, Object> paramNameVals = GetParameterNameValues(whereClauseColumnsValues, out String whereClause);
            foreach (KeyValuePair<String, Object> kvp in paramNameVals)
                cmd.Parameters.Add(kvp.Key, kvp.Value);
            return whereClause;
        }

        private String ConstructCLOBWhereClause(NpgsqlCommand cmd, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Dictionary<String, Object> paramNameVals = GetParameterNameValues(whereClauseColumnsValues, out String whereClause);
            foreach (KeyValuePair<String, Object> kvp in paramNameVals)
                cmd.Parameters.Add(new NpgsqlParameter(kvp.Key, kvp.Value));
            return whereClause;
        }

        private String ConstructCLOBInsertClause(OracleCommand cmd, String clobValue, String clobDBColumName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            String ret = String.Empty;
            String values = String.Empty;

            if (whereClauseColumnsValues != null && whereClauseColumnsValues.Count > 0)
            {
                ret = " (";
                values = " VALUES (";
                foreach (KeyValuePair<String, Object> kvp in whereClauseColumnsValues)
                {
                    ret += kvp.Key + ", ";
                    values += PARAM_PREFIX + kvp.Key + ", ";
                    cmd.Parameters.Add(PARAM_PREFIX + kvp.Key, kvp.Value);
                }
                ret += clobDBColumName + ") ";
                values += PARAM_PREFIX + clobDBColumName + ")";

                OracleParameter p = new OracleParameter(PARAM_PREFIX + clobDBColumName, OracleDbType.Clob, clobValue.Length);
                p.Direction = ParameterDirection.Input;
                OracleClob clob = new OracleClob(cmd.Connection);
                clob.Write(clobValue.ToCharArray(), 0, clobValue.Length);
                p.Value = clob;
                cmd.Parameters.Add(p);

            }
            return ret + values;
        }

        private String ConstructCLOBInsertClause(NpgsqlCommand cmd, String clobValue, String clobDBColumName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            String ret = String.Empty;
            String values = String.Empty;

            if (whereClauseColumnsValues != null && whereClauseColumnsValues.Count > 0)
            {
                ret = " (";
                values = " VALUES (";
                foreach (KeyValuePair<String, Object> kvp in whereClauseColumnsValues)
                {
                    ret += kvp.Key + ", ";
                    values += PARAM_PREFIX + kvp.Key + ", ";
                    cmd.Parameters.Add(new NpgsqlParameter(PARAM_PREFIX + kvp.Key, kvp.Value));
                }
                ret += clobDBColumName + ") ";
                values += PARAM_PREFIX + clobDBColumName + ")";

                NpgsqlParameter p = new NpgsqlParameter(PARAM_PREFIX + clobDBColumName, clobValue);
                p.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(p);
            }
            return ret + values;
        }

        private String ConstructCLOBUpdateClause(OracleCommand cmd, String clobValue, String clobDBColumName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            String set;
            String where = String.Empty;
            OracleParameter p = new OracleParameter(PARAM_PREFIX + clobDBColumName, OracleDbType.Clob, clobValue.Length);
            p.Direction = ParameterDirection.Input;
            OracleClob clob = new OracleClob(cmd.Connection);
            clob.Write(clobValue.ToCharArray(), 0, clobValue.Length);
            p.Value = clob;
            set = " SET " + clobDBColumName + " = " + cmd.Parameters.Add(p);
            if (whereClauseColumnsValues != null && whereClauseColumnsValues.Count > 0)
                where = ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);
            return set + where;
        }

        private String ConstructCLOBUpdateClause(NpgsqlCommand cmd, String clobValue, String clobDBColumName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            String set;
            String where = String.Empty;
            NpgsqlParameter p = new NpgsqlParameter(PARAM_PREFIX + clobDBColumName, clobValue);
            p.Direction = ParameterDirection.Input;
            set = " SET " + clobDBColumName + " = " + cmd.Parameters.Add(p);
            if (whereClauseColumnsValues != null && whereClauseColumnsValues.Count > 0)
                where = ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);
            return set + where;
        }

        public Int32 UpdateCLOB(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            if (DBType == DatabaseTypes.ORACLE)
                return UpdateCLOBOracle(clobValue, clobDBColumName, tableName, whereClauseColumnsValues);
            else
                return UpdateCLOBPostgres(clobValue, clobDBColumName, tableName, whereClauseColumnsValues);
        }

        private Int32 UpdateCLOBOracle(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Int32 ret = 0;

            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "UPDATE " + tableName + ConstructCLOBUpdateClause(cmd, clobValue, clobDBColumName, whereClauseColumnsValues);

                        using (OracleTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                using (OracleDataReader dr = cmd.ExecuteReader())
                                {
                                    ret = cmd.ExecuteNonQuery();
                                    trans.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        private Int32 UpdateCLOBPostgres(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Int32 ret = 0;

            using (NpgsqlConnection conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "UPDATE " + tableName + ConstructCLOBUpdateClause(cmd, clobValue, clobDBColumName, whereClauseColumnsValues);

                        using (NpgsqlTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                                {
                                    ret = cmd.ExecuteNonQuery();
                                    trans.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        public Int32 SaveCLOB(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            if (DBType == DatabaseTypes.ORACLE)
                return SaveCLOBOracle(clobValue, clobDBColumName, tableName, whereClauseColumnsValues);
            else
                return SaveCLOBPostgres(clobValue, clobDBColumName, tableName, whereClauseColumnsValues);
        }

        private Int32 SaveCLOBOracle(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Int32 ret = 0;

            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (OracleCommand cmd = new OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT Count(*) FROM " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);

                        using (OracleTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                using (OracleDataReader dr = cmd.ExecuteReader())
                                {
                                    if (dr.Read())
                                    {
                                        cmd.CommandText = "DELETE " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);
                                        cmd.ExecuteNonQuery();
                                        //ensure to clear current OracleCommand to accept new list of parameters
                                        cmd.Parameters.Clear();
                                    }

                                    cmd.CommandText = "INSERT INTO " + tableName + ConstructCLOBInsertClause(cmd, clobValue, clobDBColumName, whereClauseColumnsValues);

                                    ret = cmd.ExecuteNonQuery();
                                    trans.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        private Int32 SaveCLOBPostgres(String clobValue, String clobDBColumName, String tableName, Dictionary<String, Object> whereClauseColumnsValues)
        {
            Int32 ret = 0;

            using (NpgsqlConnection conn = new NpgsqlConnection(ConnectionString))
            {
                conn.Open();

                try
                {
                    using (NpgsqlCommand cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT Count(*) FROM " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);

                        using (NpgsqlTransaction trans = conn.BeginTransaction())
                        {
                            try
                            {
                                using (NpgsqlDataReader dr = cmd.ExecuteReader())
                                {
                                    if (dr.Read())
                                    {
                                        cmd.CommandText = "DELETE " + tableName + ConstructCLOBWhereClause(cmd, whereClauseColumnsValues);
                                        cmd.ExecuteNonQuery();
                                        //ensure to clear current OracleCommand to accept new list of parameters
                                        cmd.Parameters.Clear();
                                    }

                                    cmd.CommandText = "INSERT INTO " + tableName + ConstructCLOBInsertClause(cmd, clobValue, clobDBColumName, whereClauseColumnsValues);

                                    ret = cmd.ExecuteNonQuery();
                                    trans.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                throw new Exception(GetSQLErrorMessage(cmd.CommandText, cmd.Parameters), ex);
                            }
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return ret;
        }

        public List<String> GetLocalMachineTNSList()
        {
            List<String> items = new List<String>();

            OracleClientFactory factory = new OracleClientFactory();
            if (factory.CanCreateDataSourceEnumerator)
            {
                System.Data.Common.DbDataSourceEnumerator tnsEnum = factory.CreateDataSourceEnumerator();
                if (tnsEnum != null)
                {
                    DataTable dtF = tnsEnum.GetDataSources();
                    foreach (DataRow row in dtF.Rows)
                        items.Add(row[0].ToString().ToUpper());//uppercase as oracle will will combine dup mixed case tns entries together taking last one, so no mixed case
                }
            }

            return items;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Clear();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

    }
}
