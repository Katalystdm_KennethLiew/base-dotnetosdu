﻿using System;
using System.Windows.Forms;

namespace Framework.Controls
{
    public class ComboBoxStyled : ComboBox
    {

        public Boolean AllowEmptyEntry { get; set; } = false;
        public Boolean AllowEntryNotInList { get; set; } = false;

        public ComboBoxStyled()
        {
            //this allows end-user to type in the box and have the list auto-filtered as they type
            this.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.AutoCompleteSource = AutoCompleteSource.ListItems;

            this.Validating += new System.ComponentModel.CancelEventHandler(ComboBox_Validating);
        }

        private void ComboBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //do not allow entering text which is not an item in the list if AllowEntryNotInList is false
            //do not allow entering empty value if AllowEmptyEntry is false
            if ((!AllowEntryNotInList && (((ComboBox)sender).SelectedIndex == -1 && ((ComboBox)sender).Items.Count > 0 && ((ComboBox)sender).Text.Length > 0)) ||
            (!AllowEmptyEntry && (((ComboBox)sender).SelectedIndex == -1 && ((ComboBox)sender).Items.Count > 0 && ((ComboBox)sender).Text.Length == 0)))
            {
                e.Cancel = true;
                MessageBox.Show(this.Parent.FindForm(), "Please choose an existing value from within the drop down list.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //clear control if bad value and AllowEntryNotInList is false but there is no list populated
            //this done to clear text in case the list is not populated (probably due to some other issue) but don't want to lock user in the control
            else if (!AllowEntryNotInList && (((ComboBox)sender).SelectedIndex == -1 && ((ComboBox)sender).Text.Length > 0))
                ((ComboBox) sender).Text = String.Empty;
        }

    }
}
