﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Data;

namespace Framework.Controls
{
    public class ContextMenuColumnPreferences : IDisposable
    {
        //implementing IDisposable 
        private Boolean _disposed = false; // to detect redundant calls

        private MenuItem _mi = null;

        private const String RESET_MENU = "RESET";

        public event EventHandler SaveDisplayFormatEvent;
        public event EventHandler SaveDisplayFormatAsEvent;
        internal event EventHandler<ColumnPreferencesEventArgs> ColumnPreferencesEvent;
        public class ColumnPreferencesEventArgs : EventArgs
        {
            public enum OPERATION
            {
                GoTo,
                Hide,
                HideAllToRight,
                Insert,
                Rename,
                ResetColumn,
                ResetDisplayPrefs,
                Save,
                SaveAs,
                UnHide,
                UnHideAll
            }
            public OPERATION Operation { get; set; }
            public DataGridViewColumn DataGridViewColumn { get; set; }
            public ColumnPreferencesEventArgs(OPERATION operation, DataGridViewColumn dataGridViewColumn)
            {
                Operation = operation;
                DataGridViewColumn = dataGridViewColumn;
            }
        }

        private readonly DataGridView _dataGridView = null;
        private ContextMenu _columnHeaderMenu = null;
        private ContextMenu _cellMenu = null;
        private Int32 _indexOfCopyToMenuItem;
        public String SelectedDisplayFormat { get; set; } = String.Empty;

        public List<String> FieldGroups { get; set; }
        public Boolean AllowRename { get; set; } = true;
        public Boolean AllowHideUnhide { get; set; } = true;
        public Boolean AllowInsert { get; set; } = true;
        public Boolean AllowGoTo { get; set; } = true;
        public Boolean AllowReset { get; set; } = true;
        public Boolean AllowSave { get; set; } = true;
        public Boolean AllowSaveAs { get; set; } = true;
        public Boolean AllowAutoSequenceDown { get; set; } = true;
        public Boolean AllowAutoFillDown { get; set; } = true;
        public Boolean AllowCopyToAllSelectedEmptyRows { get; set; } = true;

        /// <summary>
        /// To use <c>fieldGroups</c>, prefix each column with the groupName + "." + columnName and add each groupName to <c>fieldGroups</c>.
        /// This will create new sub-menus for each fieldGroup under each of Hide/Unhide, Insert and GoTo.
        /// Any columns not prefixed with groupName + "." or excluded from <c>fieldGroups</c> will be shown in sibling-menus with all other groupNames in alpha order.
        /// </summary>
        public ContextMenuColumnPreferences(DataGridView dgv, Boolean allowRename, Boolean allowHideUnhide, Boolean allowInsert, Boolean allowGoTo, Boolean allowReset, Boolean allowSave, Boolean allowSaveAs, Boolean allowAutoSequenceDown, Boolean allowAutoFillDown, Boolean allowCopyToAllSelectedEmptyRows, List<String> fieldGroups)
        {
            _dataGridView = dgv;
            AllowRename = allowRename;
            AllowHideUnhide = allowHideUnhide;
            AllowInsert = allowInsert;
            AllowGoTo = allowGoTo;
            AllowReset = allowReset;
            AllowSave = allowSave;
            AllowSaveAs = allowSaveAs;
            AllowAutoSequenceDown = allowAutoSequenceDown;
            AllowAutoFillDown = allowAutoFillDown;
            AllowCopyToAllSelectedEmptyRows = allowCopyToAllSelectedEmptyRows;
            FieldGroups = fieldGroups;
            _dataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridView_ColumnHeaderMouseClick);
            _dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvLoadedDataSets_CellMouseClick);
        }

        /// <summary>
        /// Rebuilds the context menu including any changes applied.
        /// </summary>
        public void Refresh()
        {
            //separate Insert, GoTo & Hidden lists for each fieldGroup
            String headerText;
            SortedList<String, SortedList<String, MenuItem>> menuColumnsInsertGroups = new SortedList<String, SortedList<String, MenuItem>>();
            SortedList<String, SortedList<String, MenuItem>> menuColumnsGoToGroups = new SortedList<String, SortedList<String, MenuItem>>();
            SortedList<String, SortedList<String, MenuItem>> menuColumnsHiddenGroups = new SortedList<String, SortedList<String, MenuItem>>();

            SortedList<String, MenuItem> menuColumnsInsertOther = new SortedList<String, MenuItem>();
            SortedList<String, MenuItem> menuColumnsGoToOther = new SortedList<String, MenuItem>();
            SortedList<String, MenuItem> menuColumnsHiddenOther = new SortedList<String, MenuItem>();

            if (FieldGroups != null && FieldGroups.Count > 0)
            {
                foreach (String fieldGroup in FieldGroups)
                {
                    menuColumnsInsertGroups.Add(fieldGroup, new SortedList<String, MenuItem>());
                    menuColumnsGoToGroups.Add(fieldGroup, new SortedList<String, MenuItem>());
                    menuColumnsHiddenGroups.Add(fieldGroup, new SortedList<String, MenuItem>());
                }
            }

            foreach (DataGridViewColumn c in _dataGridView.Columns)
            {

                //get header names with field group prefixes removed as those will become parent menu items
                headerText = RemoveFieldGroup(c.HeaderText);

                if (c.Visible)
                {
                    _mi = new MenuItem(headerText, DataGridView_ColumnHeaderMenu_Insert_Click);
                    _mi.Tag = c.Name;
                    AddColumnToMenuGroups(menuColumnsInsertGroups, menuColumnsInsertOther, c, _mi, headerText);

                    _mi = new MenuItem(headerText, DataGridView_ColumnHeaderMenu_GoTo_Click);
                    _mi.Tag = c.Name;
                    AddColumnToMenuGroups(menuColumnsGoToGroups, menuColumnsGoToOther, c, _mi, headerText);
                }
                else
                {
                    _mi = new MenuItem(headerText, DataGridView_ColumnHeaderMenu_UnHide_Click);
                    _mi.Tag = c.Name;
                    AddColumnToMenuGroups(menuColumnsHiddenGroups, menuColumnsHiddenOther, c, _mi, headerText);
                }
            }

            _columnHeaderMenu = new ContextMenu();

            //tags for these ones have to be set during menu show within DataGridView_ColumnHeaderMouseClick since we go off which column header is clicked on
            //***************************************
            _mi = new MenuItem("Rename Column", DataGridView_ColumnHeaderMenu_Rename_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowRename;

            //the caption for this one will be created on click so can pull the existing column name into it
            _mi = new MenuItem("", DataGridView_ColumnHeaderMenu_Reset_Click);
            _mi.Name = RESET_MENU;
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowRename;

            _mi = new MenuItem("Hide Column", DataGridView_ColumnHeaderMenu_Hide_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowHideUnhide;

            _mi = new MenuItem("Hide All To Right", DataGridView_ColumnHeaderMenu_HideAllToRight_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowHideUnhide;
            //***************************************
            
            //tags for these ones come from the given tag value of the menu item created within each list
            //***************************************
            _mi = new MenuItem("Un-Hide Column", AddMenuItemArrayToGroups(menuColumnsHiddenGroups, menuColumnsHiddenOther));
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowHideUnhide;

            _mi = new MenuItem("Un-Hide All", DataGridView_ColumnHeaderMenu_UnHideAll_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowHideUnhide;

            _mi = new MenuItem("Insert Column", AddMenuItemArrayToGroups(menuColumnsInsertGroups, menuColumnsInsertOther));
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowInsert;

            _mi = new MenuItem("GoTo Column", AddMenuItemArrayToGroups(menuColumnsGoToGroups, menuColumnsGoToOther));
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowGoTo;

            _mi = new MenuItem("Reset Display Preferences to last saved", DataGridView_ColumnHeaderMenu_ResetDisplayPrefs_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowReset;

            _mi = new MenuItem("Save Display Preferences", DataGridView_ColumnHeaderMenu_SaveDisplayPrefs_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowSave;

            _mi = new MenuItem("Save Display Preferences As...", DataGridView_ColumnHeaderMenu_SaveDisplayPrefsAs_Click);
            _columnHeaderMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowSaveAs;

            _cellMenu = new ContextMenu();
            _mi = new MenuItem("Auto-Sequence down from here", DataGridView_CellMenu_AutoSequenceDown_Click);
            _cellMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowAutoSequenceDown;

            _mi = new MenuItem("Auto-Fill down from here", DataGridView_CellMenu_AutoFillDown_Click);
            _cellMenu.MenuItems.Add(_mi);
            _mi.Visible = AllowAutoFillDown;

            MenuItem[] miAllSelectedEmpty = new MenuItem[3];
            miAllSelectedEmpty[0] = new MenuItem("All rows", DataGridView_CellMenu_AutoFillAll_Click);
            miAllSelectedEmpty[1] = new MenuItem("Selected rows", DataGridView_CellMenu_AutoFillSelected_Click);
            miAllSelectedEmpty[2] = new MenuItem("Empty rows", DataGridView_CellMenu_AutoFillEmpty_Click);
            _mi = new MenuItem("Copy value to", miAllSelectedEmpty);
            _cellMenu.MenuItems.Add(_mi);
            _indexOfCopyToMenuItem = _cellMenu.MenuItems.Count - 1;
            _mi.Visible = AllowCopyToAllSelectedEmptyRows;
            //***************************************

        }

        private MenuItem[] AddMenuItemArrayToGroups(SortedList<String, SortedList<String, MenuItem>> menuGroups, SortedList<String, MenuItem> menusOther)
        {
            MenuItem[] miArray;
            Int32 i = 0;
            if (FieldGroups != null && FieldGroups.Count > 0)
            {
                miArray = new MenuItem[FieldGroups.Count + menusOther.Count];//first are for the field groups, plus all the rest not in a group
                foreach (String fieldGroup in FieldGroups)
                {
                    miArray[i] = new MenuItem(fieldGroup.TrimEnd('.'), menuGroups[fieldGroup].Values.ToArray());
                    i++;
                }
            }
            else
            {
                miArray = new MenuItem[menusOther.Count];
            }
            foreach (MenuItem m in menusOther.Values)
            {
                miArray[i] = m;
                i += 1;
            }
            return miArray;
        }

        private void AddColumnToMenuGroups(SortedList<String, SortedList<String, MenuItem>> menuGroups, SortedList<String, MenuItem> menusOther, DataGridViewColumn c, MenuItem mi, String headerText)
        {
            Boolean added = false;
            if (FieldGroups != null && FieldGroups.Count > 0)
            {
                foreach (String fieldGroup in FieldGroups)
                {
                    if (c.Name.StartsWith(fieldGroup))
                    {
                        if (!menuGroups.ContainsKey(fieldGroup))
                            menuGroups.Add(fieldGroup, new SortedList<String, MenuItem>());
                        menuGroups[fieldGroup].Add(headerText, mi);
                        added = true;
                        break;
                    }
                }
            }
            if (!added)
                menusOther.Add(headerText, mi);
        }

        private String RemoveFieldGroup(String input)
        {
            if (FieldGroups != null && FieldGroups.Count > 0)
            {
                foreach (String fieldGroup in FieldGroups)
                    if (input.StartsWith(fieldGroup))
                        return input.Substring(fieldGroup.Length);
            }
            return input;
        }

        private void DataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (AllowRename || AllowHideUnhide || AllowInsert || AllowGoTo || AllowSave || AllowSaveAs || AllowAutoSequenceDown || AllowAutoFillDown)
                {
                    //get the location of the mouse click
                    Point p = GetColumnMouseClickPositionWithinGrid(((DataGridView)sender), e.ColumnIndex, e.Location);
                    //set the tag value of the parent context menu to the current column name clickd on so subsequent click events on the menu items can use to find the column
                    //Note, the column header text may be different than the DB column name we put in the tag value of each menu item
                    _columnHeaderMenu.Tag = ((DataGridView)sender).Columns[e.ColumnIndex].Name;
                    if (AllowRename)
                    {
                        _columnHeaderMenu.MenuItems[RESET_MENU].Text = "Reset Column Name To [" + _columnHeaderMenu.Tag + "]";
                        if (!((DataGridView)sender).Columns[e.ColumnIndex].Name.Equals(((DataGridView)sender).Columns[e.ColumnIndex].HeaderText))
                            _columnHeaderMenu.MenuItems[RESET_MENU].Visible = true;
                        else
                            _columnHeaderMenu.MenuItems[RESET_MENU].Visible = false;
                    }
                    _columnHeaderMenu.Show(((DataGridView)sender), p);
                }
            }
        }
        private void DgvLoadedDataSets_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //be sure to check rowindex here otherwise, this will override the DataGridView_ColumnHeaderMouseClick (rowindex == -1 means column header)
            //and be sure to check columnindex, columnindex == -1 means row header
            //and be sure to check column is editable
            //and be sure to check cell is editable (editability may be set a cell level but not at column level)
            if (e.Button == MouseButtons.Right && e.ColumnIndex > -1 && e.RowIndex > -1 && !((DataGridView)sender).Columns[e.ColumnIndex].ReadOnly && !((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly)
            {
                if (AllowAutoSequenceDown || AllowAutoFillDown || AllowCopyToAllSelectedEmptyRows)
                {
                    //get the location of the mouse click
                    Point p = GetCellMouseClickPositionWithinGrid(((DataGridView)sender), e.ColumnIndex, e.RowIndex, e.Location);
                    //set the tag value of the parent context menu to the current column name clickd on so subsequent click events on the menu items can use to find the column
                    //Note, the column header text may be different than the DB column name we put in the tag value of each menu item
                    _cellMenu.Tag = ((DataGridView)sender).Columns[e.ColumnIndex].Name + "|" + e.RowIndex;

                    //in this case we want to include the clicked on cell value in the menu item text
                    if (AllowCopyToAllSelectedEmptyRows)
                        //Be sure to always take the cell's EditedFormattedValue which may different than Value if the cell happens to be a DataGridViewComboBoxCell (where parent Column is a DataGridViewComboBoxColumn).
                        //The DataGridViewComboBoxColumn may have a different DisplayMember than ValueMember and we want to be sure to always show the DisplayMember value.
                        _cellMenu.MenuItems[_indexOfCopyToMenuItem].Text = "Copy [" + ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue + "] to";

                    _cellMenu.Show(((DataGridView)sender), p);
                }
            }
        }
        private void DataGridView_ColumnHeaderMenu_Hide_Click(Object sender, System.EventArgs e)
        {
            //hide the selected column
            //use tag value of parent context menu to find column which is set in xxx_ColumnHeaderMouseClick (before ContextMenu.Show)
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);

            if (dgv.Columns.GetColumnCount(DataGridViewElementStates.Visible) == 1)
            {
                MessageBox.Show(_dataGridView.Parent.FindForm(), "As least one column must remain visible.", ((MenuItem)sender).Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                //hide the selected column
                dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].Visible = false;
                //update context menu
                Refresh();
            }

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.Hide, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_HideAllToRight_Click(Object sender, System.EventArgs e)
        {
            //hide all columns having a DisplayIndex greater than the current one
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 currentIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].DisplayIndex;
            foreach (DataGridViewColumn c in dgv.Columns)
            {
                if (c.DisplayIndex > currentIndex)
                    c.Visible = false;
            }

            //update context menu
            Refresh();

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.HideAllToRight, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_UnHide_Click(Object sender, System.EventArgs e)
        {
            //hide the selected column
            //use tag value of explicit menu item to find column which is set in xxx_ColumnHeaderMouseClick (before ContextMenu.Show)
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            dgv.Columns[((MenuItem)sender).Tag.ToString()].Visible = true;

            //update context menu
            Refresh();

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.UnHide, dgv.Columns[((MenuItem)sender).Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_UnHideAll_Click(Object sender, System.EventArgs e)
        {
            //un-hide all columns
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            foreach (DataGridViewColumn c in dgv.Columns)
                c.Visible = true;

            //update context menu
            Refresh();

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.UnHideAll, null));
        }
        private void DataGridView_ColumnHeaderMenu_Insert_Click(Object sender, System.EventArgs e)
        {
            //set select column display index to the clicked-on column display index
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            dgv.Columns[((MenuItem)sender).Tag.ToString()].DisplayIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].DisplayIndex;
            //Note, this will cause the FirstDisplayedScrollingColumnIndex to be off sync.
            //Not sure how to correct yet hence why using GoTo method as is.

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.Insert, dgv.Columns[((MenuItem)sender).Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_GoTo_Click(Object sender, System.EventArgs e)
        {
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            if (dgv.CurrentRow != null)
                dgv.CurrentCell = dgv.Rows[dgv.CurrentRow.Index].Cells[dgv.Columns[((MenuItem)sender).Tag.ToString()].Index];
            dgv.FindForm().ActiveControl = dgv;
            if (!dgv.Columns[((MenuItem)sender).Tag.ToString()].Displayed)
                dgv.FirstDisplayedScrollingColumnIndex = dgv.Columns[((MenuItem)sender).Tag.ToString()].Index;
            //Note, cannot just do like below since the display index goes out of sync as soon as we insert a column.
            //dgv.FirstDisplayedScrollingColumnIndex = dgv.Columns[((MenuItem)sender).Tag.ToString()].DisplayIndex;

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.GoTo, dgv.Columns[((MenuItem)sender).Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_Rename_Click(Object sender, System.EventArgs e)
        {
            //get the location of the column
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Point p = GetColumnPositionOnScreen(dgv, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].Index);

            String newName = Common.GetInput("Enter new column name:", dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].HeaderText, false, ((MenuItem)sender).GetContextMenu().SourceControl.TopLevelControl, p);
            if (newName.Length > 0)
            {
                bool exists = false;
                foreach (DataGridViewColumn c in dgv.Columns)
                {
                    if (c.HeaderText.ToLower().Equals(newName.ToLower()))
                    {
                        exists = true;
                        break;
                    }
                }
                if (exists)
                    MessageBox.Show(_dataGridView.Parent.FindForm(), "The column name [" + newName + "] is already used.  Please choose a different name.", ((MenuItem)sender).Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                    dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].HeaderText = newName;

                //update context menu
                Refresh();

                ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.Rename, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
            }
        }
        private void DataGridView_ColumnHeaderMenu_Reset_Click(Object sender, System.EventArgs e)
        {
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].HeaderText = ((MenuItem)sender).GetContextMenu().Tag.ToString();

            //update context menu
            Refresh();

            ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.ResetColumn, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
        }
        private void DataGridView_ColumnHeaderMenu_ResetDisplayPrefs_Click(Object sender, System.EventArgs e)
        {
            if (((DataGridViewStyled)((MenuItem)sender).GetContextMenu().SourceControl).HasDisplayFormat)
            {
                ((DataGridViewStyled)((MenuItem)sender).GetContextMenu().SourceControl).ResetFormat();
                ColumnPreferencesEvent?.Invoke((DataGridViewStyled)((MenuItem)sender).GetContextMenu().SourceControl, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.ResetDisplayPrefs, null));
            }
            else
                MessageBox.Show(_dataGridView.Parent.FindForm(), "No display preferences have been saved yet.  Nothing to reset to.", ((MenuItem)sender).Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void DataGridView_ColumnHeaderMenu_SaveDisplayPrefs_Click(Object sender, System.EventArgs e)
        {
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            if (SelectedDisplayFormat.Length == 0)
            {
                //get the location of the column
                Point p = GetColumnPositionOnScreen(dgv, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].Index);
                SelectedDisplayFormat = Common.GetInput("Enter new name:", SelectedDisplayFormat, false, _dataGridView.Parent.FindForm(), p).ToUpper();
            }
            if (!String.IsNullOrEmpty(SelectedDisplayFormat))
            {
                SaveDisplayFormatEvent?.Invoke(_dataGridView, EventArgs.Empty);

                ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.Save, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
            }
        }
        private void DataGridView_ColumnHeaderMenu_SaveDisplayPrefsAs_Click(Object sender, System.EventArgs e)
        {
            //get the location of the column
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Point p = GetColumnPositionOnScreen(dgv, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()].Index);
            String temp = Common.GetInput("Enter new name:", SelectedDisplayFormat, false, _dataGridView.Parent.FindForm(), p).ToUpper();
            if (!String.IsNullOrEmpty(temp))
            {
                SelectedDisplayFormat = temp;
                SaveDisplayFormatAsEvent?.Invoke(_dataGridView, EventArgs.Empty);

                ColumnPreferencesEvent?.Invoke(dgv, new ColumnPreferencesEventArgs(ColumnPreferencesEventArgs.OPERATION.SaveAs, dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString()]));
            }
        }
        private void DataGridView_CellMenu_AutoFillSelected_Click(Object sender, System.EventArgs e)
        {
            //get the grid, row & column index of clicked-on cell
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 rowIndex = Convert.ToInt32(((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[1]);
            Int32 columnIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[0]].Index;

            //take the cells value and fill it down to the bottom of the grid
            foreach (DataGridViewRow dgvr in Common.GetRowsFromSelectedCells(dgv))
            {
                dgvr.Cells[columnIndex].Value = dgv.Rows[rowIndex].Cells[columnIndex].Value;
            }
        }
        private void DataGridView_CellMenu_AutoFillAll_Click(Object sender, System.EventArgs e)
        {
            //get the grid, row & column index of clicked-on cell
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 rowIndex = Convert.ToInt32(((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[1]);
            Int32 columnIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[0]].Index;

            //take the cells value and fill it down to the bottom of the grid
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                dgvr.Cells[columnIndex].Value = dgv.Rows[rowIndex].Cells[columnIndex].Value;
            }
        }
        private void DataGridView_CellMenu_AutoFillEmpty_Click(Object sender, System.EventArgs e)
        {
            //get the grid, row & column index of clicked-on cell
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 rowIndex = Convert.ToInt32(((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[1]);
            Int32 columnIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[0]].Index;

            //take the cells value and fill it down to the bottom of the grid
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                if (dgvr.Cells[columnIndex].Value.ToString().Length == 0)
                    dgvr.Cells[columnIndex].Value = dgv.Rows[rowIndex].Cells[columnIndex].Value;
            }
        }
        private void DataGridView_CellMenu_AutoFillDown_Click(Object sender, System.EventArgs e)
        {
            //get the grid, row & column index of clicked-on cell
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 rowIndex = Convert.ToInt32(((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[1]);
            Int32 columnIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[0]].Index;

            //take the cells value and fill it down to the bottom of the grid
            for (Int32 i = rowIndex + 1; i <= dgv.RowCount-1; i++)
                dgv.Rows[i].Cells[columnIndex].Value = dgv.Rows[rowIndex].Cells[columnIndex].Value;
            if (dgv.DataSource is DataTable dt)
                dt.AcceptChanges();
            else if (dgv.DataSource is DataSet ds)
                ds.AcceptChanges();
        }
        private void DataGridView_CellMenu_AutoSequenceDown_Click(Object sender, System.EventArgs e)
        {
            //get the grid, row & column index & string value of clicked-on cell
            DataGridView dgv = ((DataGridView)((MenuItem)sender).GetContextMenu().SourceControl);
            Int32 rowIndex = Convert.ToInt32(((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[1]);
            Int32 columnIndex = dgv.Columns[((MenuItem)sender).GetContextMenu().Tag.ToString().Split('|')[0]].Index;
            String val = String.Empty;
            if (dgv.Rows[rowIndex].Cells[columnIndex].Value != null && !String.IsNullOrEmpty(dgv.Rows[rowIndex].Cells[columnIndex].Value.ToString()))
                val = dgv.Rows[rowIndex].Cells[columnIndex].Value.ToString().Trim();

            Int64 start = 0;
            String otherTxt = String.Empty;
            Boolean txtAtEnd = false;
            Boolean ok = true;

            //if there is no value in the current cell then prompt user
            if (String.IsNullOrEmpty(val))
            {
                Point p = GetCellPositionOnScreen(dgv, columnIndex, rowIndex);
                Common.ShowMessageBoxOkOnly("Auto Sequence Down", "There is no value in the clicked cell.  Please enter a value in the cell (starting or ending with numerics) in order to auto-sequence down.", ((MenuItem)sender).GetContextMenu().SourceControl.TopLevelControl, p);
                ok = false;
            }
            //if is not an integer value
            else if (!Int64.TryParse(val, out start))
            {
                //if there is are no starting or ending numerics in the current cell then prompt user
                if (!Char.IsDigit(val.Substring(0, 1)[0]) && !Char.IsDigit(val.Substring(val.Length - 1)[0]))
                {
                    Point p = GetCellPositionOnScreen(dgv, columnIndex, rowIndex);
                    Common.ShowMessageBoxOkOnly("Auto Sequence Down", "There must be a starting or ending numeric value in the cell.  Please enter a new value in the cell (starting or ending with numerics) in order to auto-sequence down.", ((MenuItem)sender).GetContextMenu().SourceControl.TopLevelControl, p);
                    ok = false;
                }
                else
                {
                    //if both starting and ending with numerics then prompt user to choose
                    if (Char.IsDigit(val.Substring(0, 1)[0]) & Char.IsDigit(val.Substring(val.Length - 1)[0]))
                    {
                        Point p = GetCellPositionOnScreen(dgv, columnIndex, rowIndex);
                        if (Common.ShowMessageBoxOkCancel("Auto Sequence Down", "Please choose which end of the text to auto-sequence.", "&Beginning", "&End", ((MenuItem)sender).GetContextMenu().SourceControl.TopLevelControl, p) == DialogResult.OK)
                            txtAtEnd = true;
                        else
                            txtAtEnd = false;
                    }
                    //starting with numeric
                    else if (Char.IsDigit(val.Substring(0, 1)[0]))
                        txtAtEnd = true;
                    //ending with numeric
                    else
                        txtAtEnd = false;

                    //pull out start and otherTxt
                    String startVal = String.Empty;
                    if (txtAtEnd)
                    {
                        foreach (Char c in val)
                        {
                            if (Char.IsDigit(c) & otherTxt.Length == 0)
                                startVal += c;
                            else
                                otherTxt += c;
                        }
                    }
                    else
                    {
                        Int32 digitStart = 0;
                        for (Int32 i = val.Length-1; i >= 0; i--)
                        {
                            if (!Char.IsDigit(val[i]))
                            {
                                break;
                            }
                            else
                                digitStart = i;
                        }
                        for (Int32 i = 0; i <= val.Length - 1; i++)
                        {
                            if (i < digitStart)
                                otherTxt += val[i];
                            else
                                startVal += val[i];
                        }
                    }

                    start = Convert.ToInt64(startVal);
                }
            }
            //is an integer, all good
            else
                ok = true;

            if (ok)
            {
                //take the cells value and fill it down to the bottom of the grid incrementing as we go
                for (Int32 i = rowIndex + 1; i <= dgv.RowCount - 1; i++)
                {
                    start += 1;
                    if (!String.IsNullOrEmpty(otherTxt))
                        if (txtAtEnd)
                            dgv.Rows[i].Cells[columnIndex].Value = start + otherTxt;
                        else
                            dgv.Rows[i].Cells[columnIndex].Value = otherTxt + start;
                    else
                        dgv.Rows[i].Cells[columnIndex].Value = start;
                }
                if (dgv.DataSource is DataTable dt)
                    dt.AcceptChanges();
                else if (dgv.DataSource is DataSet ds)
                    ds.AcceptChanges();
            }
        }
        private Point GetCellMouseClickPositionWithinGrid(DataGridView dgv, Int32 columnIndex, Int32 rowIndex, Point mouseOffset)
        {
            Point p = dgv.GetCellDisplayRectangle(columnIndex, rowIndex, true).Location;
            if (!mouseOffset.IsEmpty)
                p.Offset(mouseOffset);
            return p;
        }
        private Point GetCellPositionOnScreen(DataGridView dgv, Int32 columnIndex, Int32 rowIndex)
        {
            return dgv.PointToScreen(dgv.GetCellDisplayRectangle(columnIndex, rowIndex, true).Location);
        }
        private Point GetColumnMouseClickPositionWithinGrid(DataGridView dgv, Int32 columnIndex, Point mouseOffset)
        {
            Point p = dgv.GetColumnDisplayRectangle(columnIndex, true).Location;
            if (!mouseOffset.IsEmpty)
                p.Offset(mouseOffset);
            return p;
        }
        private Point GetColumnPositionOnScreen(DataGridView dgv, Int32 columnIndex)
        {
            return dgv.PointToScreen(dgv.GetColumnDisplayRectangle(columnIndex, true).Location);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _mi.Dispose();
                    _dataGridView.Dispose();
                    _columnHeaderMenu.Dispose();
                    _cellMenu.Dispose();
                }

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

    }
}
