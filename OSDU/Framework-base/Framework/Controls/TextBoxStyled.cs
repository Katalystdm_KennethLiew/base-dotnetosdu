﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Framework.Controls
{
    public class TextBoxStyled : TextBox
    {

        public Boolean PasteTabAndMultiLineAsCSV { get; set; } = true;

        public TextBoxStyled()
        {
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtBox_KeyDown);
        }

        private void TxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (PasteTabAndMultiLineAsCSV)
                Common.Control_KeyDown_TabsAndNewLinesToCSV(sender, e);
        }

    }
}
