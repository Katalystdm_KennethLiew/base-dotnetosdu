﻿using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Framework.Controls
{
    class DataGridViewDisplayFormat
    {
        //Key
        public String K { get; set; } //get/set required for serialization
        //List of Columns from Table or View plus additional attributes
        public List<ColumnDisplayFormat> CF { get; set; }

        public DataGridViewDisplayFormat()
        {
            CF = new List<ColumnDisplayFormat>();
        }

    }

    class ColumnDisplayFormat
    {
        public String K { get; set; } //get/set required for serialization
        public String DN { get; set; }//zero-based
        public Int32 DI { get; set; }
        public Boolean H { get; set; }
    }

}
