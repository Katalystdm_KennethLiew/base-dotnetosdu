﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data;
using System.Text.Json;

namespace Framework.Controls
{
    public class DataGridViewStyled : DataGridView
    {

        private ContextMenuColumnPreferences _contextMenuStyled = null;

        //for multi-sorting columns, DataGridView only allows one by default
        private readonly Dictionary<string, SortOrder> _sortedColumns = new Dictionary<string, SortOrder>();

        private DataGridViewDisplayFormat _displayFormat;

        public Boolean SuspendCustomActions { get; set; } = false;

        public List<String> ColumnNamesForPasteTabAndMultiLineAsCSV { get; set; } = new List<string>();

        public String SelectedDisplayFormat
        {
            get
            {
                if (_contextMenuStyled != null)
                    return _contextMenuStyled.SelectedDisplayFormat;
                else
                    return null;
            }
            set
            {
                if (_contextMenuStyled != null)
                    _contextMenuStyled.SelectedDisplayFormat = value;
            }
        }

        public Boolean HasDisplayFormat
        {
            get
            {
                if (_displayFormat != null)
                    return true;
                else
                    return false;
            }
        }

        public DataGridViewStyled()
        {
            Sorted += DataGridView_Sorted;
            EditingControlShowing += DataGridView_EditingControlShowing;
        }

        /// <summary>
        /// To use <c>fieldGroups</c>, prefix each column with the groupName + "." + columnName and add each groupName to <c>fieldGroups</c>.
        /// This will create new sub-menus for each fieldGroup under each of Hide/Unhide, Insert and GoTo.
        /// Any columns not prefixed with groupName + "." or excluded from <c>fieldGroups</c> will be shown in sibling-menus with all other groupNames in alpha order.
        /// </summary>
        public void AddContextMenu(Boolean allowRename, Boolean allowHideUnhide, Boolean allowInsert, Boolean allowGoTo, Boolean allowReset, Boolean allowSave, Boolean allowSaveAs, Boolean allowAutoSequenceDown, Boolean allowAutoFillDown, Boolean allowAutoFillAllSelectedEmpty, List<String> fieldGroups, System.EventHandler saveDisplayFormatEvent, System.EventHandler saveDisplayFormatAsEvent)
        {
            _contextMenuStyled = new ContextMenuColumnPreferences(this, allowRename, allowHideUnhide, allowInsert, allowGoTo, allowReset, allowSave, allowSaveAs, allowAutoSequenceDown, allowAutoFillDown, allowAutoFillAllSelectedEmpty, fieldGroups);
            _contextMenuStyled.SaveDisplayFormatEvent += saveDisplayFormatEvent;
            _contextMenuStyled.SaveDisplayFormatAsEvent += saveDisplayFormatAsEvent;
        }

        public Boolean HasContextMenu()
        {
            return _contextMenuStyled != null;
        }

        /// <summary>
        /// This allows calling code to hook on the GoTo, Insert, Hide/Unhide, Rename, etc. events.
        /// Meant to allow calling code to perform any follow-up functionality to match these operations. 
        /// </summary>
        public void AddColumnPreferencesEventHandler(EventHandler<ContextMenuColumnPreferences.ColumnPreferencesEventArgs> columnPreferencesEvent)
        {
            _contextMenuStyled.ColumnPreferencesEvent += columnPreferencesEvent;
        }

        public String ApplyJsonFormat(String json)
        {
            String ret;
            try
            {
                _displayFormat = JsonSerializer.Deserialize<DataGridViewDisplayFormat>(json);
                ret = ResetFormat();
            }
            catch (Exception ex)
            {
                ret = "Errored deserializing json display preferences:\n\r" + ex.ToString();
            }
            return ret;
        }

        public String ResetFormat()
        {
            String ret = String.Empty;

            if (_displayFormat != null)
            {
                String columnKey = String.Empty;
                String step = "Creating sorted list";
                try
                {
                    SuspendLayout();
                    SuspendCustomActions = true;

                    //re-create list as sorted by DisplayIndex since must set DisplayIndex in ascending order 
                    //otherwise, doesn't work correctly since setting one column's DisplayIndex affects all the others
                    SortedList<Int32, ColumnDisplayFormat> sorted = new SortedList<Int32, ColumnDisplayFormat>();
                    foreach (ColumnDisplayFormat cf in _displayFormat.CF)
                    {
                        sorted.Add(cf.DI, cf);
                    }

                    step = "Applying column format";
                    foreach (ColumnDisplayFormat cf in sorted.Values)
                    {
                        columnKey = cf.K;

                        //Use the key value for header text if nothing else is saved for it.
                        if (!String.IsNullOrEmpty(cf.DN))
                            this.Columns[cf.K].HeaderText = cf.DN;
                        else
                            this.Columns[cf.K].HeaderText = cf.K;

                        this.Columns[cf.K].DisplayIndex = cf.DI;
                        this.Columns[cf.K].Visible = !cf.H;
                    }
                }
                catch (Exception ex)
                {
                    ret = "Errored applying display preferences at step [" + step + "] and column key name of [" + columnKey + "]:" +
                    ex.ToString();
                }
                finally
                {
                    SuspendCustomActions = false;
                    ResumeLayout();
                }
            }
            return ret;
        }

        public String RetrieveJsonFormat(String name)
        {
            _displayFormat = new DataGridViewDisplayFormat();
            _displayFormat.K = name;
            ColumnDisplayFormat cf;
            foreach (DataGridViewColumn c in this.Columns)
            {
                cf = new ColumnDisplayFormat();
                cf.K = c.Name;

                //Do not save DisplayName if HeaderText is same as Key (that would be redudant and want to keep this small as possible).
                if (!cf.K.Equals(c.HeaderText))
                    cf.DN = c.HeaderText;
                else
                    //Use empty string instead of null to save space (saving null is twice as long)
                    cf.DN = String.Empty;

                cf.DI = c.DisplayIndex;
                cf.H = !c.Visible;
                _displayFormat.CF.Add(cf);
            }
            return JsonSerializer.Serialize<DataGridViewDisplayFormat>(_displayFormat);
        }

        public void RefreshGridRecordHeaders()
        {
            RefreshGridRecordHeaders(0);
        }
        public void RefreshGridRecordHeaders(int startingIndex)
        {
            foreach (DataGridViewRow r in this.Rows)
            {
                r.HeaderCell.Value = String.Format("{0}", r.Index + 1 + startingIndex);
            }
            //must call this after headers are set as only "takes effect" against newly drawn headers when called
            this.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
        }

        private void DataGridView_Sorted(object sender, EventArgs e)
        {
            //track what is being sorted
            if (Control.ModifierKeys != Keys.Shift)
                _sortedColumns.Clear();
            //reverse sort order if already sorted
            SortOrder so = ((DataGridView)sender).SortedColumn.HeaderCell.SortGlyphDirection;
            if (_sortedColumns.ContainsKey(((DataGridView)sender).SortedColumn.Name))
            {
                _sortedColumns.TryGetValue(((DataGridView)sender).SortedColumn.Name, out SortOrder soOld);
                if (soOld == SortOrder.Ascending)
                    so = SortOrder.Descending;
                else
                    so = SortOrder.Ascending;
                _sortedColumns.Remove(((DataGridView)sender).SortedColumn.Name);
            }
            _sortedColumns.Add(((DataGridView)sender).SortedColumn.Name, so);

            //if having more than one column being sorted
            if (_sortedColumns.Count > 1)
            {
                //use source dataview to apply multisorts
                DataView dv = ((DataTable)((DataGridView)sender).DataSource).DefaultView;
                String sortString = String.Empty;
                foreach (KeyValuePair<string, SortOrder> kvp in _sortedColumns)
                {
                    if (kvp.Value == SortOrder.Ascending)
                        sortString += kvp.Key + " ASC, ";
                    else
                        sortString += kvp.Key + " DESC, ";
                }
                sortString = sortString.TrimEnd(new char[] { ' ', ',' });
                dv.Sort = sortString;

                //reset the displayed glyph properly for each column being sorted, this resets after above dv.Sort call so must execute this after that
                foreach (KeyValuePair<string, SortOrder> kvp in _sortedColumns)
                {
                    if (kvp.Value == SortOrder.Ascending)
                        ((DataGridView)sender).Columns[kvp.Key].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                    else
                        ((DataGridView)sender).Columns[kvp.Key].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }

            RefreshGridRecordHeaders();
        }

        public void RefreshColumnHeaderContextMenu()
        {
            _contextMenuStyled.Refresh();
        }

        public void DataGridViewCell_KeyDown(object sender, KeyEventArgs e)
        {
            //need to check the column name here also in case user enters edit mode on a cell with this feature and then next on another cell without this feature
            if (ColumnNamesForPasteTabAndMultiLineAsCSV.Contains(((DataGridView)((TextBox)sender).Parent.Parent).CurrentCell.OwningColumn.Name))
                Common.Control_KeyDown_TabsAndNewLinesToCSV(sender, e);
        }

        private void DataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //Only add the event for column names which are meant to have this feature.
            //Note the event may still be in effect if user then enters edit mode on another different cell which is not meant to have this feature.
            //That is handled by having same check within the KeyDown event.
            if (e.Control is DataGridViewTextBoxEditingControl tb && ColumnNamesForPasteTabAndMultiLineAsCSV.Contains(((DataGridView)sender).CurrentCell.OwningColumn.Name))
            {
                tb.KeyDown -= DataGridViewCell_KeyDown;
                tb.KeyDown += DataGridViewCell_KeyDown;
            }
        }

    }
}
