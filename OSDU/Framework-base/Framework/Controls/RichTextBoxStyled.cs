﻿using System;
using System.Windows.Forms;

namespace Framework.Controls
{
    public class RichTextBoxStyled : RichTextBox
    {
        //https://stackoverflow.com/questions/3678620/c-sharp-richtextbox-selection-problem/3679036#3679036
        // this method is solved the .net bug for snapping text when highlighting by mouse 
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!base.AutoWordSelection)
            {
                base.AutoWordSelection = true;
                base.AutoWordSelection = false;
            }
        }
    }

}
