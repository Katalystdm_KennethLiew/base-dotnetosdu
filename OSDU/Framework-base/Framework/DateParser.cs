﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Framework
{
    public class DateParser
    {
        public struct DATE_ORDERS
        {
            public const String DAY_MONTH_YEAR = "Day-Month-Year";
            public const String YEAR_MONTH_DAY = "Year-Month-Day";
            public const String MONTH_DAY_YEAR = "Month-Day-Year";
            public const String MONTH_YEAR_DAY = "Month-Year-Day";
            public const String YEAR_DAY_MONTH = "Year-Day-Month";
            public const String DAY_YEAR_MONTH = "Day-Year-Month";
        }

        public Dictionary<String, DateTime> GetValidDateFormats(String dateString)
        {
            Dictionary<String, DateTime> validFormats = new Dictionary<String, DateTime>();
            Int32 year = -1;
            Int32 month = -1;
            Int32 yearPosition = -1;
            Int32 monthPosition = -1;
            Int32 ampmPosition = -1;
            Int32 timePosition = -1;
            Int32 newMonth = -1;
            Int32 newAmpmPosition = -1;
            Int32 newTimePosition = -1;

            //split out all date parts
            List<String> parts = dateString.Split(new char[] { ' ', ',', '-', '_', '/', '\\', '.', '|' }).ToList<String>();
            //not splitting out : as that will always refer to a time per handled below

            //remove all empty strings
            do { } while (parts.Remove(String.Empty));

            //Pick out alpha months & convert to digits
            //Any other alphas, remove.
            //Pick out 4-digit years (not greater than current year, not less than 1900)
            for (Int32 i = 0; i < parts.Count; i++)
            {
                String part = parts[i].Trim().ToLower();

                if (!String.IsNullOrEmpty(part))
                {
                    //if not numeric
                    if (!part.All(char.IsNumber))
                    {
                        newMonth = -1;
                        newAmpmPosition = -1;
                        newTimePosition = -1;

                        if (part.Equals("jan") || part.Equals("january"))
                            newMonth = 1;
                        else if (part.Equals("feb") || part.Equals("february"))
                            newMonth = 2;
                        else if (part.Equals("mar") || part.Equals("march"))
                            newMonth = 3;
                        else if (part.Equals("apr") || part.Equals("april"))
                            newMonth = 4;
                        else if (part.Equals("may"))
                            newMonth = 5;
                        else if (part.Equals("jun") || part.Equals("june"))
                            newMonth = 6;
                        else if (part.Equals("jul") || part.Equals("july"))
                            newMonth = 7;
                        else if (part.Equals("aug") || part.Equals("august"))
                            newMonth = 8;
                        else if (part.Equals("sep") || part.Equals("sept") || part.Equals("september"))
                            newMonth = 9;
                        else if (part.Equals("oct") || part.Equals("october"))
                            newMonth = 10;
                        else if (part.Equals("nov") || part.Equals("november"))
                            newMonth = 11;
                        else if (part.Equals("dec") || part.Equals("december"))
                            newMonth = 12;
                        else if (part.Equals("am") || part.Equals("pm"))
                            newAmpmPosition = i;
                        else if (part.Contains(":"))
                            newTimePosition = i;
                        else
                            //remove any other alpha parts
                            parts.RemoveAt(i);

                        //if found new month but was already found then continue as if none found
                        if (newMonth != -1 & month != -1)
                        {
                            month = -1;
                            monthPosition = -1;
                        }
                        //otherwise capture it
                        else if (newMonth != -1)
                        {
                            month = newMonth;
                            monthPosition = i;
                            //must update part to zero-padded digit to work with TryParseExact further below
                            parts[i] = month.ToString("00");
                        }

                        //if found new ampm but was already found then continue as if none found
                        if (newAmpmPosition != -1 & ampmPosition != -1)
                            ampmPosition = -1;
                        //otherwise capture it
                        else if (newAmpmPosition != -1)
                            ampmPosition = i;

                        //if found new time but was already found then continue as if none found
                        if (newTimePosition != -1 & timePosition != -1)
                            timePosition = -1;
                        //otherwise capture it
                        else if (newTimePosition != -1)
                            timePosition = i;

                    }
                    //is numeric 
                    else
                    {
                        //if found new year but was already found then continue as if none found
                        if (part.Length == 4 & yearPosition != -1)
                        {
                            year = -1;
                            yearPosition = -1;
                        }
                        else if (part.Length == 4)
                        {
                            //otherwise capture it if it validates
                            year = Int32.Parse(part);
                            if (year <= DateTime.Now.Year + 10 && year > 1900)//TODO: pass these in as params
                                yearPosition = i;
                            else
                            {
                                year = -1;
                                yearPosition = -1;
                            }
                        }
                    }
                }
            }

            //if having more than 3 parts plus any time positions
            if (parts.Count > 3 && (ampmPosition > -1 || timePosition > -1))
            {
                //if ampm position only and is at end and having 5 or more parts then remove all but first 3 parts
                //this meant to remove times not using : as a separator
                if ((ampmPosition > -1 & timePosition == -1) && (ampmPosition == parts.Count - 1 && parts.Count > 5))
                {
                    parts.RemoveRange(3, parts.Count - 3);
                }
                //else if have both ampm & time positions and are next to each other then remove them both
                else if (ampmPosition > -1 && timePosition > -1)
                {
                    if (ampmPosition + 1 == timePosition)
                        parts.RemoveRange(ampmPosition, 2);
                    if (ampmPosition - 1 == timePosition)
                        parts.RemoveRange(timePosition, 2);

                    //now we must recalc any month or year positions
                    if (monthPosition > ampmPosition)
                        monthPosition -= 2;
                    if (yearPosition > ampmPosition)
                        yearPosition -= 2;
                }
                //else if have time position then remove it
                else if (timePosition > -1)
                {
                    parts.RemoveAt(timePosition);

                    //now we must recalc any month or year positions
                    if (monthPosition > timePosition)
                        monthPosition -= 1;
                    if (yearPosition > timePosition)
                        yearPosition -= 1;
                }
            }

            //if having more than 3 parts but have both month and year
            if (parts.Count > 3 && monthPosition > -1 && yearPosition > -1)
            {
                String day = String.Empty;
                //if month & year next to each other then day must be next closest
                if (Math.Abs(monthPosition - yearPosition) == 1)
                {
                    if (monthPosition == parts.Count - 1)
                    {
                        day = parts[yearPosition - 1];
                        parts.Clear();
                        parts.Add(day);
                        parts.Add(year.ToString());
                        parts.Add(month.ToString("00"));
                        yearPosition = 1;
                        monthPosition = 2;
                    }
                    else if (yearPosition == parts.Count - 1)
                    {
                        day = parts[monthPosition - 1];
                        parts.Clear();
                        parts.Add(day);
                        parts.Add(month.ToString("00"));
                        parts.Add(year.ToString());
                        monthPosition = 1;
                        yearPosition = 2;
                    }
                    else if (monthPosition == 0)
                    {
                        day = parts[yearPosition + 1];
                        parts.Clear();
                        parts.Add(month.ToString("00"));
                        parts.Add(year.ToString());
                        parts.Add(day);
                        yearPosition = 1;
                        monthPosition = 0;
                    }
                    else if (yearPosition == 0)
                    {
                        day = parts[monthPosition + 1];
                        parts.Clear();
                        parts.Add(year.ToString());
                        parts.Add(month.ToString("00"));
                        parts.Add(day);
                        yearPosition = 0;
                        monthPosition = 1;
                    }
                    else if (monthPosition == 1 && yearPosition == 2)
                    {
                        day = parts[0];
                        parts.Clear();
                        parts.Add(day);
                        parts.Add(month.ToString("00"));
                        parts.Add(year.ToString());
                        monthPosition = 1;
                        yearPosition = 2;
                    }
                    else if (monthPosition == 2 && yearPosition == 1)
                    {
                        day = parts[0];
                        parts.Clear();
                        parts.Add(day);
                        parts.Add(year.ToString());
                        parts.Add(month.ToString("00"));
                        yearPosition = 1;
                        monthPosition = 2;
                    }
                    else if (monthPosition == parts.Count - 2 && yearPosition == parts.Count - 3)
                    {
                        day = parts[parts.Count - 1];
                        parts.Clear();
                        parts.Add(year.ToString());
                        parts.Add(month.ToString("00"));
                        parts.Add(day);
                        yearPosition = 0;
                        monthPosition = 1;
                    }
                    else if (yearPosition == parts.Count - 2 && monthPosition == parts.Count - 3)
                    {
                        day = parts[parts.Count - 1];
                        parts.Clear();
                        parts.Add(month.ToString("00"));
                        parts.Add(year.ToString());
                        parts.Add(day);
                        monthPosition = 0;
                        yearPosition = 1;
                    }
                }
                //if only one position between year & month then day must be that
                else if (Math.Abs(monthPosition - yearPosition) == 2)
                {
                    if (monthPosition > yearPosition)
                    {
                        day = parts[yearPosition + 1];
                        parts.Clear();
                        parts.Add(year.ToString());
                        parts.Add(day);
                        parts.Add(month.ToString("00"));
                        yearPosition = 0;
                        monthPosition = 2;
                    }
                    else if (yearPosition > monthPosition)
                    {
                        day = parts[monthPosition + 1];
                        parts.Clear();
                        parts.Add(month.ToString("00"));
                        parts.Add(day);
                        parts.Add(year.ToString());
                        monthPosition = 0;
                        yearPosition = 2;
                    }
                }

                //if any day was found then we have excluded any found found ampm or time
                if (day.Length > 0)
                {
                    ampmPosition = -1;
                    timePosition = -1;
                }

                //TODO: these cases are left here where blah or time or whatever is in the middle:
                //(may not be possible)
                //Aug 11 blah 2011
                //Aug 11 blah 2011
                //11 Aug blah 2011
                //11 Aug blah 2011
                //2011 blah Aug 11
                //2011 blah Aug 11
                //2011 blah 11 Aug
                //2011 blah 11 Aug
            }

            //if having 3 parts
            if (parts.Count == 3)
            {
                //add valid formats
                foreach (FieldInfo fi in typeof(DATE_ORDERS).GetFields())
                    if (DateTime.TryParseExact(GetDateFormatted(parts[0], parts[1], parts[2], fi.GetRawConstantValue().ToString()), GetTryParseDateFormat(fi.GetRawConstantValue().ToString(), GetYearPart(parts[0], parts[1], parts[2], fi.GetRawConstantValue().ToString())), CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result))
                        validFormats.Add(fi.GetRawConstantValue().ToString(), result);

                //filter out invalid formats based on found positions for both year & month
                //these remove without error even when don't exist
                if (monthPosition > -1 & yearPosition > -1)
                {
                    if (monthPosition < yearPosition)
                    {
                        validFormats.Remove(DATE_ORDERS.DAY_YEAR_MONTH);
                        validFormats.Remove(DATE_ORDERS.YEAR_DAY_MONTH);
                        validFormats.Remove(DATE_ORDERS.YEAR_MONTH_DAY);
                    }
                    else if (monthPosition > yearPosition)
                    {
                        validFormats.Remove(DATE_ORDERS.DAY_MONTH_YEAR);
                        validFormats.Remove(DATE_ORDERS.MONTH_DAY_YEAR);
                        validFormats.Remove(DATE_ORDERS.MONTH_YEAR_DAY);
                    }
                }

                //filter out invalid formats based on found month position by itself
                //these remove without error even when don't exist
                if (monthPosition > -1)
                {
                    if (monthPosition == 0)
                    {
                        validFormats.Remove(DATE_ORDERS.DAY_MONTH_YEAR);
                        validFormats.Remove(DATE_ORDERS.DAY_YEAR_MONTH);
                        validFormats.Remove(DATE_ORDERS.YEAR_DAY_MONTH);
                        validFormats.Remove(DATE_ORDERS.YEAR_MONTH_DAY);

                    }
                    else if (monthPosition == 1)
                    {
                        validFormats.Remove(DATE_ORDERS.DAY_YEAR_MONTH);
                        validFormats.Remove(DATE_ORDERS.MONTH_DAY_YEAR);
                        validFormats.Remove(DATE_ORDERS.MONTH_YEAR_DAY);
                        validFormats.Remove(DATE_ORDERS.YEAR_DAY_MONTH);
                    }
                    else if (monthPosition == 2)
                    {
                        validFormats.Remove(DATE_ORDERS.DAY_MONTH_YEAR);
                        validFormats.Remove(DATE_ORDERS.MONTH_DAY_YEAR);
                        validFormats.Remove(DATE_ORDERS.MONTH_YEAR_DAY);
                        validFormats.Remove(DATE_ORDERS.YEAR_MONTH_DAY);
                    }
                }
            }
            //otherwise, unable to extract parts, return as no valid formats found

            return validFormats;
        }

        private String GetDateFormatted(String part1, String part2, String part3, String dateOrder)
        {
            if (dateOrder.Equals(DATE_ORDERS.DAY_MONTH_YEAR))
                return GetMonthDayFormatted(part1) + "-" + GetMonthDayFormatted(part2) + "-" + GetYearFormatted(part3);
            else if (dateOrder.Equals(DATE_ORDERS.DAY_YEAR_MONTH))
                return GetMonthDayFormatted(part1) + "-" + GetYearFormatted(part2) + "-" + GetMonthDayFormatted(part3);
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_DAY_YEAR))
                return GetMonthDayFormatted(part1) + "-" + GetMonthDayFormatted(part2) + "-" + GetYearFormatted(part3);
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_YEAR_DAY))
                return GetMonthDayFormatted(part1) + "-" + GetYearFormatted(part2) + "-" + GetMonthDayFormatted(part3);
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_DAY_MONTH))
                return GetYearFormatted(part1) + "-" + GetMonthDayFormatted(part2) + "-" + GetMonthDayFormatted(part3);
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_MONTH_DAY))
                return GetYearFormatted(part1) + "-" + GetMonthDayFormatted(part2) + "-" + GetMonthDayFormatted(part3);
            else
                throw new NotSupportedException(dateOrder.ToString());
        }

        private String GetYearPart(String part1, String part2, String part3, String dateOrder)
        {
            if (dateOrder.Equals(DATE_ORDERS.DAY_MONTH_YEAR))
                return part3;
            else if (dateOrder.Equals(DATE_ORDERS.DAY_YEAR_MONTH))
                return part2;
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_DAY_YEAR))
                return part3;
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_YEAR_DAY))
                return part2;
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_DAY_MONTH))
                return part1;
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_MONTH_DAY))
                return part1;
            else
                throw new NotSupportedException(dateOrder.ToString());
        }

        private String GetYearFormatted(String year)
        {
            if (year.Length <= 2)
            {
                if (Int32.TryParse(year, out _))
                    return Int32.Parse(year).ToString("00");
                else
                    return year;
            }
            else if (year.Length == 3)
            {
                if (Int32.TryParse(year, out _))
                    return Int32.Parse(year).ToString("000");
                else
                    return year;
            }
            else
            {
                if (Int32.TryParse(year, out _))
                    return Int32.Parse(year).ToString("0000");
                else
                    return year;
            }
        }

        private String GetMonthDayFormatted(String monthDay)
        {
            if (Int32.TryParse(monthDay, out int iMonthDay))
                return iMonthDay.ToString("00");
            else
                return monthDay;
        }

        private String GetTryParseDateFormat(String dateOrder, String year)
        {
            //handle year
            String yearFormat;
            if (year.Length <= 2)
                yearFormat = "yy";
            else if (year.Length == 3)
                yearFormat = "yyy";
            else
                yearFormat = "yyyy";

            if (dateOrder.Equals(DATE_ORDERS.DAY_MONTH_YEAR))
                return "dd-MM-" + yearFormat;
            else if (dateOrder.Equals(DATE_ORDERS.DAY_YEAR_MONTH))
                return "dd-" + yearFormat + "-MM";
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_DAY_YEAR))
                return "MM-dd-" + yearFormat;
            else if (dateOrder.Equals(DATE_ORDERS.MONTH_YEAR_DAY))
                return "MM-" + yearFormat + "-dd";
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_DAY_MONTH))
                return yearFormat + "-dd-MM";
            else if (dateOrder.Equals(DATE_ORDERS.YEAR_MONTH_DAY))
                return yearFormat + "-MM-dd";
            else
                throw new NotSupportedException(dateOrder.ToString());
        }

    }
}
