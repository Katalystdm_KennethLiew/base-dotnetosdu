﻿using System;

namespace Framework
{
    public class DataSourceExceptionAffectedRecords : Exception
    {
        public DataSourceExceptionAffectedRecords()
        {
        }

        public DataSourceExceptionAffectedRecords(String message) : base(message)
        {
        }

        public DataSourceExceptionAffectedRecords(String message, Exception inner) : base(message, inner)
        {
        }
    }
}
