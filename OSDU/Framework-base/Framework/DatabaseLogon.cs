﻿using System;

namespace Framework
{
    public class DatabaseLogon
    {
        public String Host { get; set; } //get/set required for serialization
        public String ServiceName { get; set; }
        public String Port { get; set; }
        public String ID { get; set; }
        public String PWD { get; set; }
        public Boolean SavePWD { get; set; }

        public DatabaseLogon() { }

        public DatabaseLogon(String host, String serviceName, String port, String id, String pwd, Boolean savePWD)
        {
            Host = host;
            ServiceName = serviceName;
            Port = port;
            ID = id;
            PWD = pwd;
            SavePWD = savePWD;
        }

        public Boolean HasAllConnectionInfo()
        {
            if (!String.IsNullOrEmpty(Host) && !String.IsNullOrEmpty(ServiceName) && !String.IsNullOrEmpty(Port) && !String.IsNullOrEmpty(ID) && !String.IsNullOrEmpty(PWD))
                return true;
            else
                return false;
        }
    }

}
