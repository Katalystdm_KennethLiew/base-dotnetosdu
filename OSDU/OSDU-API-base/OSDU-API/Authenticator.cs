﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OSDU_API
{
    public class Authenticator : IAuthenticator
    {
        public String PoolID { get; set; }
        public String ClientID { get; set; }
        public String ClientSecret { get; set; }
        [JsonIgnore]
        public String UserID { get; set; }
        [JsonIgnore]
        public String UserPWD { get; set; }
        public String RefreshTokenURL { get; set; }
        public String RefreshContentType { get; set; }
        [JsonIgnore]
        public String RefreshToken { get; set; }

        //These expected to be retrieved by authentication calls once and re-used throughout the running session of the application.
        //Meaning the calling code should maintain one Authenticator object to be used for all api calls.
        //Bearer token will need to be refreshed 

        private String _bearerToken;
        private Int32 _attempts;
        private Boolean _hasConnection = false;

        public String ValidateCredentials(Boolean includingUser)
        {
            String ret = String.Empty;
            if (String.IsNullOrEmpty(PoolID))
                ret = GetMissingCredsMessage(ret, nameof(PoolID));
            if (String.IsNullOrEmpty(ClientID))
                ret = GetMissingCredsMessage(ret, nameof(ClientID));
            if (String.IsNullOrEmpty(ClientSecret))
                ret = GetMissingCredsMessage(ret, nameof(ClientSecret));
            if (includingUser && String.IsNullOrEmpty(UserID))
                ret = GetMissingCredsMessage(ret, nameof(UserID));
            if (includingUser && String.IsNullOrEmpty(UserPWD))
                ret = GetMissingCredsMessage(ret, nameof(UserPWD));
            if (String.IsNullOrEmpty(RefreshTokenURL))
                ret = GetMissingCredsMessage(ret, nameof(RefreshTokenURL));
            if (String.IsNullOrEmpty(RefreshContentType))
                ret = GetMissingCredsMessage(ret, nameof(RefreshContentType));
            return ret;
        }

        private String GetMissingCredsMessage(String currentMsg, String nameOf)
        {
            if (!String.IsNullOrEmpty(currentMsg))
                currentMsg += "\r\n";
            currentMsg += "Missing " + nameOf + ".";
            return currentMsg;
        }

        public RegionEndpoint GetRegionEndPoint()
        {
            RegionEndpoint endPoint = null;
            //get enumerable end point from pool id
            foreach (RegionEndpoint ep in RegionEndpoint.EnumerableAllRegions)
            {
                if (PoolID.StartsWith(ep.SystemName))
                {
                    endPoint = ep;
                    break;
                }
            }
            return endPoint;
        }

        public void Authenticate(IRestClient client, IRestRequest request )
        {

            String missingCreds = ValidateCredentials(true);
            if (missingCreds.Length > 0)
            {
                throw new Exception(missingCreds);
            }
            else
            {
                //no refresh token yet, get it
                //refresh token normally expires @ 30 days so should be good for the runtime of the app
                if (String.IsNullOrEmpty(RefreshToken))
                {
                    AdminInitiateAuthRequest authReq = new AdminInitiateAuthRequest
                    {
                        UserPoolId = PoolID,
                        ClientId = ClientID,
                        AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH
                    };

                    //The logic for computing this hash comes from Amazon at: https://docs.aws.amazon.com/cognito/latest/developerguide/signing-up-users-in-your-app.html#cognito-user-pools-computing-secret-hash
                    //That is java code which has very different method calls than the respective C# code below.
                    //Basically the java .update() method converts to C# .TransformBlock()
                    //And the java .doFinal() method converts to C# .ComputeHash()
                    String secretHash;
                    using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(ClientSecret)))
                    {
                        hmac.Initialize();
                        Byte[] rawHmac = Encoding.UTF8.GetBytes(UserID);
                        hmac.TransformBlock(rawHmac, 0, rawHmac.Length, null, 0);
                        secretHash = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(ClientID)));
                    }
                    authReq.AuthParameters.Add("SECRET_HASH", secretHash);
                    authReq.AuthParameters.Add("USERNAME", UserID);
                    authReq.AuthParameters.Add("PASSWORD", UserPWD);

                    RegionEndpoint endPoint = GetRegionEndPoint();
                    if (endPoint is null)
                        throw new Exception("Could not obtain RegionEndPoint using PoolID = [" + PoolID + "].");
                    else
                    {
                       
                        //using (AmazonCognitoIdentityProviderClient cognitoClient = new AmazonCognitoIdentityProviderClient(AccessKey, AccessSecret, endPoint))
                        //{
                        //    AdminInitiateAuthResponse authResp = cognitoClient.AdminInitiateAuth(authReq);
                        //    _refreshToken = authResp.AuthenticationResult.RefreshToken;
                        //}
                    }
                }

                //if unable to obtain a refresh token then error out
                if (String.IsNullOrEmpty(RefreshToken))
                    throw new Exception("Unable to obtain refresh token.");
                //otherwise continue to check for bearer token
                else
                {
                    //no bearer token yet, get it
                    //bearer token normally expires @ 3600 secs
                    if (String.IsNullOrEmpty(_bearerToken))
                    {
                        RestClient clientRefresh;
                        RestRequest requestRefresh;
                        IRestResponse response;

                        //get bearer token using the refresh token
                        clientRefresh = new RestClient(RefreshTokenURL);
                        clientRefresh.Timeout = -1;
                        requestRefresh = new RestRequest(Method.POST);
                        requestRefresh.AddHeader("Content-Type", RefreshContentType);
                        requestRefresh.AddParameter("grant_type", "refresh_token");
                        requestRefresh.AddParameter("refresh_token", RefreshToken);
                        requestRefresh.AddParameter("client_id", ClientID);
                        requestRefresh.AddParameter("Client_secret", ClientSecret);
                        _attempts += 1;
                        response = clientRefresh.Execute(requestRefresh);

                        //The refresh token may expire (normally after 30 days).
                        //This should be good for the lifetime of a running session of the app but in case not:
                        //Clear both the refresh & bearer tokens and re-call this method to get new ones from scratch.
                        if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        {
                            //only attempt twice if getting access denied
                            if (_attempts > 2)
                                throw new Exception("Unable to obtain bearer token after 2 attempts.");
                            else
                            {
                                RefreshToken = String.Empty;
                                _bearerToken = String.Empty;
                                Authenticate(client, request);
                            }
                        }
                        else
                        {
                            _attempts = 0;
                            //bearer token is the "access_token" tag of the returned json
                            using (JsonDocument document = JsonDocument.Parse(response.Content))
                            {
                                if (document.RootElement.TryGetProperty("access_token", out JsonElement element))
                                {
                                    if (element.ValueKind is JsonValueKind.String)
                                        _bearerToken = element.GetString();
                                }
                            }
                        }
                    }

                    //if still no bearer token then error out
                    if (String.IsNullOrEmpty(_bearerToken))
                        throw new Exception("Unable to obtain bearer token.");
                    else
                        //otherwise, ensure the request has the bearer token set
                        request.AddHeader("Authorization", "Bearer " + _bearerToken);
                }
            }
                
            
        }

        public void ClearBearerToken()
        {
            _bearerToken = String.Empty;
        }

        public String SignInAndVerifyDataSourceParameters(DataSourceParameters dsParams)
        {
            String ret = dsParams.ValidateParameters();
            if (ret.Length == 0)
            {
                RestRequest request;
                request = new RestRequest(Method.GET);
                request.AddHeader("data-partition-id", dsParams.DataPartitionID);

                RestClient client = new RestClient(dsParams.StorageURL + "/api/storage/v2/query/kinds");
                client.Authenticator = this;
                client.Timeout = -1;
                request.RequestFormat = DataFormat.Json;
                IRestResponse response = null;
                try
                {
                    response = client.Execute(request);
                }
                catch (Exception ex)
                {
                    //bad creds will catch error here
                    ret = ex.Message;
                }
                if (ret.Length > 0)
                {
                    _hasConnection = false;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    _hasConnection = false;
                    ret = response.Content;
                }
                else
                    _hasConnection = true;
            }
            return ret;
        }

        public Boolean HasConnection()
        {
            return _hasConnection;
        }

        public void ResetConnection()
        {
            UserPWD = String.Empty;
            RefreshToken = String.Empty;
            _bearerToken = String.Empty;
            _hasConnection = false;
        }

    }
}
