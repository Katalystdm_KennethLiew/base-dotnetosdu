﻿using System;

namespace OSDU_API.Objects
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class WellLogWorkProduct
    {
        public WellLogWorkProductResourceData data { get; set; } = new WellLogWorkProductResourceData();
        public String kind { get; private set; }
        public StorageACL acl { get; private set; } = new StorageACL();
        public StorageLegal legal { get; private set; } = new StorageLegal();

        private readonly DataSourceParameters _parameters;

        public WellLogWorkProduct(DataSourceParameters dsParams)
        {
            _parameters = dsParams;
            kind = _parameters.DataPartitionID + ":osdu:welllog-wp:" + _parameters.SchemaVersion;
        }
    }

    public class WellLogWorkProductResourceData
    {
        public String ResourceID { get; private set; } = "srn:work-product/WellLog:null:";
        public String ResourceTypeID { get; private set; } = "srn:type:work-product/WellLog:";
        public String ResourceSecurityClassification { get; private set; } = "srn:reference-data/ResourceSecurityClassification:RESTRICTED:";
        public WellLogWorkProductData Data { get; set; } = new WellLogWorkProductData();
    }

    public class WellLogWorkProductData
    {
        public WellLogWorkProductGroupTypeProperties GroupTypeProperties { get; set; } = new WellLogWorkProductGroupTypeProperties();
    }

    public class WellLogWorkProductGroupTypeProperties
    {
        public String[] Components { get; set; }
    }

}
