﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU_API.Objects
{


    public class ModelWellboreData
    {
        public String FacilityID { get; set; }
        public String FacilityName { get; set; }
        public String UWBI{ get; set; }
        public String Country { get; set; }//lookup GeoPoliticalEntityID
        public String TrajectoryTypeID { get; set; } //Lookup WellboreTrajectoryType
        public String PrimaryMaterialID{ get; set; }//Lookup  MaterialType
        public String Status { get; set; }//Lookup FacilityStateType ,special Handle
        public DateTime? StartDrillingDate { get; set; }// special Handle
        public DateTime? FinishDrillingDate { get; set; }// special handle
        public String InitialOperatorID { get; set; }
        public String CurrentOperatorID { get; set; }
        public String WellID { get; set; }
        public Int32? SequenceNumber { get; set; }
        public String InterestTypeID { get; set; }
        public Double? TD_VerticalMeasurement { get; set; }
        public String TD_VerticalMeasurementUnitOfMeasureID { get; set; }
        public Double? RT_VerticalMeasurement { get; set; }
        public String RT_VerticalMeasurementUnitOfMeasureID { get; set; }
    }

}
