﻿using System;
using System.Collections.Generic;

namespace OSDU_API.Objects
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class StorageACL 
    {
        public String[] viewers { get; set; } = { "data.default.viewers@osdu.testing.com" };
        public String[] owners { get; set; } = { "data.default.owners@osdu.testing.com" };
    }
}
