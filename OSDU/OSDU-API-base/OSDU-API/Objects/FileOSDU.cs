﻿using System;

namespace OSDU_API.Objects
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class FileOSDU
    {
        public FileResourceData data { get; set; } = new FileResourceData();
        public String kind { get; private set; }
        public StorageACL acl { get;  set; } = new StorageACL();
        public StorageLegal legal { get; private set; } = new StorageLegal();

        private readonly DataSourceParameters _parameters;

        public FileOSDU(DataSourceParameters dsParams, String digitalFormat)
        {
            _parameters = dsParams;
            kind = _parameters.DataPartitionID + ":osdu:file:" + _parameters.SchemaVersion;
            data.ResourceTypeID += digitalFormat + ":";
            if(_parameters.DataPartitionID.ToUpper() != "OSDU")
            {
                acl.viewers = new String[] {"data.default.viewers@" + _parameters.DataPartitionID + ".testing.com"};
                acl.owners = new String[] {"data.default.owners@"+ _parameters.DataPartitionID + ".testing.com" };
            }
        }
    }

    public class FileResourceData
    {
        public String ResourceID { get; private set; } = "srn:file/las:null:";
        public String ResourceTypeID { get; internal set; } = "srn:type:file/";
        public String ResourceSecurityClassification { get; private set; } = "srn:reference-data/ResourceSecurityClassification:RESTRICTED:";
        public FileData Data { get; set; } = new FileData();
    }

    public class FileData
    {
        public FileGroupTypeProperties GroupTypeProperties { get; set; } = new FileGroupTypeProperties();
    }

    public class FileGroupTypeProperties
    {
        public String PreLoadFilePath { get; set; }
        public String FileSource { get; set; }
        public Int64 FileSize { get; set; }
    }

}
