﻿using System;
using System.Collections.Generic;

namespace OSDU_API
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class SearchQuery
    {
        public String kind { get; set; } = String.Empty;
        public String query { get; set; } = String.Empty;
        public List<String> returnedFields { get; set; } = new List<String>();
        public Int32 limit { get; set; }
    }
}
