﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OSDU_API.Objects
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class StoragePayload
    {
        public String AppKey { get; set; } = "test-app";

        [JsonPropertyName("data-partition-id")]
        public String data_partition_id { get; set; } = "osdu";

    }
}
