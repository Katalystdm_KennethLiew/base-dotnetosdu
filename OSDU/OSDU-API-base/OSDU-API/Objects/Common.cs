﻿using System;

namespace OSDU_API.Objects
{
    public static class Common
    {
        public struct URLs
        {
            public const String QueryURL = "/api/search/v2/query";
            public const String StorageURL = "/api/storage/v2/records";
            public const String ManifestURL = "/api/workflow/v1/workflow/Osdu_ingest/workflowRun";
        }

        public struct JsonTags
        {
            public const String WellBoreKind = ":osdu:wellbore-master:";
            public const String OrganizationKind = ":osdu:organisationdc-master:";
            public const String DataPartition = "data-partition-id";
            public const String DataContainer = "data";
            public const String ID = "id";
            public const String QueryResultsContainer = "results";
            public const String StorageRecordIDs = "recordIds";
            public const String ManifestRecordIDs = "runId";
            public const String GeoPoliticalEntity_Country = ":reference-data--GeoPoliticalEntityType:Country:";
            public const String NameAliasEntity_UWBI = ":reference-data--AliasNameType:UWBI:";
            public const String FacilityEventTypeEntity_ProductionStart = ":reference-data--FacilityEventType:ProductionStart:";
            public const String FacilityEventTypeEntity_ProductionFinish = ":reference-data--FacilityEventType:ProductionFinish:";
            public const String CoordinateReferenceSystem_VerticalCRS_EPSG_5714 = ":reference-data--CoordinateReferenceSystem:VerticalCRS::EPSG::5714";

            public const String OrganisationKind = "osdu:wks:master-data--Organisation:1.0.0";
            public const String OperatingEnvironmentKind = "osdu:wks:reference-data--OperatingEnvironment:1.0.0";
            public const String FacilityEventTypeKind = "osdu:wks:reference-data--FacilityEventType:1.0.0";
            public const String WellInterestTypeKind = "osdu:wks:reference-data--WellInterestType:1.0.0";
            public const String UnitOfMeasureKind = "osdu:wks:reference-data--UnitOfMeasure:1.0.0";
            public const String VerticalMeasurementPathKind = "osdu:wks:reference-data--VerticalMeasurementPath:1.0.0";
            public const String VerticalMeasurementTypeKind = "osdu:wks:reference-data--VerticalMeasurementType:1.0.0";
            public const String VerticalMeasuremenSourceKind = "osdu:wks:reference-data--VerticalMeasurementSource:1.0.0";
            public const String GeoPoliticalEntityKind = "osdu:wks:master-data--GeoPoliticalEntity:1.0.0";
            public const String WellboreTrajectoryTypeKind = "osdu:wks:reference-data--WellboreTrajectoryType:1.0.0";
            public const String MaterialTypeKind = "osdu:wks:reference-data--MaterialType:1.0.0";
            public const String WellKind = "osdu:wks:master-data--Well:1.0.0";
            public const String WellboreKind = "osdu:wks:master-data--Wellbore:1.0.0";
            public const String FacilityStateTypeKind = "osdu:wks:reference-data--FacilityStateType:1.0.0";
            public const String CoordinateReferenceSystemKind = "osdu:wks:reference-data--CoordinateReferenceSystem:1.0.0";

            public const String LookUpMasterGeoPoliticalEntity = ":master-data--GeoPoliticalEntity:";
            public const String LookUpMasterWell = ":master-data--Well:";
            public const String LookUpMasterOrganisation = ":master-data--Organisation:";
            public const String LookUpOperatingEnvironment = ":reference-data--OperatingEnvironment:";
            public const String LookUpWellInterestType = ":reference-data--WellInterestType:";
            public const String LookUpFacilityEventType = ":reference-data--FacilityEventType:";
            public const String LookUpUnitOfMeasure = ":reference-data--UnitOfMeasure:";
            public const String LookUpVerticalMeasurementPath = ":reference-data--VerticalMeasurementPath:";
            public const String LookUpVerticalMeasurementType = ":reference-data--VerticalMeasurementType:";
            public const String LookUpVerticalMeasuremenSource = ":reference-data--VerticalMeasurementSource:";
            public const String LookUpWellboreTrajectoryType = ":reference-data--WellboreTrajectoryType:";
            public const String LookUpMaterialType = ":reference-data--MaterialType:";
            public const String LookUpFacilityStateType = ":reference-data--FacilityStateType:";
            public const String LookUpCoordinateReferenceSystem = ":reference-data--CoordinateReferenceSystem:";
        }
    }
}
