﻿using System;
using System.Text.Json.Serialization;

namespace OSDU_API.Objects
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class WellBoreSearchResults
    {
        public WellBoreSearchResult[] results { get; set; }
        public Int32 totalCount { get; set; }
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class WellBoreSearchResult
    {
        public String id { get; set; }
        public WellBoreSearchResultsData data { get; set; }
    }

    public class WellBoreSearchResultsData
    {
        public struct JsonPropertyNames
        {
            public const String DataIndividualTypePropertiesFacilityName = "Data.IndividualTypeProperties.FacilityName";
        }

        [JsonPropertyNameAttribute(JsonPropertyNames.DataIndividualTypePropertiesFacilityName)]
        public String DataIndividualTypePropertiesFacilityName { get; set; }
        public String UWI { get; set; }
        public String UWBI { get; set; }
    }

}
