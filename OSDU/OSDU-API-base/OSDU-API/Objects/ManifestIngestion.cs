﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OSDU_API.Objects
{   //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class ManifestIngestion
    {
        public String runId { get; set; }
        public ExecutionContext executionContext { get; set; } = new ExecutionContext();

        private readonly DataSourceParameters _parameters;

        public ManifestIngestion(DataSourceParameters dsParams)
        {
            _parameters = dsParams;
            if (_parameters.DataPartitionID.ToUpper() != "OSDU")
            {
                executionContext.acl.viewers = new String[] { "data.default.viewers@" + _parameters.DataPartitionID + ".testing.com" };
                executionContext.acl.owners = new String[] { "data.default.owners@" + _parameters.DataPartitionID + ".testing.com" };
                executionContext.legal.legaltags = new String[] { _parameters.DataPartitionID + "-public-usa-dataset-1" };
                executionContext.Payload.data_partition_id = _parameters.DataPartitionID;
            }
        }

    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class ExecutionContext
    {
        public StorageACL acl { get; private set; } = new StorageACL();
        public StorageLegal legal { get; private set; } = new StorageLegal();
        public StoragePayload Payload { get; private set; } = new StoragePayload();
        public StorageManifest manifest { get; set; } = new StorageManifest();
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class StorageManifest
    {
        public String kind { get; set; } = "osdu:wks:Manifest:1.0.0";
        public List<MasterData> MasterData { get; set; } = new List<MasterData>();
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class MasterData
    {
        public String id { get; set; }
        public String kind { get; set; }
        public StorageACL acl { get; private set; } = new StorageACL();
        public StorageLegal legal { get; private set; } = new StorageLegal();
        public Object data { get; set; } = new Object();
    }

    public class WellData
    {
        public String FacilityID { get; set; }
        public String FacilityName { get; set; }
        public String InitialOperatorID { get; set; }
        public String CurrentOperatorID { get; set; }
        public String DataSourceOrganisationID { get; set; }
        public String OperatingEnvironmentID { get; set; }
        public String InterestTypeID { get; set; }
        public List<FacilityEvent> FacilityEvents { get; set; } 
        public List<VerticalMeasurements> VerticalMeasurements { get; set; } 
        //public List<GeoContexts> GeoContexts { get; set; } = new List<GeoContexts>();

    }

    public class WellboreData
    {
        public String FacilityID { get; set; }
        public String FacilityName { get; set; }
        public String InitialOperatorID { get; set; }
        public String CurrentOperatorID { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String OperatingEnvironmentID { get; set; }
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String InterestTypeID { get; set; }
        public String TrajectoryTypeID { get; set; }
        public String PrimaryMaterialID { get; set; }
        public String WellID { get; set; }
        public Int32? SequenceNumber { get; set; }
        public List<FacilityEvent> FacilityEvents { get; set; }
        public List<FacilityState> FacilityStates { get; set; } 
        public List<VerticalMeasurements> VerticalMeasurements { get; set; } 
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<GeoContexts> GeoContexts { get; set; } = null;
        //[JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        //public List<NameAlias> NameAliases { get; set; } = new List<NameAlias>();

    }

    public class FacilityEvent
    {
        public String FacilityEventTypeID { get; set; }
        public DateTime? EffectiveDateTime { get; set; }
    }

    public class VerticalMeasurements
    {
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementID { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementDescription { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public Double VerticalMeasurement { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementUnitOfMeasureID { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementPathID { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementTypeID { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalMeasurementSourceID { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public String VerticalCRSID { get; set; }
    }

    public class GeoContexts
    {
        public String GeoPoliticalEntityID { get; set; }
        public String GeoTypeID { get; set; }
       
    }

    ///we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class Geometry
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class Feature
    {
        public string type { get; set; }
        public Geometry geometry { get; set; }
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class AsIngestedCoordinates
    {
        public string type { get; set; }
        public string CoordinateReferenceSystemID { get; set; }
        public List<Feature> features { get; set; }
        public string persistableReferenceCrs { get; set; }
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class Wgs84Coordinates
    {
        public string type { get; set; }
        public List<Feature> features { get; set; }
    }

    public class SpatialLocation
    {
        public AsIngestedCoordinates AsIngestedCoordinates { get; set; }
        public Wgs84Coordinates Wgs84Coordinates { get; set; }
    }

    public class ProjectedBottomHoleLocation
    {
        public AsIngestedCoordinates AsIngestedCoordinates { get; set; }
    }

    public class NameAlias
    {
        public String AliasName { get; set; }
        public String AliasNameTypeID { get; set; }
    }
    public class FacilityState
    {
        public String FacilityStateTypeID { get; set; }
    }
}
