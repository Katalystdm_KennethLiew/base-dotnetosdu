﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU_API.Objects
{


    public class ModelWellData
    {
        public String FacilityID { get; set; }
        public String FacilityName { get; set; }
        public String InitialOperatorID { get; set; }
        public String CurrentOperatorID { get; set; }
        public String DataSourceOrganisationID { get; set; }
        public String OperatingEnvironmentID { get; set; }
        public String FacilityEventTypeID1 { get; set; }
        public DateTime? EffectiveDateTime1 { get; set; }
        public String FacilityEventTypeID2 { get; set; }
        public DateTime? EffectiveDateTime2 { get; set; }
        public String InterestTypeID { get; set; }
        public String VerticalMeasurementID1 { get; set; }
        public String VerticalMeasurementDescription1 { get; set; }
        public Double? VerticalMeasurement1 { get; set; }
        public String VerticalMeasurementUnitOfMeasureID1 { get; set; }
        public String VerticalMeasurementPathID1 { get; set; }
        public String VerticalMeasurementTypeID1 { get; set; }
        public String VerticalMeasurementSourceID1 { get; set; }
        public String VerticalMeasurementID2 { get; set; }
        public String VerticalMeasurementDescription2 { get; set; }
        public Double? VerticalMeasurement2 { get; set; }
        public String VerticalMeasurementUnitOfMeasureID2 { get; set; }
        public String VerticalMeasurementPathID2 { get; set; }
        public String VerticalMeasurementTypeID2 { get; set; }
        public String VerticalMeasurementSourceID2 { get; set; }
    }

}
