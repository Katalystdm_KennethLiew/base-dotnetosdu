﻿using System;

namespace OSDU_API.Objects
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class WellLogWorkProductComponent
    {
        public WellLogWorkProductComponentResourceData data { get; set; } = new WellLogWorkProductComponentResourceData();
        public String kind { get; private set; }
        public StorageACL acl { get; private set; } = new StorageACL();
        public StorageLegal legal { get; private set; } = new StorageLegal();

        private readonly DataSourceParameters _parameters;

        public WellLogWorkProductComponent(DataSourceParameters dsParams)
        {
            _parameters = dsParams;
            kind = _parameters.DataPartitionID + ":osdu:welllog-wpc:" + _parameters.SchemaVersion;
        }
    }

    public class WellLogWorkProductComponentResourceData
    {
        public String ResourceID { get; private set; } = "srn:work-product-component/WellLog:null:";
        public String ResourceTypeID { get; private set; } = "srn:type:work-product-component/WellLog:";
        public String ResourceSecurityClassification { get; private set; } = "srn:reference-data/ResourceSecurityClassification:RESTRICTED:";
        public WellLogWorkProductComponentData Data { get; set; } = new WellLogWorkProductComponentData();
    }

    public class WellLogWorkProductComponentData
    {
        public WellLogWorkProductComponentFiles GroupTypeProperties { get; set; } = new WellLogWorkProductComponentFiles();
        public WellLogWorkProductComponentIndividualProperties IndividualTypeProperties { get; set; } = new WellLogWorkProductComponentIndividualProperties();
    }

    public class WellLogWorkProductComponentFiles
    {
        public String[] Files { get; set; }
    }

    public class WellLogWorkProductComponentIndividualProperties
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String WellboreID { get; set; }
        public String ServiceCompanyID { get; set; }
        public DateTime CreationDateTime { get; set; }
        public MeasuredDepth TopMeasuredDepth { get; set; } = new MeasuredDepth();
        public MeasuredDepth BottomMeasuredDepth { get; set; } = new MeasuredDepth();
        public Curve[] Curves { get; set; }
        public String[] Tags { get; set; }
    }

    public class MeasuredDepth
    {
        public Double Depth { get; set; }
        public String UnitOfMeasure { get; set; }
    }

    public class Curve
    {
        public String Mnemonic { get; set; }
        public Double TopDepth { get; set; }
        public Double BaseDepth { get; set; }
        public String DepthUnit { get; set; }
        public String CurveUnit { get; set; }
    }

}
