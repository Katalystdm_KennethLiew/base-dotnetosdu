﻿using System;

namespace OSDU_API.Objects
{
    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class OrganizationSearchResults
    {
        public OrganizationSearchResult[] results { get; set; }
        public Int32 totalCount { get; set; }
    }

    //we require camel casing on these fields for osdu api
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class OrganizationSearchResult
    {
        public String id { get; set; }
        public OrganizationSearchResultsData data { get; set; }
    }

    public class OrganizationSearchResultsData
    {
        public String OrgName { get; set; }
    }

}
