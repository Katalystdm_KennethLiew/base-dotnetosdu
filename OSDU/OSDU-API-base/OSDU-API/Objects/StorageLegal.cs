﻿using System;

namespace OSDU_API.Objects
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006")]
    public class StorageLegal
    {
        public String[] legaltags { get; set; } = { "osdu-public-usa-dataset-1" };
        public String[] otherRelevantDataCountries { get; set; } = { "US" };
    }
}
