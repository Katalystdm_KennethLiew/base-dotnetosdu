﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSDU_API.Objects
{
    public class ModelLog
    {
        public String Log { set; get; } = String.Empty;


        public ModelLog()
        {
            Log = String.Empty;
        }

        public static class Method
        {
            public const String Login = "Login";
            public const String LogOut = "Logout";
            public const String ConfigureConnection = "Configure Connection";
            public const String GetLookupData = "Get Lookup Data";
            public const String GetLookupOrganization = "Get Lookup Organization";
            public const String GetLookupOperatingEnvironment = "Get Lookup Operating Environment";
            public const String GetLookupFacilityEventType = "Get Lookup Facility Event Type ";
            public const String GetLookupWellInterestType = "Get Lookup Well Interest Type";
            public const String GetLookupUnitOfMeasure = "Get Lookup Unit Of Measure";
            public const String GetLookupVerticalMeasurementPath = "Get Lookup Vertical Measurement Path";
            public const String GetLookupVerticalMeasurementType = "Get Lookup Vertical Measurement Type";
            public const String GetLookupVerticalMeasuremenSource = "Get Lookup Vertical Measuremen Source";
            public const String GetLookupGeoPoliticalEntity = "Get Lookup Geo Political Entity";
            public const String GetLookupWellboreTrajectoryType = "Get Lookup Wellbore Trajectory Type";
            public const String GetLookupMaterialType = "Get Lookup Material Type";
            public const String GetLookupWell = "Get Lookup Well";
            public const String GetLookupFacilityStateType = "Get Lookup Facility State Type";
            public const String GetLookupCoordinateReferenceSystem = "Get Lookup Coordinate Reference System";
            public const String ExportExcelTemplate = "Export Excel Template";
            public const String ImportExcelSheetDataToApplication = "Import Excel Sheet Data To Application";
            public const String ConvertDataToJsonFormat = "Convert Data To Json Format";
            public const String DataLoadToOSDUPlatform = "Data Load to OSDU Platform";
        }

        public static class Status
        {
            public const String InProgress = "In-Progress";
            public const String Success = "Success";
            public const String Failed = "Failed";
            public const String Request = "Request";
            public const String Response = "Response";
        }

        public static class Notification
        {
            public const String Info = "INFO";
            public const String Warning = "WARNING";
            public const String Error = "ERROR";
        }

        public class URLMethod
        {
            public const String Post = "POST";
            public const String Get = "GET";
            public const String Put = "PUT";
        }

        public void SetLog(String notification,String method,String status, String remark, String uRLMethod ="" ,String uRL="")
        {
            if(!String.IsNullOrEmpty(uRLMethod))
                Log += DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd  HH':'mm':'ss'.'fff ") + " " + notification + " [" + method + " - " + status + "]\r\n["+ uRLMethod + " - " + uRL +"] : \r\n " + remark + "\r\n";
            else if (String.IsNullOrEmpty(remark))
                Log += DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd  HH':'mm':'ss'.'fff ") + " " + notification + " [" + method + " - " + status + "]\r\n";
            else
                Log += DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd  HH':'mm':'ss'.'fff ") + " " + notification + " [" + method + " - " + status + "] : " + remark + "\r\n";
        }
    }
}
