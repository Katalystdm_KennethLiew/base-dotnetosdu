﻿using System;

namespace OSDU_API
{
    public class DataSourceParameters
    {
        public String DataPartitionID { get; set; }
        public String SchemaVersion { get; set; }
        public String SearchURL { get; set; }
        public String StorageURL { get; set; }

        public String ValidateParameters()
        {
            String ret = String.Empty;
            if (String.IsNullOrEmpty(SearchURL))
                ret = GetMissingParamsMessage(ret, nameof(SearchURL));
            if (String.IsNullOrEmpty(DataPartitionID))
                ret = GetMissingParamsMessage(ret, nameof(DataPartitionID));
            if (String.IsNullOrEmpty(SchemaVersion))
                ret = GetMissingParamsMessage(ret, nameof(SchemaVersion));
            return ret;
        }

        private String GetMissingParamsMessage(String currentMsg, String nameOf)
        {
            if (!String.IsNullOrEmpty(currentMsg))
                currentMsg += "\r\n";
            currentMsg += "Missing " + nameOf + ".";
            return currentMsg;
        }

    }
}
