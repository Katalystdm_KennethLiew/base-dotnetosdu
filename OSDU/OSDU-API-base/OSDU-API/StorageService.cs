﻿using FrameworkExcel;
using OSDU_API.Objects;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OSDU_API
{
    public class StorageService
    {
        public HttpStatusCode ResponseStatusCode { get; private set; }
        public String ResponseContent { get; private set; }

        private readonly Authenticator _auth;
        private readonly DataSourceParameters _parameters;
        private readonly RestClient _client;

        public StorageService(Authenticator auth, DataSourceParameters parameters)
        {
            _auth = auth;
            _parameters = parameters;
            _client = new RestClient(_parameters.StorageURL + Common.URLs.ManifestURL);
            _client.Authenticator = _auth;
            _client.Timeout = -1;
        }

        public void Store(Object osduObject, out String id)
        {
            id = String.Empty;

            RestRequest request;
            request = new RestRequest(Method.PUT);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            request.RequestFormat = DataFormat.Json;
            Object[] objs = { osduObject };
            request.AddJsonBody(objs);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                _auth.ClearBearerToken();
                Store(osduObject, out id);
            }
            //otherwise, good response then parse out the returned id value
            //AWS returns Created
            else if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK)
            {
                using (JsonDocument document = JsonDocument.Parse(response.Content))
                {
                    if (document.RootElement.TryGetProperty(Common.JsonTags.StorageRecordIDs, out JsonElement result))
                    {
                        JsonElement.ArrayEnumerator results = result.EnumerateArray();
                        while (results.MoveNext())
                        {
                            id = results.Current.GetString();
                            break;
                        }
                    }
                }
            }
        }


        public void Store_Manifest(List<ModelWellData> wellDatas, List<ModelWellboreData> wellboreDatas, out String id, ref ModelLog log)
        {
            id = String.Empty;
            RestRequest request;
            request = new RestRequest(Method.POST);
            request.AddHeader(Common.JsonTags.DataPartition, _parameters.DataPartitionID);

            request.RequestFormat = DataFormat.Json;

            ManifestIngestion manifestIngestion = new ManifestIngestion(_parameters);
            manifestIngestion.runId = Guid.NewGuid().ToString();
            manifestIngestion.executionContext.manifest.MasterData = new List<MasterData>();
            if (wellDatas.Count > 0)
                foreach (ModelWellData wd in wellDatas)
                {
                    MasterData masterData = new MasterData();
                    masterData.id = _parameters.DataPartitionID + ":master-data--Well:" + wd.FacilityID + "";
                    masterData.kind = Common.JsonTags.WellKind;

                    if (_parameters.DataPartitionID.ToUpper() != "OSDU")
                    {
                        masterData.acl.viewers = new String[] { "data.default.viewers@" + _parameters.DataPartitionID + ".testing.com" };
                        masterData.acl.owners = new String[] { "data.default.owners@" + _parameters.DataPartitionID + ".testing.com" };
                    }

                    WellData wellData = new WellData();
                    wellData.FacilityName = wd.FacilityName;
                    wellData.FacilityID = wd.FacilityID;
                    if (!String.IsNullOrEmpty(wd.InitialOperatorID)) wellData.InitialOperatorID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterOrganisation + GetLookupID(wd.InitialOperatorID);
                    if (!String.IsNullOrEmpty(wd.CurrentOperatorID)) wellData.CurrentOperatorID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterOrganisation + GetLookupID(wd.CurrentOperatorID);
                    if (!String.IsNullOrEmpty(wd.DataSourceOrganisationID)) wellData.DataSourceOrganisationID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterOrganisation + GetLookupID(wd.DataSourceOrganisationID);
                    if (!String.IsNullOrEmpty(wd.OperatingEnvironmentID)) wellData.OperatingEnvironmentID = _parameters.DataPartitionID + Common.JsonTags.LookUpOperatingEnvironment + GetLookupID(wd.OperatingEnvironmentID);
                    if (!String.IsNullOrEmpty(wd.InterestTypeID)) wellData.InterestTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpWellInterestType + GetLookupID(wd.InterestTypeID);

                    //FacilityEvent
                    if (!String.IsNullOrEmpty(wd.FacilityEventTypeID1) || wd.EffectiveDateTime1 != null)
                    {
                        FacilityEvent facilityEvent = new FacilityEvent();
                        if (!String.IsNullOrEmpty(wd.FacilityEventTypeID1)) facilityEvent.FacilityEventTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpFacilityEventType + GetLookupID(wd.FacilityEventTypeID1);
                        if (wd.EffectiveDateTime1 != null)facilityEvent.EffectiveDateTime = ((DateTime)(wd.EffectiveDateTime1)).ToUniversalTime();

                        if (wellData.FacilityEvents == null)
                            wellData.FacilityEvents = new List<FacilityEvent>();
                        wellData.FacilityEvents.Add(facilityEvent);
                    }

                    if (!String.IsNullOrEmpty(wd.FacilityEventTypeID1) || wd.EffectiveDateTime1 != null)
                    {
                        FacilityEvent facilityEvent = new FacilityEvent();
                        if (!String.IsNullOrEmpty(wd.FacilityEventTypeID2)) facilityEvent.FacilityEventTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpFacilityEventType + GetLookupID(wd.FacilityEventTypeID2);
                        if (wd.EffectiveDateTime2 != null)facilityEvent.EffectiveDateTime = ((DateTime)(wd.EffectiveDateTime2)).ToUniversalTime();
                        if (wellData.FacilityEvents == null)
                            wellData.FacilityEvents = new List<FacilityEvent>();
                        wellData.FacilityEvents.Add(facilityEvent);
                    }


                    if (!String.IsNullOrEmpty(wd.VerticalMeasurementID1) || !String.IsNullOrEmpty(wd.VerticalMeasurementDescription1) || wd.VerticalMeasurement1 != null
                        || !String.IsNullOrEmpty(wd.VerticalMeasurementUnitOfMeasureID1) || !String.IsNullOrEmpty(wd.VerticalMeasurementPathID1) || !String.IsNullOrEmpty(wd.VerticalMeasurementTypeID1)
                        || !String.IsNullOrEmpty(wd.VerticalMeasurementSourceID1))
                        {
                        {
                            //VerticalMeasurements
                            VerticalMeasurements verticalMeasurements = new VerticalMeasurements();
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementID1)) verticalMeasurements.VerticalMeasurementID = wd.VerticalMeasurementID1;
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementDescription1)) verticalMeasurements.VerticalMeasurementDescription = wd.VerticalMeasurementDescription1;
                            if (wd.VerticalMeasurement1 != null) verticalMeasurements.VerticalMeasurement = (Double)wd.VerticalMeasurement1;
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementUnitOfMeasureID1)) verticalMeasurements.VerticalMeasurementUnitOfMeasureID = _parameters.DataPartitionID + Common.JsonTags.LookUpUnitOfMeasure + GetLookupID(wd.VerticalMeasurementUnitOfMeasureID1);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementPathID1)) verticalMeasurements.VerticalMeasurementPathID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementPath + GetLookupID(wd.VerticalMeasurementPathID1);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementTypeID1)) verticalMeasurements.VerticalMeasurementTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementType + GetLookupID(wd.VerticalMeasurementTypeID1);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementSourceID1)) verticalMeasurements.VerticalMeasurementSourceID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasuremenSource + GetLookupID(wd.VerticalMeasurementSourceID1);
                            if (wellData.VerticalMeasurements == null)
                                wellData.VerticalMeasurements = new List<VerticalMeasurements>();
                            wellData.VerticalMeasurements.Add(verticalMeasurements);
                        }

                        if (!String.IsNullOrEmpty(wd.VerticalMeasurementID2) || !String.IsNullOrEmpty(wd.VerticalMeasurementDescription2) || wd.VerticalMeasurement2 != null
                            || !String.IsNullOrEmpty(wd.VerticalMeasurementUnitOfMeasureID2) || !String.IsNullOrEmpty(wd.VerticalMeasurementPathID2)|| !String.IsNullOrEmpty(wd.VerticalMeasurementTypeID2)
                            || !String.IsNullOrEmpty(wd.VerticalMeasurementSourceID2))
                        {
                            VerticalMeasurements verticalMeasurements = new VerticalMeasurements();
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementID2)) verticalMeasurements.VerticalMeasurementID = wd.VerticalMeasurementID2;
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementDescription2)) verticalMeasurements.VerticalMeasurementDescription = wd.VerticalMeasurementDescription2;
                            if (wd.VerticalMeasurement2 != null) verticalMeasurements.VerticalMeasurement = (Double)wd.VerticalMeasurement2;
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementUnitOfMeasureID2)) verticalMeasurements.VerticalMeasurementUnitOfMeasureID = _parameters.DataPartitionID + Common.JsonTags.LookUpUnitOfMeasure + GetLookupID(wd.VerticalMeasurementUnitOfMeasureID2);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementPathID2)) verticalMeasurements.VerticalMeasurementPathID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementPath + GetLookupID(wd.VerticalMeasurementPathID2);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementTypeID2)) verticalMeasurements.VerticalMeasurementTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementType + GetLookupID(wd.VerticalMeasurementTypeID2);
                            if (!String.IsNullOrEmpty(wd.VerticalMeasurementSourceID2)) verticalMeasurements.VerticalMeasurementSourceID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasuremenSource + GetLookupID(wd.VerticalMeasurementSourceID2);
                            if (wellData.VerticalMeasurements == null)
                                wellData.VerticalMeasurements = new List<VerticalMeasurements>();
                            wellData.VerticalMeasurements.Add(verticalMeasurements);
                        }

                    }

                    masterData.data = wellData;
                    manifestIngestion.executionContext.manifest.MasterData.Add(masterData);
                }


            if (wellboreDatas.Count > 0)
                foreach (ModelWellboreData wbd in wellboreDatas)
                {
                    MasterData masterData = new MasterData();
                    masterData.id = _parameters.DataPartitionID + ":master-data--Wellbore:" + wbd.FacilityID + "";
                    masterData.kind = Common.JsonTags.WellboreKind;

                    WellboreData wellboreData = new WellboreData();
                    wellboreData.FacilityID = wbd.FacilityID;
                    wellboreData.FacilityName = wbd.FacilityName;

                    if (_parameters.DataPartitionID.ToUpper() != "OSDU")
                    {
                        masterData.acl.viewers = new String[] { "data.default.viewers@" + _parameters.DataPartitionID + ".testing.com" };
                        masterData.acl.owners = new String[] { "data.default.owners@" + _parameters.DataPartitionID + ".testing.com" };
                    }
                    //NameAlias nameAlias = new NameAlias();
                    //nameAlias.AliasNameTypeID = Common.JsonTags.NameAliasEntity_UWI;
                    //nameAlias.AliasName = wbd.UWBI;
                    //wellboreData.NameAliases.Add(nameAlias);

                    if (!String.IsNullOrEmpty(wbd.Country))
                    {
                        GeoContexts geoContexts = new GeoContexts();
                        geoContexts.GeoTypeID = _parameters.DataPartitionID + Common.JsonTags.GeoPoliticalEntity_Country;
                        geoContexts.GeoPoliticalEntityID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterGeoPoliticalEntity + GetLookupID(wbd.Country);
                        if(wellboreData.GeoContexts == null)
                        wellboreData.GeoContexts = new List<GeoContexts>();
                        wellboreData.GeoContexts.Add(geoContexts);
                    }

                    if (!String.IsNullOrEmpty(wbd.TrajectoryTypeID)) wellboreData.TrajectoryTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpWellboreTrajectoryType + GetLookupID(wbd.TrajectoryTypeID);
                    if (!String.IsNullOrEmpty(wbd.PrimaryMaterialID)) wellboreData.PrimaryMaterialID = _parameters.DataPartitionID + Common.JsonTags.LookUpMaterialType + GetLookupID(wbd.PrimaryMaterialID);

                    if (!String.IsNullOrEmpty(wbd.Status))
                    {
                        FacilityState facilityState = new FacilityState();
                        facilityState.FacilityStateTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpFacilityStateType + GetLookupID(wbd.Status);
                        if (wellboreData.FacilityStates == null)
                            wellboreData.FacilityStates= new List<FacilityState>();
                        wellboreData.FacilityStates.Add(facilityState);
                    }

                    if (wbd.StartDrillingDate != null)
                    {
                        //FacilityEvent
                        FacilityEvent facilityEvent = new FacilityEvent();
                        facilityEvent.FacilityEventTypeID = _parameters.DataPartitionID + Common.JsonTags.FacilityEventTypeEntity_ProductionStart;
                        if (wbd.StartDrillingDate != null)
                            facilityEvent.EffectiveDateTime = ((DateTime)(wbd.StartDrillingDate)).ToUniversalTime();
                        if (wellboreData.FacilityEvents == null)
                            wellboreData.FacilityEvents = new List<FacilityEvent>();
                        wellboreData.FacilityEvents.Add(facilityEvent);
                    }

                    if (wbd.FinishDrillingDate != null)
                    {
                        FacilityEvent facilityEvent = new FacilityEvent();
                        facilityEvent.FacilityEventTypeID = _parameters.DataPartitionID + Common.JsonTags.FacilityEventTypeEntity_ProductionFinish;

                        if (wbd.FinishDrillingDate != null)
                            facilityEvent.EffectiveDateTime = ((DateTime)(wbd.FinishDrillingDate)).ToUniversalTime();
                        if (wellboreData.FacilityEvents == null)
                            wellboreData.FacilityEvents = new List<FacilityEvent>();
                        wellboreData.FacilityEvents.Add(facilityEvent);
                    }

                    if (!String.IsNullOrEmpty(wbd.InitialOperatorID)) wellboreData.InitialOperatorID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterOrganisation + GetLookupID(wbd.InitialOperatorID);
                    if (!String.IsNullOrEmpty(wbd.CurrentOperatorID)) wellboreData.CurrentOperatorID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterOrganisation + GetLookupID(wbd.CurrentOperatorID);

                    if (!String.IsNullOrEmpty(wbd.WellID)) wellboreData.WellID = _parameters.DataPartitionID + Common.JsonTags.LookUpMasterWell + GetLookupID(wbd.WellID);
                    if (wbd.SequenceNumber!=null) wellboreData.SequenceNumber = (Int32)wbd.SequenceNumber;

                    if(wbd.TD_VerticalMeasurement!=null || !String.IsNullOrEmpty (wbd.TD_VerticalMeasurementUnitOfMeasureID))
                    {
                        //VerticalMeasurements
                        VerticalMeasurements verticalMeasurements = new VerticalMeasurements();
                        verticalMeasurements.VerticalMeasurementID = "Driller-TD";
                        verticalMeasurements.VerticalMeasurementDescription = "Driller-TD";
                        if(wbd.TD_VerticalMeasurement != null) verticalMeasurements.VerticalMeasurement = (Double)wbd.TD_VerticalMeasurement;
                        if (!String.IsNullOrEmpty(wbd.TD_VerticalMeasurementUnitOfMeasureID)) verticalMeasurements.VerticalMeasurementUnitOfMeasureID = _parameters.DataPartitionID + Common.JsonTags.LookUpUnitOfMeasure + GetLookupID(wbd.TD_VerticalMeasurementUnitOfMeasureID);
                        verticalMeasurements.VerticalMeasurementPathID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementPath + "MD" + ":";
                        verticalMeasurements.VerticalMeasurementTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementType + "TD" + ":";
                        //verticalMeasurements.VerticalCRSID = Common.JsonTags.LookUpCoordinateReferenceSystem + GetLookupID(wbd.MD_VerticalCRSID);
                        if (wellboreData.VerticalMeasurements == null)
                            wellboreData.VerticalMeasurements = new List<VerticalMeasurements>();
                        wellboreData.VerticalMeasurements.Add(verticalMeasurements);
                    }

                    if (wbd.RT_VerticalMeasurement != null || !String.IsNullOrEmpty(wbd.RT_VerticalMeasurementUnitOfMeasureID))
                    {
                        //VerticalMeasurements
                        VerticalMeasurements verticalMeasurements = new VerticalMeasurements();
                        verticalMeasurements.VerticalMeasurementID = "RT Elevation";
                        verticalMeasurements.VerticalMeasurementDescription = "RT Elevation";
                        if (wbd.RT_VerticalMeasurement != null) verticalMeasurements.VerticalMeasurement = (Double)wbd.RT_VerticalMeasurement;
                        if (!String.IsNullOrEmpty(wbd.RT_VerticalMeasurementUnitOfMeasureID)) verticalMeasurements.VerticalMeasurementUnitOfMeasureID = _parameters.DataPartitionID + Common.JsonTags.LookUpUnitOfMeasure + GetLookupID(wbd.RT_VerticalMeasurementUnitOfMeasureID);
                        verticalMeasurements.VerticalMeasurementPathID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementPath + "ELEV" + ":";
                        verticalMeasurements.VerticalMeasurementTypeID = _parameters.DataPartitionID + Common.JsonTags.LookUpVerticalMeasurementType + "RT" + ":";
                        //verticalMeasurements.VerticalCRSID = Common.JsonTags.LookUpCoordinateReferenceSystem + GetLookupID(wbd.MD_VerticalCRSID);
                        if (wellboreData.VerticalMeasurements == null)
                            wellboreData.VerticalMeasurements = new List<VerticalMeasurements>();
                        wellboreData.VerticalMeasurements.Add(verticalMeasurements);
                    }

                    masterData.data = wellboreData;
                    manifestIngestion.executionContext.manifest.MasterData.Add(masterData);

                }

            JsonSerializerOptions options = new JsonSerializerOptions();
            options.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;

            String JsonText = JsonSerializer.Serialize(manifestIngestion,options);

            ExcelWrite.WriteJsonExecute(JsonText, manifestIngestion.runId);

            log.SetLog(ModelLog.Notification.Info, ModelLog.Method.ConvertDataToJsonFormat, ModelLog.Status.Success, "Manifest JSON file succesfully saved in "+ ExcelWrite.JsonFilePath + @"\" + manifestIngestion.runId +".json");
            request.AddJsonBody(JsonText);

            IRestResponse response = _client.Execute(request);
            ResponseStatusCode = response.StatusCode;
            ResponseContent = response.Content;

            String remarkLog = "Response Body: HTML STATUS Code " + response.StatusCode + "\r\n" + ResponseContent;
            //Bearer token may expire during a running session of the app (normally after 60 mins).
            //If so then clear the bearer token and try again.
            //The Authorization object will then only make a limited number of attempts and error out if still getting access denied.
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                log.SetLog(ModelLog.Notification.Warning, ModelLog.Method.DataLoadToOSDUPlatform , ModelLog.Status.Failed,
                remarkLog,ModelLog.URLMethod.Post, _parameters.StorageURL + Common.URLs.ManifestURL);
                _auth.ClearBearerToken();
                Store_Manifest(wellDatas,wellboreDatas,out id, ref log);
            }
            //otherwise, good response then parse out the returned id value
            //AWS returns Created
            else if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK)
            {
                using (JsonDocument document = JsonDocument.Parse(response.Content))
                {
                    if (document.RootElement.TryGetProperty(Common.JsonTags.ManifestRecordIDs, out JsonElement result))
                    {
                        log.SetLog(ModelLog.Notification.Info, ModelLog.Method.DataLoadToOSDUPlatform, ModelLog.Status.Success,
                        remarkLog, ModelLog.URLMethod.Post, _parameters.StorageURL + Common.URLs.ManifestURL);
                        id = result.GetString();
                    }
                }
            }
        }


        public String GetLookupID(String keyValue)
        {
            return keyValue.Split('|').Last().Trim() + ":";
        }
    }
}
