﻿using OSDU_API;
using System;
using System.Windows.Forms;
using System.Text.Json;
using System.IO;
using OSDU.Model;

namespace OSDU
{
    public partial class ViewConnection : Form
    {
        public Authenticator Auth;
        public DataSourceParameters DSParams;
        public ViewConnection(Authenticator auth, DataSourceParameters parameters)
        {
            InitializeComponent();
            Auth = auth;
            DSParams = parameters;
            DialogResult = DialogResult.Cancel;
            PopulateControls();
        }

        private void PopulateControls()
        {
            txtPoolID.Text = Auth.PoolID;
            txtClientID.Text = Auth.ClientID;
            txtClientSecret.Text = Auth.ClientSecret;
            txtRefreshURL.Text = Auth.RefreshTokenURL;
            txtRefreshType.Text = Auth.RefreshContentType;

            txtSearchURL.Text = DSParams.SearchURL;
            txtStorageURL.Text = DSParams.StorageURL;
            txtDataPartitionID.Text = DSParams.DataPartitionID;
            txtSchemaVersion.Text = DSParams.SchemaVersion;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnApply_Click(object sender, EventArgs e)
        {
            if (txtPoolID.Text.Trim().Length == 0 || txtClientID.Text.Trim().Length == 0 || txtClientSecret.Text.Trim().Length == 0 || txtRefreshURL.Text.Trim().Length == 0 || txtRefreshType.Text.Trim().Length == 0 )
                MessageBox.Show("Please enter all settings.", btnApply.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                UpdateFromControls(Auth, DSParams);
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void UpdateFromControls(Authenticator auth, DataSourceParameters dsParams)
        {
            auth.PoolID = txtPoolID.Text.Trim();
            auth.ClientID = txtClientID.Text.Trim();
            auth.ClientSecret = txtClientSecret.Text.Trim();
            auth.RefreshTokenURL = txtRefreshURL.Text.Trim();
            auth.RefreshContentType = txtRefreshType.Text.Trim();

            dsParams.SearchURL = txtSearchURL.Text.Trim();
            dsParams.StorageURL = txtStorageURL.Text.Trim();
            dsParams.DataPartitionID = txtDataPartitionID.Text.Trim().ToLower();
            dsParams.SchemaVersion = txtSchemaVersion.Text.Trim();
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON files (*.json)|*.json";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                String filePathName = openFileDialog.FileName.Trim();
                if (!String.IsNullOrEmpty(filePathName))
                {
                    try
                    {
                        Cursor = Cursors.WaitCursor;
                        ModelConnectionInfo connInfo = new ModelConnectionInfo();
                        using (StreamReader file = new StreamReader(filePathName))
                        {
                            connInfo = JsonSerializer.Deserialize<ModelConnectionInfo>(file.ReadToEnd());
                            file.Close();
                        }
                        Auth = connInfo.Auth;
                        DSParams = connInfo.DSParams; 
                        PopulateControls();

                        Cursor = Cursors.Default;
                        MessageBox.Show(this, "Import complete.", btnImport.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show(this, "Unexpected error:\r\n" + ex.ToString(), btnImport.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JSON files (*.json)|*.json";
            DialogResult result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                String filePathName = saveFileDialog.FileName.Trim();
                if (!String.IsNullOrEmpty(filePathName))
                {
                    try
                    {
                        Cursor = Cursors.WaitCursor;
                        UpdateFromControls(Auth, DSParams);
                        ModelConnectionInfo connInfo = new ModelConnectionInfo();
                        connInfo.Auth = Auth;
                        connInfo.DSParams = DSParams;
                        using (StreamWriter file = new StreamWriter(filePathName))
                        {
                            file.Write(JsonSerializer.Serialize<ModelConnectionInfo>(connInfo));
                            file.Flush();
                            file.Close();
                        }
                        Cursor = Cursors.Default;
                        MessageBox.Show(this, "Export complete.", btnExport.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show(this, "Unexpected error:\r\n" + ex.ToString(), btnExport.Text.Replace("&", ""), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }


    }
}
