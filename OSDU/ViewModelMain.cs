﻿using OSDU.Model;
using OSDU_API;
using OSDU_API.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OSDU
{
    public class ViewModelMain
    {
        public DataSourceParameters DSParams = new DataSourceParameters();

        public Authenticator Auth = new Authenticator();

        public Model.ModelDropDownListResult search;

        public ModelLog Log = new ModelLog();

        public String SetupAllLookUpValues()
        {
            String ret = String.Empty;
            try
            {   
                search = new Model.ModelDropDownListResult(Auth, DSParams);
                search.SetAllLookUpValues(ref Log);
            }
            catch(Exception ex)
            {
                ret = ex.Message;
            }
            return ret;
        }

        public String GenerateOSDUTemplate(String filePath)
        {
            String ret = String.Empty;
            try
            {
                ModelExcelTemplate collection = new ModelExcelTemplate();
                collection.WriteExcel(filePath, search);
            }
            catch (Exception ex)
            {
                ret = ex.Message;
            }
            return ret;
        }

        public String ManifestIngestionProcess(Authenticator auth, DataSourceParameters dsParams, List<ModelWellData> wellDatas , List<ModelWellboreData> wellboreDatas, out String id)
        {
            String ret = String.Empty;
            try
            {
                StorageService storage = new StorageService(auth, dsParams);
                storage.Store_Manifest(wellDatas, wellboreDatas, out id, ref Log);
            }
            catch (Exception ex)
            {
                id = string.Empty;
                ret = ex.Message;
            }
            return ret;
        }

    }
}
