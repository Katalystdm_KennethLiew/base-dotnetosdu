﻿namespace OSDU
{
    partial class ViewMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewMain));
            this.SCMain = new System.Windows.Forms.SplitContainer();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.btnConfigureConnection = new System.Windows.Forms.Button();
            this.btnSignIn = new System.Windows.Forms.Button();
            this.txtboxUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnImportData = new System.Windows.Forms.Button();
            this.btnGenerateWellTemplate = new System.Windows.Forms.Button();
            this.lblWellbore = new System.Windows.Forms.Label();
            this.rtxtBoxLog = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatuslblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.SCMain)).BeginInit();
            this.SCMain.Panel1.SuspendLayout();
            this.SCMain.Panel2.SuspendLayout();
            this.SCMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SCMain
            // 
            this.SCMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SCMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SCMain.IsSplitterFixed = true;
            this.SCMain.Location = new System.Drawing.Point(0, 0);
            this.SCMain.Name = "SCMain";
            this.SCMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SCMain.Panel1
            // 
            this.SCMain.Panel1.Controls.Add(this.btnLogOut);
            this.SCMain.Panel1.Controls.Add(this.btnConfigureConnection);
            this.SCMain.Panel1.Controls.Add(this.btnSignIn);
            this.SCMain.Panel1.Controls.Add(this.txtboxUserName);
            this.SCMain.Panel1.Controls.Add(this.lblUserName);
            // 
            // SCMain.Panel2
            // 
            this.SCMain.Panel2.Controls.Add(this.splitContainer1);
            this.SCMain.Panel2.Controls.Add(this.statusStrip1);
            this.SCMain.Size = new System.Drawing.Size(750, 484);
            this.SCMain.SplitterDistance = 36;
            this.SCMain.TabIndex = 1;
            // 
            // btnLogOut
            // 
            this.btnLogOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogOut.Location = new System.Drawing.Point(667, 9);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 23);
            this.btnLogOut.TabIndex = 4;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.BtnLogOut_Click);
            // 
            // btnConfigureConnection
            // 
            this.btnConfigureConnection.Location = new System.Drawing.Point(433, 9);
            this.btnConfigureConnection.Name = "btnConfigureConnection";
            this.btnConfigureConnection.Size = new System.Drawing.Size(162, 23);
            this.btnConfigureConnection.TabIndex = 3;
            this.btnConfigureConnection.Text = "Configure Connection";
            this.btnConfigureConnection.UseVisualStyleBackColor = true;
            this.btnConfigureConnection.Click += new System.EventHandler(this.BtnConfigureConnection_Click);
            // 
            // btnSignIn
            // 
            this.btnSignIn.Location = new System.Drawing.Point(352, 9);
            this.btnSignIn.Name = "btnSignIn";
            this.btnSignIn.Size = new System.Drawing.Size(75, 23);
            this.btnSignIn.TabIndex = 2;
            this.btnSignIn.Text = "Sign In";
            this.btnSignIn.UseVisualStyleBackColor = true;
            this.btnSignIn.Click += new System.EventHandler(this.BtnSignIn_Click);
            // 
            // txtboxUserName
            // 
            this.txtboxUserName.Location = new System.Drawing.Point(146, 9);
            this.txtboxUserName.Name = "txtboxUserName";
            this.txtboxUserName.Size = new System.Drawing.Size(200, 20);
            this.txtboxUserName.TabIndex = 1;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(8, 9);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(66, 13);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "User Name :";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnImportData);
            this.splitContainer1.Panel1.Controls.Add(this.btnGenerateWellTemplate);
            this.splitContainer1.Panel1.Controls.Add(this.lblWellbore);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rtxtBoxLog);
            this.splitContainer1.Size = new System.Drawing.Size(750, 422);
            this.splitContainer1.SplitterDistance = 33;
            this.splitContainer1.TabIndex = 9;
            // 
            // btnImportData
            // 
            this.btnImportData.Location = new System.Drawing.Point(183, 3);
            this.btnImportData.Name = "btnImportData";
            this.btnImportData.Size = new System.Drawing.Size(166, 23);
            this.btnImportData.TabIndex = 5;
            this.btnImportData.Text = "Import Data";
            this.btnImportData.UseVisualStyleBackColor = true;
            this.btnImportData.Click += new System.EventHandler(this.BtnImportData_Click);
            // 
            // btnGenerateWellTemplate
            // 
            this.btnGenerateWellTemplate.Location = new System.Drawing.Point(11, 3);
            this.btnGenerateWellTemplate.Name = "btnGenerateWellTemplate";
            this.btnGenerateWellTemplate.Size = new System.Drawing.Size(166, 23);
            this.btnGenerateWellTemplate.TabIndex = 4;
            this.btnGenerateWellTemplate.Text = "Generate Template";
            this.btnGenerateWellTemplate.UseVisualStyleBackColor = true;
            this.btnGenerateWellTemplate.Click += new System.EventHandler(this.BtnGenerateWellTemplate_Click);
            // 
            // lblWellbore
            // 
            this.lblWellbore.AutoSize = true;
            this.lblWellbore.Location = new System.Drawing.Point(-131, 33);
            this.lblWellbore.Name = "lblWellbore";
            this.lblWellbore.Size = new System.Drawing.Size(58, 13);
            this.lblWellbore.TabIndex = 0;
            this.lblWellbore.Text = "Wellbore : ";
            // 
            // rtxtBoxLog
            // 
            this.rtxtBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtBoxLog.Location = new System.Drawing.Point(0, 0);
            this.rtxtBoxLog.Name = "rtxtBoxLog";
            this.rtxtBoxLog.ReadOnly = true;
            this.rtxtBoxLog.Size = new System.Drawing.Size(750, 385);
            this.rtxtBoxLog.TabIndex = 0;
            this.rtxtBoxLog.Text = "";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatuslblStatus,
            this.toolStripProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 422);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(750, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatuslblStatus
            // 
            this.toolStripStatuslblStatus.Name = "toolStripStatuslblStatus";
            this.toolStripStatuslblStatus.Size = new System.Drawing.Size(129, 17);
            this.toolStripStatuslblStatus.Text = "toolStripStatuslblStatus";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.MarqueeAnimationSpeed = 30;
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(200, 16);
            this.toolStripProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // ViewMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 484);
            this.Controls.Add(this.SCMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewMain";
            this.Text = "Excel loader for OSDU Data Platform (R3)";
            this.SCMain.Panel1.ResumeLayout(false);
            this.SCMain.Panel1.PerformLayout();
            this.SCMain.Panel2.ResumeLayout(false);
            this.SCMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SCMain)).EndInit();
            this.SCMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer SCMain;
        private System.Windows.Forms.Button btnConfigureConnection;
        private System.Windows.Forms.Button btnSignIn;
        private System.Windows.Forms.TextBox txtboxUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblWellbore;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatuslblStatus;
        private System.Windows.Forms.Button btnGenerateWellTemplate;
        private System.Windows.Forms.Button btnImportData;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.RichTextBox rtxtBoxLog;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
    }
}

