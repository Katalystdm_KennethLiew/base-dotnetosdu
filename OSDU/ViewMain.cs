﻿using Amazon.CognitoIdentityProvider;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.Runtime;
using OSDU_API.Objects;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.XSSF.UserModel;
using OSDU.Model;
using System.Linq;
using System.Text.RegularExpressions;

namespace OSDU
{
    public partial class ViewMain : Form
    {
        public ViewModelMain ViewModelMain { get; } = new ViewModelMain();

        public const String WellSheetName = "Well";
        public const String WellboreSheetName = "Wellbore";

        public ViewMain()
        {
            InitializeComponent();
            toolStripProgressBar.Visible = false;
            btnSignIn.Enabled = true;
            btnLogOut.Enabled = false;
            toolStripStatuslblStatus.Text = "Not connected...";
        }

        private void BtnSignIn_Click(object sender, EventArgs e)
        {
            try
            {

                String result = ViewModelMain.Auth.ValidateCredentials(false);
                if (result.Length > 0)
                    MessageBox.Show(this, "Please configure your connection, missing connection details.", "Sign In", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else if (txtboxUserName.Text.Trim().Length == 0)
                    MessageBox.Show(this, "Please enter a user name.", "Sign In", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    String pwd = Framework.Common.GetInput("Enter password:", String.Empty, true, this);
                    if (pwd.Length > 0)
                    {
                        toolStripStatuslblStatus.Text = "Not connected...";
                        ViewModelMain.Auth.ResetConnection();
                        ViewModelMain.Auth.UserID = txtboxUserName.Text.Trim();
                        ViewModelMain.Auth.UserPWD = pwd.Trim();

                        SetLog(ModelLog.Notification.Info, ModelLog.Method.Login, ModelLog.Status.InProgress, "");

                        Login();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnConfigureConnection_Click(object sender, EventArgs e)
        {
            ViewConnection vConn = new ViewConnection(ViewModelMain.Auth, ViewModelMain.DSParams);
            DialogResult ret = vConn.ShowDialog(this);
            if (ret == DialogResult.OK)
            {
                toolStripStatuslblStatus.Text = "Not connected...";
                ViewModelMain.Auth = vConn.Auth;
                ViewModelMain.Auth.ResetConnection();
                ViewModelMain.DSParams = vConn.DSParams;
                SetLog(ModelLog.Notification.Info, ModelLog.Method.ConfigureConnection, ModelLog.Status.Success, "Data Partition ID : " + ViewModelMain.DSParams.DataPartitionID) ;
            }
        }

        private void BtnGenerateWellTemplate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ViewModelMain.Auth.RefreshToken))
            {
                ClearSession();
                MessageBox.Show("Please login before process this method. ");
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = "Well_" + Guid.NewGuid() + ".xlsx";
                saveFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx";
                DialogResult result = saveFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    GenerateWellTemplate_MainAsync(saveFileDialog.FileName);
                }

            }

        }

        private void BtnImportData_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ViewModelMain.Auth.RefreshToken))
            {
                ClearSession();
                MessageBox.Show("Please login before process this method. ");
            }
            else
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx";
                DialogResult result = openFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    ImportData_MainAsync(openFileDialog.FileName);
                }
            }
        }

        private void BtnLogOut_Click(object sender, EventArgs e)
        {
            ClearSession();
            SetLog(ModelLog.Notification.Info, ModelLog.Method.LogOut, ModelLog.Status.Success, "User " + ViewModelMain.Auth.UserID + " logged out successfully.");
        }

        public void SetLog(String notification, String method, String status, String remark, String uRLMethod = "", String uRL = "", Boolean SynctxtBoxLog = true)
        {
            ViewModelMain.Log.SetLog(notification, method, status, remark, uRLMethod, uRL);
            if(SynctxtBoxLog)
            {
                rtxtBoxLog.Text = ViewModelMain.Log.Log;
                rtxtBoxLog.SelectionStart = rtxtBoxLog.Text.Length;
                rtxtBoxLog.ScrollToCaret();
                rtxtBoxLog.Refresh();
            }
        }

        public String ExcelConvertStringData(ICell cell)
        {
            String data = String.Empty;
            if (cell == null)
                data = String.Empty;
            else if (cell.CellType == CellType.String)
                data = cell.StringCellValue;
            else if (cell.CellType == CellType.Numeric)
                data = cell.NumericCellValue.ToString();
            else if (cell.CellType == CellType.Blank)
                data = String.Empty;
            return data;
        }

        public Boolean ExcelConvertDoubleData(String strValue, ref Double dblValue)
        {
            return Double.TryParse(strValue, out dblValue);
        }

        public Boolean ExcelConvertInt32Data(String strValue, ref Int32 int32Value)
        {
            return Int32.TryParse(strValue, out int32Value);
        }

        public Boolean IDValidate(String value)
        {
           String ret = String.Empty;
            Regex rgx = new Regex("[^A-Za-z0-9]");
            if (!rgx.IsMatch(value) && value.Trim().Any(c => !char.IsWhiteSpace(c)))
                return true;
            else
                return false;
        }

        private void EnableDisableProgressBar(Boolean enable)
        {
            toolStripProgressBar.Visible = enable;
            foreach (Control ctl in Controls) ctl.Enabled = !enable;
        }

        private async void Login()
        {
            try
            {
                EnableDisableProgressBar(true);
                Cursor = Cursors.WaitCursor;
                String ret = ViewModelMain.DSParams.ValidateParameters();
                if (ret.Length == 0)
                {

                    String result = String.Empty;
                    if (String.IsNullOrEmpty(ViewModelMain.Auth.RefreshToken))
                    {
                        result = await GetRefreshToken();
                    }

                    if (!String.IsNullOrWhiteSpace(result))
                    {
                        SetLog(ModelLog.Notification.Warning, ModelLog.Method.Login, ModelLog.Status.Failed, result);
                        MessageBox.Show(result);
                    }
                    else
                    {
                        RestRequest request;
                        request = new RestRequest(Method.GET);
                        request.AddHeader("data-partition-id", ViewModelMain.DSParams.DataPartitionID);

                        RestClient client = new RestClient(ViewModelMain.DSParams.StorageURL + "/api/storage/v2/query/kinds");
                        client.Authenticator = ViewModelMain.Auth;
                        client.Timeout = -1;
                        request.RequestFormat = DataFormat.Json;
                        IRestResponse response = null;
                        try
                        {
                            response = client.Execute(request);
                        }
                        catch (Exception ex)
                        {
                            ret = ex.Message.ToString();
                        }
                        if (ret.Length > 0)
                        {
                            SetLog(ModelLog.Notification.Warning, ModelLog.Method.Login, ModelLog.Status.Failed, "");
                            MessageBox.Show(this, "Unable to sign in:\r\n" + ret, "Sign In", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                        {
                            ViewModelMain.Log.SetLog(ModelLog.Notification.Warning, ModelLog.Method.Login, ModelLog.Status.Failed, response.Content);
                            ret = response.Content;
                        }
                        else
                        {
                            SetLog(ModelLog.Notification.Info, ModelLog.Method.Login, ModelLog.Status.Success, "User " + ViewModelMain.Auth.UserID + " logged in successfully.");
                            toolStripStatuslblStatus.Text = "Connected to " + ViewModelMain.Auth.UserID + "@" + ViewModelMain.DSParams.DataPartitionID + "@" + "EL el-20210706-1";
                            btnSignIn.Enabled = false;
                            btnLogOut.Enabled = true;
                        }

                    }

                }
                Cursor = Cursors.Default;
                EnableDisableProgressBar(false);
            }
            catch (Exception ex)
            {
                EnableDisableProgressBar(false);
                Cursor = Cursors.Default;
                MessageBox.Show(this, "Failed to sign in:\r\n" + ex.Message, "Sign In", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private async Task<String> GetRefreshToken()
        {
            try
            {
                var provider = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), ViewModelMain.Auth.GetRegionEndPoint());
                var userPool = new CognitoUserPool(ViewModelMain.Auth.PoolID, ViewModelMain.Auth.ClientID, provider, ViewModelMain.Auth.ClientSecret);
                var user = new CognitoUser(ViewModelMain.Auth.UserID, ViewModelMain.Auth.ClientID, userPool, provider, ViewModelMain.Auth.ClientSecret);
                var password = ViewModelMain.Auth.UserPWD;

                AuthFlowResponse authResponse = await user.StartWithSrpAuthAsync(new InitiateSrpAuthRequest()
                {
                    Password = password

                }).ConfigureAwait(true);
                ViewModelMain.Auth.RefreshToken = authResponse.AuthenticationResult.RefreshToken;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

            return String.Empty;
        }

        private async void ImportData_MainAsync(String filePath)
        {
            try
            {
                EnableDisableProgressBar(true);
                Cursor = Cursors.WaitCursor;
                Int32 rowCount = 0;
                String runID = String.Empty;
                SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupData, ModelLog.Status.InProgress, "");
                String result = await Task.Run(() => ImportData_Sub(filePath,ref rowCount,ref runID));
                EnableDisableProgressBar(false);
                Cursor = Cursors.Default;
                if (!String.IsNullOrEmpty(result))
                {
                    String Msg = "Failed to load data to OSDU platform. \r\n" + result;
                    EnableDisableProgressBar(false);
                    MessageBox.Show(Msg);
                    SetLog(ModelLog.Notification.Error, ModelLog.Method.DataLoadToOSDUPlatform, ModelLog.Status.Failed, Msg);
                }
                else
                {
                    EnableDisableProgressBar(false);
                    String successMsg = "Manifest json created for " + rowCount.ToString() + " records.\r\nYou may find JSON file (for your reference only) " + runID + ".json at \"C:\"Temp\" location.\r\nWorkflow initiated for manifest - based ingestion.RunID = " + runID.ToString() + ".\r\nYou may check its status and outcome from Airflow console.";
                    SetLog(ModelLog.Notification.Info, ModelLog.Method.DataLoadToOSDUPlatform, ModelLog.Status.Success, successMsg);
                    MessageBox.Show(successMsg);
                }


            }
            catch (Exception ex)
            {
                String ErrMsg = "Failed to Generate the Excel file." + ex.Message;
                EnableDisableProgressBar(false);
                Cursor = Cursors.Default;
                SetLog(ModelLog.Notification.Warning, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Failed, ErrMsg);
                MessageBox.Show(ErrMsg);
            }
           
        }

        private String ImportData_Sub(String filePath , ref Int32 rowUpload, ref String runID) 
        {
            String ret = String.Empty;
            try
            {
                
                IWorkbook book;
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                book = new XSSFWorkbook(fs);
                ISheet sheetWell = book.GetSheet(WellSheetName);
                ISheet sheetWellbore = book.GetSheet(WellboreSheetName);
                List<ModelWellData> wellDatas = new List<ModelWellData>();
                List<ModelWellboreData> wellboreDatas = new List<ModelWellboreData>();

                if (sheetWell.LastRowNum == 0 && sheetWellbore.LastRowNum == 0)
                {
                    ViewModelMain.Log.SetLog(ModelLog.Notification.Warning, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Failed, "The Excel sheet not contain any data.");
                    throw new Exception("The Excel sheet doesn't contain any data.");
                }
                else
                {
                    if (sheetWell.LastRowNum > 0)
                        if (sheetWell.GetRow(1).GetCell((Int32)GetColNum_Well.FacilityName) != null)
                            for (int row = 1; row <= sheetWell.LastRowNum; row++)
                            {
                                ModelWellData wellData = new ModelWellData();
                                if (sheetWell.GetRow(row) != null && (!sheetWell.GetRow(row).All(d => d.CellType == CellType.Blank))) //null is when the row only contains empty cells 
                                {
                                    Double tempDouble = 0;

                                    if(String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityID))) || String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityName))))
                                    {
                                        throw new Exception(ModelWell.FacilityID + " and " + ModelWell.FacilityName + " is mandatory field on " + WellSheetName + "'s sheet.");
                                    }
                                    if(IDValidate(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityID))))
                                      wellData.FacilityID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityID));
                                    else
                                      throw new Exception( "Well - " + nameof(GetColNum_Well.FacilityID) + " - \"" + ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityID)) + "\" contains special character and/or space. Only alphabet and numbers are allowed.");
                                    
                                    wellData.FacilityName = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityName));
                                    wellData.InitialOperatorID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.InitialOperatorID));
                                    wellData.CurrentOperatorID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.CurrentOperatorID));
                                    wellData.DataSourceOrganisationID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.DataSourceOrganisationID));
                                    wellData.OperatingEnvironmentID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.OperatingEnvironmentID));
                                    wellData.FacilityEventTypeID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityEventTypeID1));
                                    wellData.EffectiveDateTime1 = String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.EffectiveDateTime1)))? (DateTime?)null : sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.EffectiveDateTime1).DateCellValue;
                                    wellData.FacilityEventTypeID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.FacilityEventTypeID2));
                                    wellData.EffectiveDateTime2 = String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.EffectiveDateTime2))) ? (DateTime?)null : sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.EffectiveDateTime2).DateCellValue;
                                    wellData.InterestTypeID = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.InterestTypeID));
                                    wellData.VerticalMeasurementID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementID1));
                                    wellData.VerticalMeasurementDescription1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementDescription1));
                                    if (!String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurement1))))
                                    if (!ExcelConvertDoubleData(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurement1)), ref tempDouble))
                                        throw new Exception("Well sheet - VerticalMeasurement1 field (Row " + row + ") contain invalid data.");
                                    else
                                        wellData.VerticalMeasurement1 = tempDouble;

                                    wellData.VerticalMeasurementUnitOfMeasureID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementUnitOfMeasureID1));
                                    wellData.VerticalMeasurementPathID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementPathID1));
                                    wellData.VerticalMeasurementTypeID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementTypeID1));
                                    wellData.VerticalMeasurementSourceID1 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementSourceID1));
                                    wellData.VerticalMeasurementID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementID2));
                                    wellData.VerticalMeasurementDescription2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementDescription2));
                                    if (!String.IsNullOrEmpty(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurement2))))
                                        if (!ExcelConvertDoubleData(ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurement2)), ref tempDouble))
                                        throw new Exception("Well sheet - VerticalMeasurement2 field (Row " + row + ") contain invalid data.");
                                    else
                                        wellData.VerticalMeasurement2 = tempDouble;
                                    wellData.VerticalMeasurementUnitOfMeasureID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementUnitOfMeasureID2));
                                    wellData.VerticalMeasurementPathID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementPathID2));
                                    wellData.VerticalMeasurementTypeID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementTypeID2));
                                    wellData.VerticalMeasurementSourceID2 = ExcelConvertStringData(sheetWell.GetRow(row).GetCell((Int32)GetColNum_Well.VerticalMeasurementSourceID2));
                                    wellDatas.Add(wellData);
                                }
                            }

                    if (sheetWellbore.LastRowNum > 0)
                        if (sheetWellbore.GetRow(1).GetCell((Int32)GetColNum_Wellbore.FacilityID) != null || sheetWellbore.GetRow(1).GetCell((Int32)GetColNum_Wellbore.FacilityName) != null)
                            for (int row = 1; row <= sheetWellbore.LastRowNum; row++)
                            {
                                ModelWellboreData wellboreData = new ModelWellboreData();
                                if (sheetWellbore.GetRow(row) != null && (!sheetWellbore.GetRow(row).All(d => d.CellType == CellType.Blank))) //null is when the row only contains empty cells 
                                {
                                    Double tempDouble = 0;
                                    Int32 tempInt32 = 0;

                                    if (String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityID))) && String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityName))) && String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.Country))))
                                    {
                                        throw new Exception(ModelWellbore.FacilityID + " and " + ModelWellbore.FacilityName + " is mandatory field on " + WellboreSheetName + "'s sheet.");
                                    }

                                    if (String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityID))) || String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityName))))
                                    {
                                        throw new Exception(ModelWellbore.FacilityID + " and " + ModelWellbore.FacilityName + " is mandatory field on " + WellboreSheetName + "'s sheet.");
                                    }
                                    if (IDValidate(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityID))))
                                        wellboreData.FacilityID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityID));
                                    else
                                        throw new Exception("Wellbore - " + nameof(GetColNum_Wellbore.FacilityID) + " - \"" + ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityID)) + "\" contains special character and/or space. Only alphabet and numbers are allowed.");
                                    wellboreData.FacilityName = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FacilityName));
                                    wellboreData.Country = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.Country));
                                    wellboreData.TrajectoryTypeID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.TrajectoryTypeID));
                                    wellboreData.PrimaryMaterialID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.PrimaryMaterialID));
                                    wellboreData.Status = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.Status));
                                    wellboreData.StartDrillingDate = String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.StartDrillingDate))) ? (DateTime?)null : (DateTime)sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.StartDrillingDate).DateCellValue;
                                    wellboreData.FinishDrillingDate = String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FinishDrillingDate))) ? (DateTime?)null : (DateTime)sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.FinishDrillingDate).DateCellValue;
                                    wellboreData.InitialOperatorID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.InitialOperatorID));
                                    wellboreData.CurrentOperatorID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.CurrentOperatorID));
                                    wellboreData.WellID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.WellID));
                                    if (!String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.SequenceNumber))))
                                        if (!ExcelConvertInt32Data(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.SequenceNumber)), ref tempInt32))
                                            throw new Exception("Wellbore sheet - TD_VerticalMeasurement field (Row " + row + ") contain invalid data.");
                                        else
                                            wellboreData.SequenceNumber = tempInt32;
                                    if (!String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.TD_VerticalMeasurement))))
                                        if (!ExcelConvertDoubleData(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.TD_VerticalMeasurement)), ref tempDouble))
                                        throw new Exception("Wellbore sheet - TD_VerticalMeasurement field (Row " + row + ") contain invalid data.");
                                    else
                                        wellboreData.TD_VerticalMeasurement = tempDouble;
                                    wellboreData.TD_VerticalMeasurementUnitOfMeasureID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.TD_VerticalMeasurementUnitOfMeasureID));
                                    if (!String.IsNullOrEmpty(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.RT_VerticalMeasurement))))
                                        if (!ExcelConvertDoubleData(ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.RT_VerticalMeasurement)), ref tempDouble))
                                        throw new Exception("Wellbore sheet - RT_VerticalMeasurement field (Row " + row + ") contain invalid data.");
                                    else
                                        wellboreData.RT_VerticalMeasurement = tempDouble;
                                    wellboreData.RT_VerticalMeasurementUnitOfMeasureID = ExcelConvertStringData(sheetWellbore.GetRow(row).GetCell((Int32)GetColNum_Wellbore.RT_VerticalMeasurementUnitOfMeasureID));
                                    wellboreDatas.Add(wellboreData);
                                }
                            }

                    if (wellDatas.Count == 0 && wellboreDatas.Count == 0)
                    {
                        ViewModelMain.Log.SetLog(ModelLog.Notification.Warning, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Failed, "The Excel sheet not contain any data.");
                        throw new Exception("The Excel sheet doesn't contain any data.");
                    }

                    ViewModelMain.Log.SetLog(ModelLog.Notification.Info, ModelLog.Method.ImportExcelSheetDataToApplication, ModelLog.Status.Success, "Excel sheet contain " + wellDatas.Count.ToString() + " well records");
                    ViewModelMain.Log.SetLog(ModelLog.Notification.Info, ModelLog.Method.ImportExcelSheetDataToApplication, ModelLog.Status.Success, "Excel sheet contain " + wellboreDatas.Count.ToString() + " wellbore records");
                    String id = String.Empty;
                    ViewModelMain.Log.SetLog(ModelLog.Notification.Info, ModelLog.Method.DataLoadToOSDUPlatform, ModelLog.Status.InProgress, "");
                    ret = ViewModelMain.ManifestIngestionProcess(ViewModelMain.Auth, ViewModelMain.DSParams, wellDatas, wellboreDatas, out runID);
                    if (String.IsNullOrEmpty(ret))
                    {
                        rowUpload = wellDatas.Count + wellboreDatas.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = "Failed to Import the Excel data to OSDU Data Platform.\r\n" + ex.Message.ToString();
                ViewModelMain.Log.SetLog(ModelLog.Notification.Error, ModelLog.Method.DataLoadToOSDUPlatform, ModelLog.Status.Failed, ret);
            }

            return ret;
        }


        private async void GenerateWellTemplate_MainAsync(String filePath)
        {
            try
            {
                EnableDisableProgressBar(true);
                Cursor = Cursors.WaitCursor;
                SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupData, ModelLog.Status.InProgress, "");
                String result = await Task.Run(() => GenerateWellTemplate_Sub(filePath));
                EnableDisableProgressBar(false);
                Cursor = Cursors.Default;
                if (!String.IsNullOrEmpty(result))
                {
                    string Msg = "Failed to Generate the Excel file.\r\n" + result;
                    SetLog(ModelLog.Notification.Warning, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Failed, Msg);
                    MessageBox.Show(Msg);
                }
                else
                {
                    string Msg = "Excel template succesfully generated. See " + filePath + "";
                    SetLog(ModelLog.Notification.Info, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Success, Msg);
                    MessageBox.Show(Msg);
                }
                   
            }
            catch (Exception ex)
            {
                String ErrMsg = "Failed to Generate the Excel file." + ex.Message;
                EnableDisableProgressBar(false);
                Cursor = Cursors.Default;
                SetLog(ModelLog.Notification.Warning, ModelLog.Method.ExportExcelTemplate, ModelLog.Status.Failed, ErrMsg);
                MessageBox.Show(ErrMsg);
            }

        }

        private String GenerateWellTemplate_Sub(String filePath)
        {
            String ret = String.Empty;
            String result = ViewModelMain.SetupAllLookUpValues();
            if (!String.IsNullOrEmpty(result))
            {
                ViewModelMain.Log.SetLog(ModelLog.Notification.Warning, ModelLog.Method.GetLookupData, ModelLog.Status.Failed, result);
                ret = "Failed to get lookup data.\r\n" + result;
            }
            else
            {
                ViewModelMain.Log.SetLog(ModelLog.Notification.Info, ModelLog.Method.GetLookupData, ModelLog.Status.Success, "");
                ret = ViewModelMain.GenerateOSDUTemplate(filePath);
            }
            return ret;
        }

        private void ClearSession()
        {
            ViewModelMain.Auth.RefreshToken = String.Empty;
            txtboxUserName.Text = string.Empty;
            toolStripStatuslblStatus.Text = "Not connected...";
            btnLogOut.Enabled = false;
            btnSignIn.Enabled = true;
        }


    }

}
